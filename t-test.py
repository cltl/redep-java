import subprocess
from scipy import stats
import numpy as np
from collections import defaultdict
import sys

report_table = defaultdict(lambda: defaultdict(dict))

sl = {'valid': {'Labeled': 0.896, 'Unlabeled': 0.915},
      'test': {'Labeled': 0.894, 'Unlabeled': 0.913}}
for sample_num in [1,2,4,8]:
    for dataset in ['valid', 'test']:
        for measure in ['Unlabeled', 'Labeled']:
            cmd = ("grep -A 2 %s.conll exp-deviation.sh.out "
                   "| grep -A 2 sample-%d | grep %s | cut -c 34-39"
                   %(dataset, sample_num, measure))
#             print cmd
            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            out = proc.communicate()[0]
            x = np.array([float(line) for line in out.split('\n') if line])*100
            p_value = stats.ttest_1samp(x, sl[dataset][measure]*100)[1]
            report_table[sample_num][dataset][measure] = (np.mean(x), p_value)
            if dataset == 'test':
                report_table[sample_num]['Test std.'][measure] = np.std(x)
            print ("#sample=%d\tdataset=%s\tmeasure=%s\t-->\t%.1f\t%.2f\tp-value=%s" 
                   %(sample_num, dataset, measure.ljust(9), np.mean(x), np.std(x), p_value))
        
for sample_num in [1,2,4,8]:
    sys.stdout.write(r'%d' %sample_num)
    for dataset in ['valid', 'test']:
        for measure in ['Unlabeled', 'Labeled']:
            sys.stdout.write(r' & %.1f' %(report_table[sample_num][dataset][measure][0]))
            if report_table[sample_num][dataset][measure][1] < 0.001:
                sys.stdout.write('\s')
    for measure in ['Unlabeled', 'Labeled']:
        sys.stdout.write(r' & %.2f' %(report_table[sample_num]['Test std.'][measure]))
    sys.stdout.write(r' \\ \hline' + '\n')