nohup bash -c "./$1 ; mail -s \"Experiment notification: $1\" $2 \
        <<< \"Your experiment have finished or failed.\"" >$1.out 2>&1 &