Experiments for the paper "Tackling Error Propagation through Reinforcement Learning: A Case of Greedy Dependency Parsing". Minh Lê and Antske Fokkens. EACL 2017.

## Requirements

You will need Java 8 to compile and run the code.

## Prepare data

1. Download word clusters from [Stanford POS tagger website](http://nlp.stanford.edu/software/pos-tagger-faq.shtml#distsim) and put into folder `stanford-postagger-2015-12-09`
2. Download [PENN Treebank 3](https://catalog.ldc.upenn.edu/LDC99T42) and copy *directories* under `parsed/mrg/wsj` into a directory named `penntree` in project root directory. The directory structure should look like `<project>/penntree/00`, `<project>/penntree/01`,... DON'T copy `MERGE.LOG`.
3. Run `prepare.sh` (usually it takes less than 2 hours but if jackknifing is enabled, it takes about a day to finish).

## Run experiments

> Notice: all experiments write to standard output/error. If you want to inspect 
> the results later, make sure you redirect it to some file. Some experiments
> can take days to finish so you might want to use `nohup` or `screen`.

Main experiments:

1. `exp-arc-standard.sh`: train [Stanford supervisedly-trained model](nlp.stanford.edu/software/nndep.shtml) with reinforcement learning
1. `exp-arc-eager.sh`: train an arc-eager system with supervised and reinforcement learning
1. `exp-swap-standard.sh`: train a swap-standard system with supervised and reinforcement learning
1. `exp-sample-size.sh`: evaluate the effect of sample sizes on performance
2. `exp-deviation.sh`: evaluate the effect of sample sizes on deviation
3. `exp-error-prop.sh`: run error propagation experiment on the original model
3. `error_prop_rl-memory.sh`: run error propagation experiment on the reinforcement learning memory model (produces stdout)
3. `error_prop_rl-oracle.sh`: run error propagation experiment on the reinforcement learning oracle model (produces stdout)
3. `error_prop_rl-random.sh`: run error propagation experiment on the reinforcement learning random model (produces stdout)
3. `pull_out_stats_and_create_csv.py`: pulls out the process per sentence from the output of the error propagation experiments (producing a .tsv file).
3. `evaluate_error_propagation.py`: provides overall statistics based on the tsv file of the results per sentence.

Other scripts:

1. `stanford-published-model.sh`: Run Stanford parser + published models on standard test sets 
2. `supervised-learning.sh`: Train (supervisedly) and test arc-standard system
3. `start.sh <script> <email>`: run a script in background and email you when it finishes

## References

Chen, D., & Manning, C. (2014). A Fast and Accurate Dependency Parser using Neural Networks. In Proceedings of the 2014 Conference on Empirical Methods in Natural Language Processing (EMNLP) (pp. 740–750). Doha, Qatar: Association for Computational Linguistics.