import sys



def convert_to_csv(inputfile):

    outputfile = inputfile.replace('.txt', '.tsv')
    my_error_prop_results = {}
    myin = open(inputfile, 'r')
    output = False
    for line in myin:
        #setting range for where output of parser occurs
        if 'Parsing' in line:
            output = False
        if output:
            parts = line.split('\t')
            key = int(parts[0])
            if not key in my_error_prop_results:
                my_error_prop_results[key] = [parts[1].rstrip()]
            else:
                my_error_prop_results[key].append(parts[1].rstrip())
                    
        if 'PreComputed 1' in line:
            output = True


    myin.close()
    myout = open(outputfile, 'w')
    for key in sorted(my_error_prop_results):
        values = my_error_prop_results.get(key)
        for val in values:
            myout.write(str(key) + '\t' + val + '\n')
    myout.close()


def main():

    myargs = sys.argv
    if len(myargs) < 2:
        print('Error: you must provide an input file')
    else:
        convert_to_csv(myargs[1])

if __name__ == '__main__':
    main()