'''
Convert the output of exp-sample-size.sh into a Latex table
'''
import re
from collections import defaultdict
import sys

fname = sys.argv[1] if len(sys.argv) >= 2 else 'exp-sample-size.sh.out'
with open(fname) as f:
    table = defaultdict(lambda: defaultdict(dict))
    line = f.readline()
    while line:
        m = re.search("Parsing results.+sampleSize-(\d+)-maxIters-(\d+)-(\w+)", line)
        if m:
            col = m.group(1) + ("*" if m.group(2) == "8000" else "")
            row1 = m.group(3)
            read_rows = set()
            while line and len(read_rows) < 2:
                m2 = re.search("(\w+) Attachment Score.+(0\\.\d+)", line)
                if m2:
                    row2 = m2.group(1)
                    val = float(m2.group(2))
                    table[row1][row2][col] = val
                    read_rows.add(row2)
                line = f.readline()
        else:
            line = f.readline()

def write_row(row):
    sys.stdout.write(" & %.1f" %(row['1']*100))
    sys.stdout.write(" & %.1f" %(row['1*']*100))
    sys.stdout.write(" & %.1f" %(row['2']*100))
    sys.stdout.write(" & %.1f" %(row['4']*100))
    sys.stdout.write(" & %.1f" %(row['8']*100))
    sys.stdout.write(" & %.1f" %(row['16']*100))
    sys.stdout.write(" & %.1f" %(row['32']*100))
        
print(r"""
\begin{tabular}{|l|l|c|c|c|c|c|c|c|}
\hline
\multicolumn{2}{|l|}{} & 1 & 1$^*$ & 2 & 4 & 8 & 16 & 32 \\ \hline""")
sys.stdout.write(r"\multirow{2}{*}{Dev}  & UAS")
write_row(table['valid']['Unlabeled'])
print(r" \\ \cline{2-9}")
sys.stdout.write(r" & LAS")
write_row(table['valid']['Labeled'])
print(r" \\ \hline")
sys.stdout.write(r"\multirow{2}{*}{Test}  & UAS")
write_row(table['test']['Unlabeled'])
print(r" \\ \cline{2-9}")
sys.stdout.write(r" & LAS")
write_row(table['test']['Labeled'])
print(r" \\ \hline")
print(r"\end{tabular}")