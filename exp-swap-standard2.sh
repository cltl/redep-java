REINFORCEMENT_CMD="java -cp 
        target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.re.ReinforcementLearning \
        -train spmrl2014/polish/conll/train/train.Polish.gold.conll \
        -valid spmrl2014/polish/conll/dev/dev.Polish.gold.conll \
        -pretrained output/dep/supervised-swap-standard.bin"

evaluate_with_sprml() {
    MODEL_NAME=$1

    # evaluate against valid set
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.Parser -rootLabel root \
            -model output/dep/$MODEL_NAME.bin \
            -input spmrl2014/polish/conll/dev/dev.Polish.gold.conll \
            -output output/dep/$MODEL_NAME-valid.conll && \
    java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
            edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True \
            -g spmrl2014/polish/conll/dev/dev.Polish.gold.conll \
            -s output/dep/$MODEL_NAME-valid.conll && \
    # evaluate against test set
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.Parser -rootLabel root \
            -model output/dep/$MODEL_NAME.bin \
            -input spmrl2014/polish/conll/test/test.Polish.gold.conll \
            -output output/dep/$MODEL_NAME-test.conll && \
    java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
            edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True \
            -g spmrl2014/polish/conll/test/test.Polish.gold.conll \
            -s output/dep/$MODEL_NAME-test.conll
}

repeat_reinforce() { # because it varies more
    for x in $(seq -f "%02g" 1 5); do
        echo "######### reinforcement learning: reinforce$x" && \
        $REINFORCEMENT_CMD -reinforce -maxIters 8000 \
                -parser output/dep/reinforcement-swap-standard-reinforce$x.bin && \
        evaluate_with_sprml reinforcement-swap-standard-reinforce$x

        if [ $? -ne 0 ]
        then
            return $?
        fi
    done
}

echo -n 'Start: '; date
mvn clean package compile assembly:single && \
    echo "######### supervised learning" && \
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.SupervisedLearning \
            -transition swap-standard -rootLabel root \
            -train spmrl2014/polish/conll/train/train.Polish.gold.conll \
            -valid spmrl2014/polish/conll/dev/dev.Polish.gold.conll \
            -parser output/dep/supervised-swap-standard.bin && \
    evaluate_with_sprml supervised-swap-standard && \
    repeat_reinforce 5 && \
    echo "######### reinforcement learning: oracle" && \
    $REINFORCEMENT_CMD -sampler oracle -maxIters 1000 \
            -parser output/dep/reinforcement-swap-standard-oracle.bin && \
    evaluate_with_sprml reinforcement-swap-standard-oracle && \
    echo "######### reinforcement learning: random" && \
    $REINFORCEMENT_CMD -sampler random -maxIters 1000 \
            -parser output/dep/reinforcement-swap-standard-random.bin && \
    evaluate_with_sprml reinforcement-swap-standard-random && \
    echo "######### reinforcement learning: memory" && \
    $REINFORCEMENT_CMD -sampler memory -maxIters 1000 \
            -parser output/dep/reinforcement-swap-standard-memory.bin && \
    evaluate_with_sprml reinforcement-swap-standard-memory
EXIT_CODE=$?
echo -n 'Stop: '; date
exit $EXIT_CODE
