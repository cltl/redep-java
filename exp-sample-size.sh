train_and_evalute() {
    NAME="reinforcement-arc-standard-random-sampleSize-$1-maxIters-$2"
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.re.ReinforcementLearning -sampler random \
            -sampleSize $1 -maxIters $2 \
            -train output/dep/penntree.sd/train.mrg.dep \
            -valid output/dep/penntree.sd/valid.mrg.dep \
            -pretrained stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz \
            -parser output/dep/sd-$NAME.bin && \
    ./evaluate.sh sd root $NAME
}

echo -n 'Start: '; date 
mvn clean package compile assembly:single && \
    train_and_evalute 1 8000 && \
    train_and_evalute 1 1000 && \
    train_and_evalute 2 1000 && \
    train_and_evalute 4 1000 && \
    train_and_evalute 8 1000 && \
    train_and_evalute 16 1000 && \
    train_and_evalute 24 1000 && \
    train_and_evalute 32 1000
EXIT_CODE=$?
echo -n 'Stop: '; date
exit $EXIT_CODE