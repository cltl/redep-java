REINFORCEMENT_CMD="java -cp 
        target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.re.ReinforcementLearning \
        -train output/dep/penntree.sd/train.mrg.dep \
        -valid output/dep/penntree.sd/valid.mrg.dep \
        -pretrained stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz"

repeat_reinforce() { # because it varies more
    for x in $(seq -f "%02g" 1 5); do
        echo "######### reinforcement learning: reinforce$x" && \
        $REINFORCEMENT_CMD -reinforce -maxIters 8000 \
                -parser output/dep/sd-reinforcement-arc-standard-reinforce$x.bin && \
        ./evaluate.sh sd root reinforcement-arc-standard-reinforce$x
        
        if [ $? -ne 0 ]
        then
            return $?
        fi
    done
}

echo -n 'Start: '; date 
mvn clean package compile assembly:single && \
    echo "######### supervised learning" && \
    java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
            edu.stanford.nlp.parser.nndep.DependencyParser \
            -model stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz \
            -testFile output/dep/penntree.sd/valid.mrg.dep && \
    java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
            edu.stanford.nlp.parser.nndep.DependencyParser \
            -model stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz \
            -testFile output/dep/penntree.sd/test.mrg.dep && \
    repeat_reinforce 5 && \
    echo "######### reinforcement learning: oracle" && \
    $REINFORCEMENT_CMD -sampler oracle -maxIters 1000 \
            -parser output/dep/sd-reinforcement-arc-standard-oracle.bin && \
    ./evaluate.sh sd root reinforcement-arc-standard-oracle && \
    echo "######### reinforcement learning: random" && \
    $REINFORCEMENT_CMD -sampler random -maxIters 1000 \
            -parser output/dep/sd-reinforcement-arc-standard-random.bin && \
    ./evaluate.sh sd root reinforcement-arc-standard-random && \
    echo "######### reinforcement learning: memory" && \
    $REINFORCEMENT_CMD -sampler memory -maxIters 1000 \
            -parser output/dep/sd-reinforcement-arc-standard-memory.bin && \
    ./evaluate.sh sd root reinforcement-arc-standard-memory
EXIT_CODE=$?
echo -n 'Stop: '; date
exit $EXIT_CODE