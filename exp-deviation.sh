repeat() {
    for x in $(seq -f "%02g" 1 10); do
        $@ $x
        if [ $? -ne 0 ]
        then
            return $?  
        fi
    done
}

train_and_evalute() {
    NAME="reinforcement-arc-standard-random-deviation-sample-$1-attempt-$3"
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.re.ReinforcementLearning -sampler random \
            -sampleSize $1 -maxIters $2 \
            -train output/dep/penntree.sd/train.mrg.dep \
            -valid output/dep/penntree.sd/valid.mrg.dep \
            -pretrained stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz \
            -parser output/dep/sd-$NAME.bin && \
    ./evaluate.sh sd root $NAME
}

echo -n 'Start: '; date 
mvn clean package compile assembly:single && \
    repeat train_and_evalute 1 1000 && \
    repeat train_and_evalute 1 8000 && \
    repeat train_and_evalute 2 1000 && \
    repeat train_and_evalute 4 1000 && \
    repeat train_and_evalute 8 1000
    echo "Done!"
EXIT_CODE=$?
echo -n 'Stop: '; date
exit $EXIT_CODE