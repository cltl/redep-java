package nl.cltl.ulm4.util;

import static nl.cltl.ulm4.util.Pythonic.assert_;

import java.util.Arrays;
import java.util.Random;

/**
 * Treating arrays as tensors
 * 
 */
public final class Tensors {
	
	public static double exp(double val) {
	    final long tmp = (long) (1512775 * val + 1072632447);
	    double ret = Double.longBitsToDouble(tmp << 32);
	    if (ret > 0) return ret;
	    return Double.MIN_VALUE;
	}
	
	public static int[][] clone(int[][] t) {
		if (t == null) return null; 
		return Arrays.stream(t).map(row -> row.clone())
				.toArray(length -> new int[length][]);
	}

	public static int[] fill(int[] t, int val) {
		Arrays.fill(t, val);
		return t;
	}

	public static int[][] fill(int[][] t, int num) {
		for (int i = 0; i < t.length; i++) {
			Arrays.fill(t[i], num);
		}
		return t;
	}

	public static void fill(double[][] t, double val) {
		for (int i = 0; i < t.length; i++) {
			Arrays.fill(t[i], val);
		}
	}

	public static double[] fill(double[] t, int val) {
		Arrays.fill(t, val);
		return t;
	}

	public static void fillRange(int[][] t, int val, int start, int stop) {
		for (int i = start; i < stop; i++) {
			Arrays.fill(t[i], val);
		}
	}

	public static void fill(Object[][] t, Object val) {
		for (int i = 0; i < t.length; i++) {
			Arrays.fill(t[i], val);
		}
	}

	public static void fillColumns(int[][] t, int start, int len, int value) {
		for (int i = 0; i < t.length; i++) {
			Arrays.fill(t[i], start, start + len, value);
		}
	}

	public static void copyColumns(int[][] src, int srcCol, int[][] dest, int destCol, int len) {
		assert_(src.length == dest.length);
		for (int i = 0; i < src.length; i++) {
			System.arraycopy(src[i], srcCol, dest[i], destCol, len);
		}
	}

	public static int[][] narrow(int[][] arr, int start, int num) {
		return Arrays.copyOfRange(arr, start, start + num);
	}

	public static int[] narrow(int[] arr, int start, int num) {
		return Arrays.copyOfRange(arr, start, start + num);
	}

	public static int[][] narrowColumns(int[][] t, int start, int num) {
		int[][] newArr = new int[t.length][num];
		copyColumns(t, start, newArr, 0, num);
		return newArr;
	}

	public static boolean notEquals(int[] a1, int[] a2) {
		if (a1.length != a2.length) return true;
		for (int i = 0; i < a1.length; i++) {
			if (a1[i] != a2[i])
				return false;
		}
		return true;
	}

	/**
	 * Add the two 2d arrays in place of {@code m1}.
	 *
	 * @throws java.lang.IndexOutOfBoundsException
	 *             (possibly) If {@code m1} and {@code m2} are not of the same
	 *             dimensions
	 */
	public static void addInPlace(double[][] m1, double[][] m2) {
		for (int i = 0; i < m1.length; i++)
			for (int j = 0; j < m1[0].length; j++)
				m1[i][j] += m2[i][j];
	}

	/**
	 * Add the two 1d arrays in place of {@code a1}.
	 *
	 * @throws java.lang.IndexOutOfBoundsException
	 *             (Possibly) if {@code a1} and {@code a2} are not of the same
	 *             dimensions
	 */
	public static void addInPlace(double[] a1, double[] a2) {
		for (int i = 0; i < a1.length; i++)
			a1[i] += a2[i];
	}

	public static String toString(int[][] t) {
		StringBuilder sb = new StringBuilder("int[][] {\n");
		for (int i = 0; i < t.length; i++) {
			for (int j = 0; j < t[i].length; j++) {
				sb.append("\t").append(t[i][j]);
			}
			sb.append("\n");
		}
		sb.append("}");
		return sb.toString();
	}

	public static int[] random(Random r, int lowerInclusive, int upperExclusive, int dim) {
		int[] t = new int[dim];
		for (int i = 0; i < t.length; i++) {
			t[i] = lowerInclusive + r.nextInt(upperExclusive-lowerInclusive);
		}
		return t;
	}

	public static int[][] random(Random r, int lowerInclusive, int upperExclusive, int dim1, int dim2) {
		int[][] t = new int[dim1][dim2];
		for (int i = 0; i < t.length; i++) {
			for (int j = 0; j < t[0].length; j++) {
				t[i][j] = lowerInclusive + r.nextInt(upperExclusive-lowerInclusive);
			}
		}
		return t;
	}

	public static double norm(double[][] t) {
		double s = 0;
		for (int i = 0; i < t.length; i++) {
			for (int j = 0; j < t[0].length; j++) {
				s += t[i][j] * t[i][j];
			}
		}
		return Math.sqrt(s);
	}

	public static double norm(double[] t) {
		double s = 0;
		for (int i = 0; i < t.length; i++) {
			s += t[i] * t[i];
		}
		return Math.sqrt(s);
	}

	public static void normalize(double[] t) {
		double n = norm(t);
		if (n > 0) {
			for (int i = 0; i < t.length; i++) {
				t[i] /= n;
			}
		}
	}

	/**
	 * Normalize word embeddings by setting mean = rMean, std = rStd
	 */
	public static void scale(double[][] A, double rMean, double rStd) {
		int count = 0;
		double sum = 0.0;
		double sum2 = 0.0;
		for (double[] ai : A)
			for (double aij : ai) {
				count += 1;
				sum += aij;
				sum2 += aij * aij;
			}
		double mean = sum / count;
		double std = Math.sqrt(sum2 / count - mean * mean);
		for (int i = 0; i < A.length; ++i)
			for (int j = 0; j < A[i].length; ++j)
				A[i][j] = (A[i][j] - mean) * rStd / std + rMean;
	}

	/**
	 * Normalize word embeddings by setting mean = 0, std = 1
	 */
	public static void scale(double[][] A) {
		scale(A, 0.0, 1.0);
	}

	public static double sum(double[] t) {
		double s = 0;
		for (int i = 0; i < t.length; i++) {
			s += t[i];
		}
		return s;
	}

	public static void div(double[] t, double d) {
		for (int i = 0; i < t.length; i++) {
			t[i] /= d;
		}
	}

	public static int maxOfColumn(int[][] t, int col) {
		int v = Integer.MIN_VALUE;
		for (int i = 0; i < t.length; i++) {
			if (t[i][col] > v) {
				v = t[i][col];
			}
		}
		return v;
	}

}
