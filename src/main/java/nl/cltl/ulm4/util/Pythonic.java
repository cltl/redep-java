package nl.cltl.ulm4.util;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

public final class Pythonic {

	public static void error(String message) {
		throw new RuntimeException(message);
	}

	public static void assert_(boolean b) {
		assert_(b, null);
	}

	public static void assert_(boolean b, String message) {
		if (!b) {
			throw new RuntimeException(message);
		}
	}
	
	public static List<Integer> list(int... arr) {
		List<Integer> list = new ArrayList<>();
		for (int v : arr) list.add(v);
		return list;
	}
	
	public static List<Integer> range(int stop) {
		return range(0, stop, 1);
	}

	public static List<Integer> range(int start, int stop) {
		return range(start, stop, 1);
	}

	public static List<Integer> range(int start, int stop, int step) {
		List<Integer> list = new ArrayList<>((stop-start)/step); 
		for (int i = start; i < stop; i += step) {
			list.add(i);
		}
		return list;
	}

	public static <K, V> Map<K, V> defaultdict(Supplier<V> defaultValueFactory) {
		return defaultdict(new HashMap<>(), defaultValueFactory);
	}
	
	public static <K, V> Map<K, V> defaultdict(Map<K, V> map, Supplier<V> defaultValueFactory) {
		return new MapWithFactory<K, V>(map, defaultValueFactory);
	}
	
	private static class MapWithFactory<K, V> extends MapWrapper<K, V> implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Supplier<V> defaultValueFactory;

		public MapWithFactory(Map<K, V> innerMap, Supplier<V> defaultValueFactory) {
			super(innerMap);
			this.defaultValueFactory = defaultValueFactory;
		}

		@Override
		public V get(Object key) {
			V value = super.get(key);
			if (value == null) {
				value = defaultValueFactory.get();
				super.put((K)key, value);
			}
			return value;
		}
		
	}
	
	public static void exec(String cmd, Object... args) {
		cmd = String.format(cmd, args);
		System.out.println("exec: " + cmd);
	    try {
	    	ProcessBuilder pb = new ProcessBuilder("sh", "-c", cmd);
	    	pb.inheritIO();
	    	Process p = pb.start();
			int code = p.waitFor();
			assert_(code == 0);
		} catch (InterruptedException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static String concatPaths(String parent, String child) {
		return new File(parent, child).getPath();
	}

	public static <K, V> HashMap<K, V> newHashMap(K key1, V val1) {
		return newHashMap(key1, val1, null, null, null, null);
	}

	public static <K, V> HashMap<K, V> newHashMap(K key1, V val1, K key2, V val2) {
		return newHashMap(key1, val1, key2, val2, null, null);
	}

	public static <K, V> HashMap<K, V> newHashMap(K key1, V val1, K key2, V val2, K key3, V val3) {
		HashMap<K, V> map = new HashMap<>();
		if (key1 != null && val1 != null) map.put(key1, val1);
		if (key2 != null && val2 != null) map.put(key2, val2);
		if (key3 != null && val3 != null) map.put(key3, val3);
		return map;
	}

	public static File tmpname() {
		File file;
		try {
			file = File.createTempFile("redep", "", new File(System.getProperty("user.dir")));
			file.deleteOnExit();
			return file;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SafeVarargs
	public static <T> Iterable<T> concatIters(Iterable<T>... iters) {
		return concatIters(Arrays.asList(iters));
	}

	public static <T> Iterable<T> concatIters(List<Iterable<T>> iters) {
		return new Iterable<T>() {

			@Override
			public Iterator<T> iterator() {
				return new Iterator<T>() {
					
					Iterator<T> iter = null;
					int iter_index = -1;

					@Override
					public boolean hasNext() {
						while ((iter == null || !iter.hasNext()) 
								&& iter_index < iters.size()-1) {
							iter = iters.get(++iter_index).iterator();
						}
						return iter.hasNext();
					}

					@Override
					public T next() {
						if (!hasNext()) throw new NoSuchElementException();
						return iter.next();
					}
				};
			}
		};
	}
	
	public static <T> int len(Iterable<T> iter) {
		int s = 0;
		for (T v : iter) s++;
		return s;
	}

	public static <T> int iter_size(Iterator<T> iter) {
		return len(iter);
	}
		
	public static <T> int len(Iterator<T> iter) {
		int s = 0;
		while (iter.hasNext()) {
			iter.next();
			s++;
		}
		return s;
	}
	
	public static Iterator<Integer> findIter(String needle, String haystack) {
		return new Iterator<Integer>() {

			int i = 0;
			int lastReturnedResult = -1;
			
			@Override
			public boolean hasNext() {
				if (i >= 0 && i < lastReturnedResult) return true;
				i = haystack.indexOf(needle, lastReturnedResult+1);
				return i >= 0;
			}

			@Override
			public Integer next() {
				if (hasNext()) {
					lastReturnedResult = i;
					return i;
				}
				throw new NoSuchElementException();
			}
		};
	}
	
	public static <T> List<T> reversed(List<T> list) {
		ArrayList<T> newList = new ArrayList<T>(list);
		Collections.reverse(newList);
		return newList;
	}

}
