package nl.cltl.ulm4.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

class MapWrapper<K, V> implements Map<K, V>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Map<K, V> innerMap;
	
	public MapWrapper(Map<K, V> innerMap) {
		super();
		this.innerMap = innerMap;
	}

	public int size() {
		return innerMap.size();
	}

	public boolean isEmpty() {
		return innerMap.isEmpty();
	}

	public boolean containsKey(Object key) {
		return innerMap.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return innerMap.containsValue(value);
	}

	public V get(Object key) {
		return innerMap.get(key);
	}

	public V put(K key, V value) {
		return innerMap.put(key, value);
	}

	public V remove(Object key) {
		return innerMap.remove(key);
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		innerMap.putAll(m);
	}

	public void clear() {
		innerMap.clear();
	}

	public Set<K> keySet() {
		return innerMap.keySet();
	}

	public Collection<V> values() {
		return innerMap.values();
	}

	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return innerMap.entrySet();
	}

	public boolean equals(Object o) {
		return innerMap.equals(o);
	}

	public int hashCode() {
		return innerMap.hashCode();
	}

	@Override
	public String toString() {
		return innerMap.toString();
	}
	
}
