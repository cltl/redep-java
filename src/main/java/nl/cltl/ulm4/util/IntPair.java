package nl.cltl.ulm4.util;

import java.io.Serializable;

public class IntPair implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public int a;
	public int b;

	public IntPair() {	}

	public IntPair(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + a;
		result = prime * result + b;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntPair other = (IntPair) obj;
		if (a != other.a)
			return false;
		if (b != other.b)
			return false;
		return true;
	}
}
