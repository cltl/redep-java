package nl.cltl.ulm4.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileUtils {

	/**
	 * Find all files, sorted in lexicographic order.
	 * 
	 * @param list
	 * @param file
	 * @return
	 */
	public static List<File> find_files_recursively(List<File> list, File file) {
		if (list == null) list = new ArrayList<>();
		if (file.isDirectory()) {
			File[] children = file.listFiles();
			Arrays.sort(children);
			for (File child : children) {
				find_files_recursively(list, child);
			}
		} else if (file.isFile()) {
			list.add(file);
		}
		return list;
	}

}
