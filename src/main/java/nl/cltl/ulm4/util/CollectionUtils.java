package nl.cltl.ulm4.util;

import static nl.cltl.ulm4.util.Pythonic.concatIters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public final class CollectionUtils {
	
	public static String[] toArray(String[]... arrs){
	    List<String> list=new ArrayList<>();
	    for (String[] i : arrs) {
	        list.addAll(Arrays.asList(i));
	    }
	    return list.toArray(new String[list.size()]);
	}

	@SafeVarargs
	public static int[] toArray(List<Integer>... lists){
		int totalLength = Arrays.stream(lists).mapToInt(arr -> arr.size()).sum();
		int[] newArr = new int[totalLength];
		for (int i = 0, j = 0; i < lists.length; i++) {
			for (int v : lists[i]) {
				newArr[j++] = v;
			}
		}
	    return newArr;
	}

	public static int[] toArray(int[]... arrs){
		int totalLength = Arrays.stream(arrs).mapToInt(arr -> arr.length).sum();
		int[] newArr = new int[totalLength];
		for (int i = 0, j = 0; i < arrs.length; i++) {
			System.arraycopy(arrs[i], 0, newArr, j, arrs[i].length);
		}
	    return newArr;
	}
	
	public static <T> T unique(Collection<T> col) {
		Iterator<T> it = col.iterator();
		if (!it.hasNext()) {
			throw new IllegalStateException("Collection must not be empty.");
		}
		T v = it.next();
		if (it.hasNext()) {
			throw new IllegalStateException("Collection must not have more than one element.");
		}
		return v;
	}
	
	public static <T> List<Map<String, Iterable<T>>> kfold_contiguous(List<T> list, int k) {
	    // folds form contiguous blocks: 0,0,...0,1,1,...,1,...
		List<List<T>> dividens = new ArrayList<>(); 
		int start = 0;
		for (int i = 0; i < k; i++) {
			int stop = Math.min((i+1) * (list.size()/k), list.size());
			dividens.add(list.subList(start, stop));
			start = stop;
		}
		List<Map<String, Iterable<T>>> folds = new ArrayList<>();
		for (int i = 0; i < k; i++) {
			List<Iterable<T>> train_iters = new ArrayList<>(dividens);
			train_iters.remove(i);
			Map<String, Iterable<T>> fold = new HashMap<>();
			fold.put("train", concatIters(train_iters));
			fold.put("test", dividens.get(i));
			folds.add(fold);
		}
	    return folds;
	}

	public static void randomizeFirstN(int[] arr, int n, Random random) {
		for (int i = 0; i < n; i++) {
			int indexToSwap = i + random.nextInt(arr.length - i);
			int temp = arr[i];
			arr[i] = arr[indexToSwap];
			arr[indexToSwap] = temp;
		}
	}
	
	public static int[] sample(List<Double> probs, int k, Random random) {
		List<Integer> indices = new ArrayList<>(probs.size());
		double[] culmulativeProbs = new double[probs.size()];
		double lastProb = 0;
		for (int j = 0; j < culmulativeProbs.length; j++) {
			indices.add(j);
			culmulativeProbs[j] = lastProb + probs.get(j);
			lastProb = culmulativeProbs[j];
		}
		
		int retLen = Math.min(k, probs.size());
		int[] ret = new int[retLen];
		for (int i = 0; i < retLen; i++) {
			double r = random.nextDouble() * lastProb;
			int s = Arrays.binarySearch(culmulativeProbs, r);
			if (s < 0) s = -(s + 1);
			ret[i] = indices.remove(s);
			
			lastProb = 0;
			if (s > 0) lastProb = culmulativeProbs[s-1];
			for (int j = s; j < indices.size(); j++) {
				culmulativeProbs[j] = lastProb + probs.get(indices.get(j));
				lastProb = culmulativeProbs[j];
			}
		}
		return ret;
	}

	public static <T extends Comparable<T>> int minIndexRandomTieBreaking(List<T> list) {
		int minIndex = -1;
		T minValue = null;
		double[] randomValues = new Random().doubles(list.size()).toArray();
		for (int i = 0; i < list.size(); i++) {
			T v = list.get(i);
			if (minValue == null || minValue.compareTo(v) > 0 ||
					(minValue.compareTo(v) == 0 && 
					randomValues[minIndex] > randomValues[i])) {
				minIndex = i;
				minValue = v;
			}
		}
		return minIndex;
	}

}
