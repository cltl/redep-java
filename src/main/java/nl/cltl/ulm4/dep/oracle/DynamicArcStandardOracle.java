package nl.cltl.ulm4.dep.oracle;

import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.util.CollectionUtils.unique;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.concatIters;
import static nl.cltl.ulm4.util.Pythonic.defaultdict;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.ArcStandard;
import nl.cltl.ulm4.dep.TransitionSystem;
import nl.cltl.ulm4.util.IntPair;

public class DynamicArcStandardOracle implements DynamicOracle {

	/**
	 * see Goldberg et al. (2014) Goldberg, Y., Sartorio, F., & Satta, G.
	 * (2014). A tabular method for dynamic oracles in transition-based parsing.
	 * Transactions of the Association for; Computational Linguistics, 2,
	 * 119–130.
	 */

	public static List<Integer> make_right_stack(TransitionSystem sys, int[][] gold_links) {
		List<Integer> buffer = sys.getBuffer();
		if (buffer.isEmpty()) return new ArrayList<>();
		List<Integer> right_stack = new ArrayList<>();
		Set<Integer> is_in_left_stack = new HashSet<>(sys.getStack());
		Set<Integer> is_in_right_stack = new HashSet<>();
		int min_buffer = buffer.get(0);
		int max_buffer = buffer.get(buffer.size()-1);
		for (Integer i : buffer) {
			int parent_i = gold_links[i][COL_HEAD];
			boolean added = !(parent_i >= min_buffer && parent_i <= max_buffer);
			if (!added) {
				for (int j = 0; j < gold_links.length; j++) {
					if (gold_links[j][COL_HEAD] == i
							&& (is_in_left_stack.contains(j) || is_in_right_stack
									.contains(j))) {
						added = true;
						break;
					}
				}
			}
			if (added) {
				right_stack.add(i);
				is_in_right_stack.add(i);
			}
		}
		return right_stack;
	}

	static int tree_frag_unlabeled_loss(int subtreeRoot, int[][] links,
			int[][] gold_links) {
		int loss = 0;
		for (int i = 0; i < gold_links.length; i++) {
			if (links[i][0] == subtreeRoot) {
				if (links[i][0] != gold_links[i][COL_HEAD]) loss++;
				loss += tree_frag_unlabeled_loss(i, links, gold_links);
			}
		}
		return loss;
	}

	static int tree_frag_labeled_loss(int root, int[][] links,
			int[][] gold_links) {
		int loss = 0;
		for (int i = 0; i < gold_links.length; i++) {
			if (links[i][0] == root) {
				if (links[i][0] != gold_links[i][COL_HEAD] ||
						links[i][1] != gold_links[i][COL_LABEL]) loss++;
				loss += tree_frag_labeled_loss(i, links, gold_links);
			}
		}
		return loss;
	}

	static int edge_delta(int head, int dependent, int[][] gold_links) {
		if (gold_links[dependent][COL_HEAD] == head) {
			return 0;
		} else {
			return 1;
		}
	}

	public static int config_propagated_loss(TransitionSystem sys,
			int[][] gold_links) {
		List<Integer> right_stack = make_right_stack(sys, gold_links);
		List<Integer> stack = sys.getStack();
		right_stack.add(0, stack.get(0)); // see the beginning of section 4.3;
		// starts implementing Algorithm 1
		Supplier<Integer> a0 = () -> Integer.MAX_VALUE;
		Supplier<Map<Integer, Integer>> a1 = () -> defaultdict(a0);
		Supplier<Map<Integer, Map<Integer, Integer>>> a2 = () -> defaultdict(a1);
		Map<Integer, Map<Integer, Map<Integer, Integer>>> t = defaultdict(a2);
		// this line is different from the original algorithm  to avoid adding
		// Loss(t(stack.get(1)), t_G) twice
		t.get(0).get(0).put(stack.get(0), 0);
		int ll = stack.size();
		int lr = right_stack.size();
		for (int d = 0; d <= ll + lr - 2; d++) {
			for (int j = Math.max(0, d-ll+1); j <= Math.min(d, lr-1); j++) { // column index;
				int i = d - j; // row index;
				Iterable<Integer> iter_Delta = concatIters(
						stack.subList(0, i+1), right_stack.subList(0, j+1));
				if (i < ll-1) {
					for (int h : iter_Delta) {
						t.get(i + 1).get(j).put(h,
								Math.min(
										t.get(i + 1).get(j).get(h),
										t.get(i).get(j).get(h)
												+ edge_delta(h, stack.get(i + 1), gold_links)));
						t.get(i + 1).get(j).put(stack.get(i + 1),
								Math.min(
										t.get(i + 1).get(j).get(stack.get(i + 1)),
										t.get(i).get(j).get(h)
												+ edge_delta(stack.get(i + 1), h, gold_links)));
					}
				}
				if (j < lr-1) {
					for (int h : iter_Delta) {
						t.get(i).get(j + 1).put(h,
								Math.min(
										t.get(i).get(j + 1).get(h),
										t.get(i).get(j).get(h)
												+ edge_delta(h, right_stack.get(j + 1), gold_links)));
						t.get(i).get(j + 1).put(right_stack.get(j + 1),
								Math.min(
										t.get(i).get(j + 1).get(right_stack.get(j + 1)),
										t.get(i).get(j).get(h)
												+ edge_delta(right_stack.get(j + 1), h, gold_links)));
					}
				}
			}
		}
		return t.get(ll-1).get(lr-1).get(0);
	}

	/**
	 * Return one zero-loss action (there are possibly many).
	 * @param sys
	 * @param gold_links
	 * @return
	 */
	@Override
	public int getZeroLossAction(TransitionSystem sys, int[][] gold_links, ActionsInfo info) {
		List<Integer> right_stack = make_right_stack(sys, gold_links);
		List<Integer> stack = sys.getStack();
		right_stack.add(0, stack.get(0)); // see the beginning of section 4.3
		// starts implementing Algorithm 1
		Supplier<Integer> a0 = () -> Integer.MAX_VALUE;
		Supplier<Map<Integer, Integer>> a1 = () -> defaultdict(a0);
		Supplier<Map<Integer, Map<Integer, Integer>>> a2 = () -> defaultdict(a1);
		Map<Integer, Map<Integer, Map<Integer, Integer>>> t = defaultdict(a2);
		Map<Integer, Map<Integer, Map<Integer, IntPair>>> trace = defaultdict(
				() -> defaultdict(() -> new HashMap<Integer, IntPair>()));
		// this line is different from the original algorithm  to avoid adding
		// Loss(t(stack.get(1)), t_G) twice
		t.get(0).get(0).put(stack.get(0), 0);
		final int EXPAND_LEFT = 0;
		final int EXPAND_RIGHT = 1;
		int temp;
		int ll = stack.size();
		int lr = right_stack.size();
		for (int d = 0; d <= ll + lr - 2; d++) {
			for (int j = Math.max(0, d-ll+1); j <= Math.min(d, lr-1); j++) { // column index;
				int i = d - j; // row index;
				Iterable<Integer> iter_Delta = concatIters(
						stack.subList(0, i+1), right_stack.subList(0, j+1));
				if (i < ll-1) {
					for (int h : iter_Delta) {
						temp = t.get(i).get(j).get(h)
								+ edge_delta(h, stack.get(i + 1), gold_links);
						if (temp < t.get(i + 1).get(j).get(h)) {
							t.get(i + 1).get(j).put(h, temp);
							trace.get(i + 1).get(j).put(h, 
									new IntPair(EXPAND_LEFT, stack.get(i + 1)));
						}
						temp = t.get(i).get(j).get(h)
								+ edge_delta(stack.get(i + 1), h, gold_links);
						if (temp < t.get(i + 1).get(j).get(stack.get(i + 1))) {
							t.get(i + 1).get(j).put(stack.get(i + 1), temp);
							trace.get(i + 1).get(j).put(stack.get(i + 1), 
									new IntPair(EXPAND_LEFT, h));
						}
					}
				}
				if (j < lr-1) {
					for (int h : iter_Delta) {
						temp = t.get(i).get(j).get(h)
								+ edge_delta(h, right_stack.get(j + 1), gold_links);
						if (temp < t.get(i).get(j + 1).get(h)) {
							t.get(i).get(j + 1).put(h, temp);
							trace.get(i).get(j + 1).put(h, 
									new IntPair(EXPAND_RIGHT, right_stack.get(j + 1)));
						}
						temp = t.get(i).get(j).get(h)
								+ edge_delta(right_stack.get(j + 1), h, gold_links);
						if (temp < t.get(i).get(j + 1).get(right_stack.get(j + 1))) {
							t.get(i).get(j + 1).put(right_stack.get(j + 1), temp);
							trace.get(i).get(j + 1).put(right_stack.get(j + 1), 
									new IntPair(EXPAND_RIGHT, h));
						}
					}
				}
			}
		}
		// trace back
		int head = -1, dependent = -1;
		int i = ll-1, j = lr-1, h = 0;
		while (i > 0 || j > 0) {
//			System.out.println(stack.get(i));
			IntPair cue = trace.get(i).get(j).get(h);
			head = h;
			dependent = cue.b;
//			System.out.format("%d --> %d\n", head, dependent);
			if (cue.a == EXPAND_LEFT) {
				i--;
				if (h == stack.get(i + 1)) {
					h = cue.b;
				}
			} else if (cue.a == EXPAND_RIGHT) {
				j--;
				if (h == right_stack.get(j + 1)) {
					h = cue.b;
				}
			} else {
				throw new IllegalStateException("Don't know what to do.");
			}
		}
		if (stack.size() >= 2 && head == stack.get(0) && dependent == stack.get(1)) {
			if (gold_links[dependent][COL_HEAD] == head) {
				return info.label2action(gold_links[dependent][COL_LABEL], 
						ArcStandard.ATYPE_LEFT);
			} else {
				return info.type2actions(ArcStandard.ATYPE_LEFT).get(0);
			}
		} else if (stack.size() >= 2 && head == stack.get(1) && dependent == stack.get(0)) {
			if (gold_links[dependent][COL_HEAD] == head) {
				return info.label2action(gold_links[dependent][COL_LABEL], 
						ArcStandard.ATYPE_RIGHT, ArcStandard.ATYPE_RIGHT_ROOT);
			} else {
				return info.type2actions(ArcStandard.ATYPE_RIGHT).get(0);
			}
		} else {
			return unique(info.type2actions(ArcStandard.ATYPE_SHIFT));
		}
	}

	public int unlabeledLoss(TransitionSystem sys) {
		int loss = config_propagated_loss(sys, sys.getTokens());
		int ll = sys.getStack().size();
		for (int i = 0; i < ll; i++) {
			//System.out.println(sys.getStack().get(i) + " getting stack " + i);
			loss += tree_frag_unlabeled_loss(sys.getStack().get(i), sys.getLinks(),
					sys.getTokens());
		}
		return loss;
	}

	@Override
	public int loss(TransitionSystem sys) {
		assert_(sys instanceof ArcStandard);
		int loss = config_propagated_loss(sys, sys.getTokens());
		int ll = sys.getStack().size();
		for (int i = 0; i < ll; i++) {
			loss += tree_frag_labeled_loss(sys.getStack().get(i), sys.getLinks(),
					sys.getTokens());
		}
		return loss;
	}
}