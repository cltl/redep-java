package nl.cltl.ulm4.dep.oracle;

import static nl.cltl.ulm4.dep.DependencyUtils.is_projective;
import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.util.CollectionUtils.unique;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.reversed;

import java.util.List;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.ArcStandard;
import nl.cltl.ulm4.dep.DependencyUtils;

public class DeterministicArcStandardShortestStackOracle extends
		StaticOracle<ArcStandard> {

	public DeterministicArcStandardShortestStackOracle() {
	}

	@Override
	public boolean canApply(int[][] tokens) {
		return is_projective(tokens, COL_HEAD);
	}
	
	public int next(ArcStandard sys) {
		/*
		 * based on Malt parser's implementation:
		 * http://grepcode.com/file/repo1.maven.org/maven2/org.maltparser/maltparser/1.8/org/maltparser/parser/algorithm/nivre/ArcStandardOracle.java#ArcStandardOracle
		 */
		boolean debug = false;
		int[][] tokens = sys.getTokens();
		ActionsInfo actions = sys.getInfo();
		List<Integer> buffer = sys.getBuffer();
		List<Integer> stack = sys.getStack();
		if (debug) {
			System.out.print(reversed(stack));
			System.out.println(buffer);
			for (int i = 1; i < tokens.length; i++)
				System.out.format("%d->%d, ", i, tokens[i][COL_HEAD]);
			System.out.println();
			for (int i = 1; i < sys.getLinks().length; i++)
				System.out.format("%d->%d, ", i, sys.getLinks()[i][0]);
			System.out.println();
		}
		if (stack.size() < 2) {
			assert_(stack.get(0) == 0); // ROOT
			if (buffer.size() <= 0) {
				return -1;
			} else {
				if (debug) System.out.println("shift");
				return unique(actions.type2actions(ArcStandard.ATYPE_SHIFT));
			}
		} else {
			int s1 = stack.get(0), s2 = stack.get(1);
			if (tokens[s2][COL_HEAD] == s1) {
				if (debug) System.out.println("left");
				return actions.label2action(tokens[s2][COL_LABEL], ArcStandard.ATYPE_LEFT);
			} else if (tokens[s1][COL_HEAD] == s2
					&& checkRightDependency(tokens, sys.getLinks(), s1)) {
				if (debug) System.out.println("right");
				return actions.label2action(tokens[s1][COL_LABEL], 
						ArcStandard.ATYPE_RIGHT, ArcStandard.ATYPE_RIGHT_ROOT);
			} else if (buffer.size() >= 1) {
				if (debug) System.out.println("shift");
				return unique(actions.type2actions(ArcStandard.ATYPE_SHIFT));
			} else {
				throw new RuntimeException(
						"You shouldn't see this! Probably unprojective tree was used.");
			}
		}
	}

	static boolean checkRightDependency(int[][] tokens, int[][] sysLinks, int v) {
		int l = tokens.length;
		assert_(l == sysLinks.length);
		int goldRightChild = DependencyUtils.getRightDependent(tokens, v, 1, COL_HEAD);
		int sysRightChild = DependencyUtils.getRightDependent(sysLinks, v, 1, 0);
		if (goldRightChild < 0) {
			return true;
		} else if (sysRightChild >= 0) {
			if (goldRightChild == sysRightChild) {
				return true;
			}
		}
		return false;
	}
}