package nl.cltl.ulm4.dep.oracle;

import java.util.ArrayList;
import java.util.List;

import nl.cltl.ulm4.dep.TransitionSystem;

public abstract class StaticOracle<T extends TransitionSystem> {

	public boolean canApply(int[][] tokens) {
		return true;
	}
	
	public void reset(int[][] tokens) { }
	
	public abstract int next(T sys);
	
	public List<Integer> run(T sys, int[][] tokens) {
		List<Integer> actions = new ArrayList<>(tokens.length*2);
		sys.reset(tokens);
		this.reset(tokens);
		while (!sys.isTerminated()) {
			int action = this.next(sys);
			if (action >= 0) {
				sys.execute(action);
			}
			actions.add(action);
		}
		return actions;
	}

	
}
