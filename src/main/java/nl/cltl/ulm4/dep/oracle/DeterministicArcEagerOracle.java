package nl.cltl.ulm4.dep.oracle;

import static nl.cltl.ulm4.dep.DependencyUtils.is_projective;
import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.util.CollectionUtils.unique;

import java.util.List;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.ArcEager;

public class DeterministicArcEagerOracle extends StaticOracle<ArcEager> {

	@Override
	public boolean canApply(int[][] tokens) {
		return is_projective(tokens, COL_HEAD);
	}
	
	@Override
	public int next(ArcEager sys) {
		int[][] tokens = sys.getTokens();
		ActionsInfo actions = sys.getInfo();
		List<Integer> buffer = sys.getBuffer();
		List<Integer> stack = sys.getStack();
		if (buffer.size() >= 1) {
			int s0 = stack.get(0), b0 = buffer.get(0);
			if (tokens[s0][COL_HEAD] == b0) {
				return actions.label2action(tokens[s0][COL_LABEL], ArcEager.ATYPE_LEFT_ARC);
			} else if (tokens[b0][COL_HEAD] == s0) {
				return actions.label2action(tokens[b0][COL_LABEL],
						ArcEager.ATYPE_RIGHT_ARC, ArcEager.ATYPE_RIGHT_ARC_ROOT);
			} else {
				for (int k = 0; k < s0; k++) {
					if (tokens[b0][COL_HEAD] == k || tokens[k][COL_HEAD] == b0) {
						return unique(actions.type2actions(ArcEager.ATYPE_REDUCE));
					}
				}
				return unique(actions.type2actions(ArcEager.ATYPE_SHIFT));
			}
		} else {
			if (stack.size() >= 2) {
				return unique(actions.type2actions(ArcEager.ATYPE_REDUCE));
			} else {
				return -1;
			}
		}
	}

}
