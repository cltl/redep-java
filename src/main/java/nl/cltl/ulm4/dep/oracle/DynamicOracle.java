package nl.cltl.ulm4.dep.oracle;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.TransitionSystem;

public interface DynamicOracle {
	
	int loss(TransitionSystem sys);
	
	int unlabeledLoss(TransitionSystem sys);

	int getZeroLossAction(TransitionSystem sys, int[][] gold_links, ActionsInfo info);


}
