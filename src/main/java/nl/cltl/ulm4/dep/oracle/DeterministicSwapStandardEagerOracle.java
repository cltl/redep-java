package nl.cltl.ulm4.dep.oracle;

import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.util.CollectionUtils.unique;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.reversed;

import java.util.List;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.DependencyUtils;
import nl.cltl.ulm4.dep.SwapStandard;

public class DeterministicSwapStandardEagerOracle extends StaticOracle<SwapStandard> {

	private int[] order;
	private int[] goldCount;
	private int[] sysCount;

	@Override
	public void reset(int[][] tokens) {
		order = DependencyUtils.getTreeInOrder(tokens, COL_HEAD);
		goldCount = DependencyUtils.countChildren(tokens, COL_HEAD);
		sysCount = new int[tokens.length];
	}

	@Override
	public int next(SwapStandard sys) {
		boolean debug = false;
		int[][] tokens = sys.getTokens();
		ActionsInfo actions = sys.getInfo();
		List<Integer> buffer = sys.getBuffer();
		List<Integer> stack = sys.getStack();
		if (debug) {
			System.out.print(reversed(stack));
			System.out.println(buffer);
			for (int i = 1; i < tokens.length; i++)
				System.out.format("%d->%d, ", i, tokens[i][COL_HEAD]);
			System.out.println();
			for (int i = 1; i < sys.getLinks().length; i++)
				System.out.format("%d->%d, ", i, sys.getLinks()[i][0]);
			System.out.println();
		}
		if (stack.size() < 2) {
			assert_(stack.get(0) == 0); // ROOT
			if (buffer.size() <= 0) {
				return -1;
			} else {
				if (debug) System.out.println("shift");
				return unique(actions.type2actions(SwapStandard.ATYPE_SHIFT));
			}
		} else {
			int s1 = stack.get(0), s2 = stack.get(1);
			if (tokens[s2][COL_HEAD] == s1 && sysCount[s2] == goldCount[s2]) {
				if (debug) System.out.println("left");
				sysCount[s1]++;
				return actions.label2action(tokens[s2][COL_LABEL], SwapStandard.ATYPE_LEFT);
			} else if (tokens[s1][COL_HEAD] == s2 && sysCount[s1] == goldCount[s1]) {
				if (debug) System.out.println("right");
				sysCount[s2]++;
				return actions.label2action(tokens[s1][COL_LABEL], 
						SwapStandard.ATYPE_RIGHT, SwapStandard.ATYPE_RIGHT_ROOT);
			} else if (order[s1] < order[s2]) {
				if (debug) System.out.println("swap");
				return unique(actions.type2actions(SwapStandard.ATYPE_SWAP));
			} else if (buffer.size() >= 1) {
				if (debug) System.out.println("shift");
				return unique(actions.type2actions(SwapStandard.ATYPE_SHIFT));
			} else {
				throw new RuntimeException("Don't know what to do.");
			}
		}
	}

}
