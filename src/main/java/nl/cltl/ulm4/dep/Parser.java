package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_POS;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_WORD;
import static nl.cltl.ulm4.dep.DependencyUtils.newVocabularies;
import static nl.cltl.ulm4.nlp.Dataset.COL_LENGTH;
import static nl.cltl.ulm4.nlp.Dataset.COL_START;
import static nl.cltl.ulm4.nlp.Dataset.COL_WORD;
import static nl.cltl.ulm4.util.Pythonic.assert_;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.Reader;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.GZIPInputStream;

import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.FileHandler;

import nl.cltl.ulm4.nlp.CoNLL;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nlp.FeatureExtractor;
import nl.cltl.ulm4.nlp.Vocabulary;
import nl.cltl.ulm4.nn.ChenManningNeuralNetwork;
import nl.cltl.ulm4.util.IntPair;
import nl.cltl.ulm4.util.Tensors;
import nl.cltl.ulm4.dep.oracle.*;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

public class Parser<T extends TransitionSystem> implements Serializable {

	/**
	 * 
	 */
	private static final Logger logger = Logger.getLogger(Parser.class.getName());
	
	private static final long serialVersionUID = 1L;
	
	String name;
	ChenManningFeatureExtractor featureExtractor;
	public Vocabulary[] vocabs;
	ChenManningNeuralNetwork mlp;
	private Supplier<T> transitionSystemSupplier;
	
	private transient ThreadLocal<Worker> workerLocal;
	
	private DynamicOracle myOracle;
	
	@Parameter(names={"-beamSize"}, description="number of parses to explore concurrently")
	private int beamSize = 1;
	
	@Parameter(names={"-model"}, required=true,
			description="path to model of a parser (Stanford or Java format)")
	private String modelPath;

	@Parameter(names={"-input"}, required=true, description="path to input file")
	private String inputPath;

	@Parameter(names={"-output"}, required=true, description="path to output file")
	private String outputPath;

	@Parameter(names={"-rootLabel"}, description="a string for root label, different when using LTH || Stanford dependencies")
	private String rootLabel = "root";
	
	//FIXME: this should be a boolean; default False, when given (without specification), True
	@Parameter(names={"-errorProp"}, description="an option that calls the algorithm for evaluating error propagation")
	private String errorProp = "off";
	
	@Parameter(names={"-printTrace"}, description="print the transitions and confidence")
	private boolean printTrace = false;
	
	private Parser() { 
	}
	
	public Parser(String name, Supplier<T> transitionSystemSupplier, 
			ChenManningFeatureExtractor featureExtractor, 
			Vocabulary[] vocabs, ChenManningNeuralNetwork mlp) {
		super();
		this.name = name;
		this.transitionSystemSupplier = transitionSystemSupplier;
		this.featureExtractor = featureExtractor;
		this.vocabs = vocabs;
		this.mlp = mlp;
	}

	public static Parser<ArcStandard> load_stanford_model(String path,
			String rootLabel) throws IOException {
		if (path.endsWith(".gz")) {
			try (InputStream fileStream = new FileInputStream(path);
					InputStream gzipStream = new GZIPInputStream(fileStream);
					Reader decoder = new InputStreamReader(gzipStream, "UTF-8");
					BufferedReader buffered = new BufferedReader(decoder);) {
				return load_stanford_model(buffered, path, rootLabel);
			}
		} else {
			try (InputStream fileStream = new FileInputStream(path);
					Reader decoder = new InputStreamReader(fileStream, "UTF-8");
					BufferedReader buffered = new BufferedReader(decoder);) {
				return load_stanford_model(buffered, path, rootLabel);
			}
		}
	}

	private static Parser<ArcStandard> load_stanford_model(BufferedReader f,
			String path, String rootLabel) throws IOException {
		Parser<ArcStandard> parser = new Parser<>();
		parser.name = new File(path).getName();
		long start = System.currentTimeMillis();
		System.out.format("Loading depparse model file: %s \n", path);
		String s = f.readLine();
		int nDict = Integer.parseInt(s.substring(s.indexOf("=") + 1));
		s = f.readLine();
		int nPOS = Integer.parseInt(s.substring(s.indexOf("=") + 1));
		s = f.readLine();
		int nLabel = Integer.parseInt(s.substring(s.indexOf("=") + 1));
		s = f.readLine();
		int eSize = Integer.parseInt(s.substring(s.indexOf("=") + 1));
		s = f.readLine();
		int hSize = Integer.parseInt(s.substring(s.indexOf("=") + 1));
		s = f.readLine();
		int nTokens = Integer.parseInt(s.substring(s.indexOf("=") + 1));
		s = f.readLine();
		int nPreComputed = Math.min(10000, Integer.parseInt(s.substring(s.indexOf('=') + 1)));

		parser.featureExtractor = new ChenManningFeatureExtractor();
		parser.mlp = new ChenManningNeuralNetwork(nDict + nPOS + nLabel, eSize,
				nTokens, hSize, nLabel * 2 - 1);

		parser.vocabs = newVocabularies();
		ArrayList<String> knownLabels = new ArrayList<>();
		double[][] E = parser.mlp.getEmbeddingWeights();

		for (int k = 0; k < nDict; k++) {
			s = f.readLine();
			String[] splits = s.split(" ");
			int index = parser.vocabs[VOCAB_WORD].getIndex(
					translate_magic_words(splits[0]));
			assert_(splits.length - 1 == eSize);
			for (int i = 0; i < eSize; i++) {
				E[index][i] = Double.parseDouble(splits[i + 1]);
			}
		}
		for (int k = 0; k < nPOS; k++) {
			s = f.readLine();
			String[] splits = s.split(" ");
			int index = parser.vocabs[VOCAB_POS].getIndex(
					translate_magic_words(splits[0]));
			assert_(splits.length - 1 == eSize);
			for (int i = 0; i < eSize; i++) {
				E[index][i] = Double.parseDouble(splits[i + 1]);
			}
		}
		for (int k = 0; k < nLabel; k++) {
			s = f.readLine();
			String[] splits = s.split(" ");
			String label = translate_magic_words(splits[0]);
			int index = parser.vocabs[VOCAB_LABEL].getIndex(label);
			knownLabels.add(label);
			assert_(splits.length - 1 == eSize);
			for (int i = 0; i < eSize; i++) {
				E[index][i] = Double.parseDouble(splits[i + 1]);
			}
		}
		assert_(Vocabulary.max_index(parser.vocabs) + 1 == E.length);

		double[][] W1 = parser.mlp.getHiddenLayerWeight();
		for (int j = 0; j < W1[0].length; j++) {
			s = f.readLine();
			String[] splits = s.split(" ");
			assert_(splits.length == W1.length);
			for (int i = 0; i < W1.length; i++) {
				W1[i][j] = Double.parseDouble(splits[i]);
			}
		}

		double[] b1 = parser.mlp.getHiddenyLayerBias();
		s = f.readLine();
		String[] splits = s.split(" ");
		assert_(splits.length == b1.length);
		for (int i = 0; i < b1.length; i++) {
			b1[i] = Double.parseDouble(splits[i]);
		}

		double[][] W2 = parser.mlp.getOutputWeight();
		for (int j = 0; j < W2[0].length; j++) {
			s = f.readLine();
			splits = s.split(" ");
			assert_(splits.length == W2.length);
			for (int i = 0; i < W2.length; i++) {
				W2[i][j] = Double.parseDouble(splits[i]);
			}
		}

		List<IntPair> preComputed = new ArrayList<>(nPreComputed);
		while (preComputed.size() < nPreComputed) {
			s = f.readLine();
			splits = s.split(" ");
			for (String split : splits) {
				int fIndex = Integer.parseInt(split);
				int tok = fIndex / nTokens;
				int pos = fIndex % nTokens;
				preComputed.add(new IntPair(tok, pos));
			}
		}
		parser.mlp.setPrecomputedFeatures(preComputed);
		
		// equivalent to makeTransitions()
		ActionsInfo info = ArcStandard.buildActionsInfo(knownLabels, rootLabel,
				parser.vocabs[VOCAB_LABEL]);
		// System.out.println(vocabs.label)
		for (Vocabulary vocab : parser.vocabs) {
			vocab.seal();
		}
		parser.mlp.setEligibleActionsMap(ArcStandard.eligibleMap(info));
		parser.transitionSystemSupplier = 
				(Supplier<ArcStandard> & Serializable)
				(() -> new ArcStandard(info));

		System.out.format("Done loading (%.2f s).\n",
				(System.currentTimeMillis() - start) / 1000.0);
		return parser;
	}

	private static String translate_magic_words(String s) {
		s = s.replace("-NULL-", "__NONE__");
		s = s.replace("-UNKNOWN-", "__MISSING__");
		s = s.replace("-ROOT-", "__ROOT__");
		return s;
	}

	public int[][] predict(Dataset ds) {
		return predict(ds, false, false);
	}

	public int[][] predict(Dataset ds, boolean printTrace, boolean errorEval) {
		if (workerLocal == null) {
			workerLocal = new ThreadLocal<Parser<T>.Worker>() {
				@Override
				protected Worker initialValue() {
					return newWorker();
				}
			};
		}
		int[][] output = new int[ds.tokens.length][];
		IntStream.range(0, ds.sents.length).parallel().forEach(
				s -> workerLocal.get().predict(ds, s, output));
		if (errorEval) {
			myOracle = new DynamicArcStandardOracle();
			IntStream.range(0, ds.sents.length).forEach(
					s -> workerLocal.get().call_walkthrough_error_propagation(ds, s, output));
		} 
		if (printTrace) {
			myOracle = new DynamicArcStandardOracle();
			IntStream.range(0, ds.sents.length).forEach(
					s -> workerLocal.get().printTrace(ds, s, output));
		} 
		return output;
	}
	
	public Worker newWorker() {
		return new Worker(mlp.newWorker());
	}
	
	public class Worker {
		int[] x = new int[featureExtractor.num()];
		TransitionSystem sys = transitionSystemSupplier.get();
		ChenManningNeuralNetwork.Worker mlpWorker;

		Worker(ChenManningNeuralNetwork.Worker mlpWorker) {
			this.mlpWorker = mlpWorker;
		}

		public int step() {
			featureExtractor.extract(x, sys, vocabs);	
			int action = mlpWorker.forward(x, sys.state());
//			logProb += Math.log(mlpWorker.probs[0][action]);
			sys.execute(action);
			return action;
		}
		
		void predict(Dataset ds, int s, int[][] output) {
			int start = ds.sents[s][COL_START];
			int num = ds.sents[s][COL_LENGTH];
			int[][] tokens = Tensors.narrow(ds.tokens, start, num);
			sys.reset(tokens);
//			double logProb = 0;
			while (!sys.isTerminated()) { 
				step();
			}
//			System.out.println(Math.exp(logProb));
			System.arraycopy(sys.getLinks(), 0, output, start, num);
		}
		
		void printSentence(int[][] tokens) {
			for (int i = 0; i < tokens.length; i++) {
				System.out.print(vocabs[VOCAB_WORD].getWord(tokens[i][COL_WORD]));
				System.out.print(' ');
			}
			System.out.println();
		}
		
		void printTrace(Dataset ds, int s, int[][] output) {
			int start = ds.sents[s][COL_START];
			int num = ds.sents[s][COL_LENGTH];
			int[][] tokens = Tensors.narrow(ds.tokens, start, num);
			printSentence(tokens);
			sys.reset(tokens);
			int known_loss = 0;
			int wrongActions = 0;
			List<String> actionStrs = new ArrayList<>();
			while (!sys.isTerminated()) {
				featureExtractor.extract(x, sys, vocabs);	
				int action = mlpWorker.forward(x, sys.state());
				double prob = mlpWorker.probs[0][action];
				sys.execute(action);
				int foundLoss = myOracle.loss(sys);
				int lossChange = known_loss - foundLoss;
				if (lossChange != 0) wrongActions += 1;
				known_loss = foundLoss;
				String actionStr = sys.info.vocab.getWord(action);
				actionStrs.add(actionStr);
				System.out.format("%d\t%s\t%d\t%.4f\n", actionStrs.size(), actionStr, lossChange, prob);
			}
			System.out.format("System transitions: %s\n", 
					actionStrs.stream().map(str -> "S".equals(str) ? "'S'" : 
					"['" + str.replaceAll("\\((\\w+)\\)", "', '$1") + "']")
					.collect(Collectors.joining(", ")));
			if (known_loss > 0) {
				actionStrs = new ArrayList<>();
				sys.reset(tokens);
				while (!sys.isTerminated()) {
					int action = myOracle.getZeroLossAction(sys, tokens, sys.info);
					sys.execute(action);
					String actionStr = sys.info.vocab.getWord(action);
					actionStrs.add(actionStr);
				}
				System.out.format("Gold transitions: %s\n", 
						actionStrs.stream().map(str -> "S".equals(str) ? "'S'" : 
						"['" + str.replaceAll("\\((\\w+)\\)", "', '$1") + "']")
						.collect(Collectors.joining(", ")));
			}
			System.out.format("Total loss: %d (%d times) --> Accuracy: %.2f%%\n", 
					known_loss, wrongActions, (1-known_loss/((double)tokens.length-1))*100);
			System.out.println();
		}
		
		void call_walkthrough_error_propagation(Dataset ds, int s, int[][] output){
			List<Integer> fixes = new Vector<Integer>();
			this.walktrough_error_propagation(ds, s, output, fixes);
		}
		
		void walktrough_error_propagation(Dataset ds, int s, int[][] output, List<Integer> fixes) {
			int start = ds.sents[s][COL_START];
			int num = ds.sents[s][COL_LENGTH];
			int[][] tokens = Tensors.narrow(ds.tokens, start, num);
			sys.reset(tokens);
			int counter = 0;
			int known_loss = 0;
			//registers the point in the process where errors are made
			List<Integer> identified_errors = new Vector<Integer>();
			//registers the height of each increase in loss while processing
			List<List<Integer>> loss_registration = new Vector<List<Integer>>();
			while (!sys.isTerminated()) {
				counter += 1;
				featureExtractor.extract(x, sys, vocabs);
				int action = mlpWorker.forward(x, sys.state());
				
				if (fixes.contains(counter)) {
					action = myOracle.getZeroLossAction(sys, tokens, sys.info);
					sys.execute(action);
					System.out.println("Fixed: " + counter + " in sentence " + s);
				} else {
					sys.execute(action);
					System.out.println("Carried out action: " + counter + " for sentence" + s);
					int foundLoss = myOracle.loss(sys);
					if (foundLoss > known_loss) {
						System.out.println("unlabelledLoss: " + myOracle.unlabeledLoss(sys) + " " + counter + " " + s);
						identified_errors.add(counter);
						List<Integer> lossAndLocation = new Vector<Integer>();
						lossAndLocation.add(counter);
						lossAndLocation.add(foundLoss);
						loss_registration.add(lossAndLocation);
						known_loss = foundLoss;
					}
				}

			}
			System.out.println(s + "\t" + loss_registration.toString());
			if (myOracle.loss(sys) > 0) {
				//System.out.println("Still loss present " + counter);
				for (int actionNr : identified_errors) {
					if (!fixes.contains(actionNr)) {
						fixes.add(actionNr);
						break;
					}
				}
				//System.out.println("Start:" + start + " Sentence: " + s + " fixes: " + fixes.toString());
				this.walktrough_error_propagation(ds, s, output, fixes);
			}
			System.arraycopy(sys.getLinks(), 0, output, start, num);
		}
		
		public TransitionSystem getTransitionSystem() {
			return sys;
		}
	}
	
	public FeatureExtractor<TransitionSystem> getFeatureExtractor() {
		return featureExtractor;
	}

	public Vocabulary[] getVocabs() {
		return vocabs;
	}

	public ChenManningNeuralNetwork getNeuralNetwork() {
		return mlp;
	}

	public void setNeuralNetwork(ChenManningNeuralNetwork mlp) {
		this.mlp = mlp;
	}

	public String getName() {
		return name;
	}
	
	public T newTransitionSystem() {
		return transitionSystemSupplier.get();
	}
	
	@SuppressWarnings("unchecked")
	private void run() throws IOException, ClassNotFoundException {
		Parser<? extends TransitionSystem> parser;
		if (modelPath.endsWith(".txt") || modelPath.endsWith(".txt.gz")) {
			parser = Parser.load_stanford_model(modelPath, rootLabel);
		} else {
			try (FileInputStream fis = new FileInputStream(modelPath);
					ObjectInputStream ois = new ObjectInputStream(fis);) {
				parser = (Parser<TransitionSystem>) ois.readObject();
			}
		}
	
		parser.mlp.evaluate(); // affects dropout layer, if present;
		System.out.println("Network: " + parser.mlp);
	
		CoNLL conll = new CoNLL();
		conll.vocabs = parser.vocabs;
		int max_rows = (int) (Files.lines(Paths.get(inputPath)).count() + 1);
		Dataset ds = conll.build_dataset(inputPath, "", max_rows);
		// for debugging
	//	ds.sents = Tensors.narrow(ds.sents, 0, 1);
	
		System.out.println("Parsing...");
		long start_time = System.currentTimeMillis();
		parser.getNeuralNetwork().updatePrecomputedVectors();

		boolean errorEval = false;
		if(errorProp.equals("on"))
		{
			errorEval = true;
			//myOracle = new DynamicArcStandardOracle(); 
		}
		int[][] links = parser.predict(ds, printTrace, errorEval);
		double elapsedTime = (System.currentTimeMillis() - start_time) / 1000.0;
		System.out.format("Parsing... Done (%.2f s, %.2f sentences/s).\n",
				elapsedTime, ds.sents.length / elapsedTime);
	
		conll.substitue_dependency(inputPath, outputPath, ds, links);
		System.out.println("Parsing results written to " + outputPath);
	}

	private void InitiateLogger() throws SecurityException, IOException{
		FileHandler fh = new FileHandler("logger.txt");  
		logger.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();  
		fh.setFormatter(formatter);  
	}
	
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) throws Exception {
		System.out.format("Command line parameters: %s\n", Arrays.toString(args));
		Parser parser = new Parser();
		parser.InitiateLogger();
		new JCommander(parser, args);
		parser.run();
	}

}
