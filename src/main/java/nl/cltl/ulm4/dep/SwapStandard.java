package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.error;

import java.util.Collections;
import java.util.List;

import nl.cltl.ulm4.nlp.Vocabulary;

public class SwapStandard extends TransitionSystem {

	public static final int ATYPE_LEFT = 0;
	public static final int ATYPE_RIGHT = 1;
	public static final int ATYPE_RIGHT_ROOT = 2;
	public static final int ATYPE_SHIFT = 3;
	public static final int ATYPE_SWAP = 4;
	
	public static final int STATE_TERMINAL = -1;
	public static final int STATE_LEFT_RIGHT_SHIFT = 0;
	public static final int STATE_RIGHT_ROOT_SHIFT = 1;
	public static final int STATE_RIGHT_ROOT = 2;
	public static final int STATE_LEFT_RIGHT = 3;
	public static final int STATE_LEFT_RIGHT_SHIFT_SWAP = 4;
	public static final int STATE_LEFT_RIGHT_SWAP = 5;
	public static final int STATE_SHIFT = 6;

	SwapStandard() {
		// for cloning
	}
	
	public SwapStandard(ActionsInfo actionsInfo) {
		this.info = actionsInfo;
	}

	public SwapStandard(ActionsInfo actionsInfo, int[][] tokens) {
		this(actionsInfo);
		reset(tokens);
	}

	@Override
	public boolean isTerminated() {
		return state() == STATE_TERMINAL;
	}
	
	public int state() {
		if (buffer.size() >= 1) {
			if (stack.size() > 2) {
				// see Nivre (2009) page 354
				// Nivre, J. (2009). Non-projective Dependency Parsing in Expected Linear Time. In Proceedings of the Joint Conference of the 47th Annual Meeting of the ACL and the 4th International Joint Conference on Natural Language Processing of the AFNLP: Volume 1 - Volume 1 (pp. 351–359). Stroudsburg, PA, USA: Association for Computational Linguistics.
				int s0 = stack.get(0), s1 = stack.get(1);
				if (s0 > s1 && s1 != 0) { // original order and not root
					return STATE_LEFT_RIGHT_SHIFT_SWAP;
				} else {
					return STATE_LEFT_RIGHT_SHIFT;
				}
			} else if (stack.size() == 2) {
				return STATE_RIGHT_ROOT_SHIFT;
			} else {
				return STATE_SHIFT;
			}
		} else {
			if (stack.size() > 2) {
				// see Nivre (2009) page 354
				// Nivre, J. (2009). Non-projective Dependency Parsing in Expected Linear Time. In Proceedings of the Joint Conference of the 47th Annual Meeting of the ACL and the 4th International Joint Conference on Natural Language Processing of the AFNLP: Volume 1 - Volume 1 (pp. 351–359). Stroudsburg, PA, USA: Association for Computational Linguistics.
				int s0 = stack.get(0), s1 = stack.get(1);
				if (s0 > s1 && s1 != 0) { // original order and not root
					return STATE_LEFT_RIGHT_SWAP;
				} else {
					return STATE_LEFT_RIGHT;
				}
			} else if (stack.size() == 2) {
				assert_(stack.get(0) != 0 && stack.get(1) == 0,
						"Expecting root followed by something else in the stack");
				return STATE_RIGHT_ROOT;
			} else {
				return STATE_TERMINAL;
			}
		}
	}

	public SwapStandard execute(int a) {
		if (info.isType(a, ATYPE_LEFT)) {
			assert_(stack.size() >= 2);
			int s1 = stack.get(0), s2 = stack.get(1);
			links[s2][0] = s1;
			links[s2][1] = info.action2label(a);
			stack.remove(1);
		} else if (info.isType(a, ATYPE_RIGHT) || info.isType(a, ATYPE_RIGHT_ROOT)) {
			assert_(stack.size() >= 2);
			int s1 = stack.get(0), s2 = stack.get(1);
			links[s1][0] = s2;
			links[s1][1] = info.action2label(a);
			stack.remove(0);
		} else if (info.isType(a, ATYPE_SWAP)) {
			assert_(stack.size() >= 2);
			assert_(stack.get(1) != 0);
			assert_(stack.get(0) > stack.get(1));
			buffer.add(0, stack.remove(1));
		} else if (info.isType(a, ATYPE_SHIFT)) {
			assert_(buffer.size() >= 1);
			stack.add(0, buffer.remove(0));
		} else {
			error("Unsupported action: " + a);
		}
		actions.add(a);
		return this;
	}

	public List<Integer> get_executed_actions() {
		return Collections.unmodifiableList(actions);
	}
	

	public static ActionsInfo buildActionsInfo(Vocabulary labelVocab, String root_dep_label) {
		return buildActionsInfo(labelVocab.word2index.keySet(), root_dep_label, labelVocab);
	}

	public static ActionsInfo buildActionsInfo(Iterable<String> labels, String rootLabel, 
			Vocabulary labelVocab) {
		ActionsInfo.Builder builder = new ActionsInfo.Builder(labelVocab);
	    for (String label : labels) {
	        if (!label.equals("__NONE__")) {
	            builder.register(label, "L(" + label + ")", ATYPE_LEFT);
	        }
	    }
	    boolean rootFound = false;
	    for (String label : labels) {
	        if (!label.equals("__NONE__")) {
	        	if (label.equals(rootLabel)) {
	        		builder.register(label, "R(" + label + ")", ATYPE_RIGHT_ROOT);
	        		rootFound = true;
	        	} else {
	        		builder.register(label, "R(" + label + ")", ATYPE_RIGHT);
	        	}
	        }
	    }
	    assert_(rootFound, "Can't find " + rootLabel + ". Wrong root label, maybe?");
	    builder.register(null, "SH", ATYPE_SHIFT);
	    builder.register(null, "SW", ATYPE_SWAP);
	    return builder.build();
	}

	public static int[][] eligibleMap(ActionsInfo info) {
		int[][] map = new int[7][];
		map[STATE_LEFT_RIGHT_SHIFT] = info.types2actionsArr(ATYPE_LEFT, ATYPE_RIGHT, ATYPE_SHIFT);
		map[STATE_SHIFT] = info.types2actionsArr(ATYPE_SHIFT);
		map[STATE_LEFT_RIGHT] = info.types2actionsArr(ATYPE_LEFT, ATYPE_RIGHT);
		map[STATE_RIGHT_ROOT_SHIFT] = info.types2actionsArr(ATYPE_RIGHT_ROOT, ATYPE_SHIFT);
		map[STATE_RIGHT_ROOT] = info.types2actionsArr(ATYPE_RIGHT_ROOT);
		map[STATE_LEFT_RIGHT_SHIFT_SWAP] = info.types2actionsArr(ATYPE_LEFT, ATYPE_RIGHT, ATYPE_SHIFT, ATYPE_SWAP);
		map[STATE_LEFT_RIGHT_SWAP] = info.types2actionsArr(ATYPE_LEFT, ATYPE_RIGHT, ATYPE_SWAP);
		return map;
	}

	@Override
	public TransitionSystem execute_alternative(int action, boolean first) {
		throw new UnsupportedOperationException();
	}
	

	@Override
	public TransitionSystem execute_identified(int action, int first) {
		throw new UnsupportedOperationException();
	}
}