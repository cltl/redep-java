package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.defaultdict;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.cltl.ulm4.dep.oracle.StaticOracle;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nlp.FeatureExtractor;
import nl.cltl.ulm4.nlp.Vocabulary;
import nl.cltl.ulm4.util.IntPair;
import nl.cltl.ulm4.util.Tensors;

public class SupervisedDataset {
	
	public int[][] x;
	public int[] states;
	public int[] y;
	
	public List<IntPair> frequentFeatures;
	
	public SupervisedDataset() {
	}
	
	public static <S extends TransitionSystem> SupervisedDataset build_dataset(
			Dataset ds, StaticOracle<S> oracle, S sys, int[][] eligibleActionsMap,
			FeatureExtractor<? super S> feature_extractor, String name, int max_rows,
			Vocabulary[] input_vocabs) {
        if (name == null) name = "[noname]";
        System.out.format("Corpus to training examples (dataset \"%s\")...\n", name);
        SupervisedDataset sds = new SupervisedDataset();
        long start = System.currentTimeMillis();
        Map<IntPair, Integer> counter = defaultdict(() -> 0);
        sds.x = new int[max_rows][feature_extractor.num()];
        sds.states = new int[max_rows];
        sds.y = new int[max_rows];
        int count = 0;
        int rejectedCount = 0;
        List<Set<Integer>> eligibleActionsSets = new ArrayList<>();
        for (int[] indices : eligibleActionsMap) {
        	Set<Integer> s = new HashSet<Integer>();
        	for (int i : indices) s.add(i);
			eligibleActionsSets.add(s);
		}
        for (int s = 0; s < ds.sents.length; s++) {
            int[][] tokens = Tensors.narrow(ds.tokens, ds.sents[s][1], ds.sents[s][2]);
            if (oracle.canApply(tokens)) {
                assert_(tokens[0][COL_HEAD] == -1); // first token must be root
                sys.reset(tokens);
                oracle.reset(tokens);
                int action = oracle.next(sys);
                while (action >= 0) {
                	assert_(eligibleActionsSets.get(sys.state()).contains(action));
                	
                    feature_extractor.extract(sds.x[count], sys, input_vocabs);
                    sds.states[count] = sys.state();
                    sds.y[count] = action;               
                    sys.execute(action);
                    action = oracle.next(sys);

                    for (int i = 0; i < sds.x[count].length; i++) {
						IntPair f = new IntPair(sds.x[count][i], i);
						counter.put(f, counter.get(f) + 1);
					}
                    count += 1;
                }
                for (int i = 1; i < tokens.length; i++) {
                	assert_(tokens[i][COL_HEAD] == sys.links[i][0] &&
                			tokens[i][COL_LABEL] == sys.links[i][1], 
                			"Wrong oracle sequence");
                }
            } else {
            	rejectedCount++;
            }
        } // of for
        sds.x = Tensors.narrow(sds.x, 0, count);
        sds.states = Tensors.narrow(sds.states, 0, count);
        sds.y = Tensors.narrow(sds.y, 0, count);
        sds.frequentFeatures = new ArrayList<IntPair>(counter.keySet());
        Collections.sort(sds.frequentFeatures , 
        		(a, b) -> Integer.compare(counter.get(b), counter.get(a)));
        long stop = System.currentTimeMillis();
        if (rejectedCount > 0) {
			System.out.format("Rejected %d out of %d sentences.\n", rejectedCount, ds.sents.length);
		}
        System.out.format("Corpus to training examples (dataset \"%s\")... "
        		+ "Done (%d examples, %d s).\n",
                name, sds.y.length, (stop-start)/1000);
        return sds;
    }

}
