package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.error;
import nl.cltl.ulm4.nlp.Vocabulary;

/**
 * Implement the algorithm in Nivre & Fernández-González (2014)
 * 
 * Nivre, J., & Fernández-González, D. (2014). Arc-eager Parsing with the Tree
 * Constraint. Computational Linguistics, 40(2), 259–267.
 * doi:10.1162/COLI_a_00185
 *
 */
public class ArcEagerWithTreeConstraint extends ArcEager {

	public static final int ATYPE_UNSHIFT = 5;
	
	public static final int STATE_RIGHT_REDUCE = 4;
	public static final int STATE_LEFT_RIGHT = 5;
	public static final int STATE_RIGHT_ROOT = 6;
	public static final int STATE_UNSHIFT = 7;
	
	private boolean e;

	ArcEagerWithTreeConstraint() { 
		// for cloning
	}
	
	public ArcEagerWithTreeConstraint(ActionsInfo actionsInfo) {
		this.info = actionsInfo;
	}

	public ArcEagerWithTreeConstraint(ActionsInfo actionsInfo, int[][] tokens) {
		this(actionsInfo);
		reset(tokens);
	}
	
	@Override
	public TransitionSystem reset(int[][] tokens) {
		e = false;
		return super.reset(tokens);
	}
	
	@Override
	public TransitionSystem copy() {
		ArcEagerWithTreeConstraint copy = (ArcEagerWithTreeConstraint) super.copy();
		copy.e = e;
		return copy;
	}

	@Override
	public boolean isTerminated() {
		return state() == STATE_TERMINAL;
	}

	public int state() {
		if (buffer.size() >= 1) {
			if (stack.size() >= 2) {
				if (links[stack.get(0)][0] >= 0) {
					if (e) {
						return STATE_RIGHT_REDUCE;
					} else {
						return STATE_RIGHT_REDUCE_SHIFT;
					}
				} else {
					if (e) {
						return STATE_LEFT_RIGHT;
					} else {
						return STATE_LEFT_RIGHT_SHIFT;
					}
				}
			} else {
				assert_(stack.size() == 1 && stack.get(0) == 0,
						"Root disappear! Where's my root???");
				if (e) {
					return STATE_RIGHT_ROOT;
				} else {
					return STATE_RIGHT_ROOT_SHIFT;
				}
			}
		} else {
			if (stack.size() >= 2) {
				if (links[stack.get(0)][0] >= 0) {
					return STATE_REDUCE;
				} else {
					return STATE_UNSHIFT;
				}
			} else {
				assert_(stack.size() == 1 && stack.get(0) == 0,
						"Root disappear! Where's my root???");
				assert(e);
				return STATE_TERMINAL;
			}
		}
	}

	public TransitionSystem execute(int a) {
		if (info.isType(a, ATYPE_LEFT_ARC)) {
			assert_(stack.size() >= 1);
			int s0 = stack.get(0);
			int b0 = buffer.get(0);
			links[s0][0] = b0;
			links[s0][1] = info.action2label(a);
			stack.remove(0);
		} else if (info.isType(a, ATYPE_RIGHT_ARC) || 
				info.isType(a, ATYPE_RIGHT_ARC_ROOT)) {
			assert_(stack.size() >= 1);
			int s0 = stack.get(0);
			int b0 = buffer.get(0);
			links[b0][0] = s0;
			links[b0][1] = info.action2label(a);
			stack.add(0, buffer.remove(0));
			if (buffer.isEmpty()) e = true;
		} else if (info.isType(a, ATYPE_REDUCE)) {
			assert_(stack.size() >= 2, "Can't reduce root.");
			stack.remove(0);
		} else if (info.isType(a, ATYPE_SHIFT)) {
			assert_(buffer.size() >= 1, "The last word is already consumed.");
			assert_(!e);
			stack.add(0, buffer.remove(0));
			if (buffer.isEmpty()) e = true;
		} else if (info.isType(a, ATYPE_UNSHIFT)) {
			assert_(buffer.size() == 0, "Buffer must be empty.");
			assert_(stack.size() >= 2, "Stack must have non-root element.");
			assert_(e);
			buffer.add(0, stack.remove(0));
		} else {
			error("Unsupported action: " + a);
		}
		actions.add(a);
		return this;
	}
	
	public static ActionsInfo buildActionsInfo(Vocabulary labelVocab, String root_dep_label) {
		return buildActionsInfo(labelVocab.word2index.keySet(), root_dep_label, labelVocab);
	}

	public static ActionsInfo buildActionsInfo(Iterable<String> labels, String rootLabel, 
			Vocabulary labelVocab) {
		ActionsInfo.Builder builder = new ActionsInfo.Builder(labelVocab);
	    registerActionsInfo(builder, labels, rootLabel);
	    builder.register(null, "UN", ATYPE_UNSHIFT);
	    return builder.build();
	}

	public static int[][] eligibleMap(ActionsInfo info) {
		int[][] map = new int[8][];
		map[STATE_RIGHT_REDUCE_SHIFT] = info.types2actionsArr(
				ATYPE_RIGHT_ARC, ATYPE_REDUCE, ATYPE_SHIFT);
		map[STATE_LEFT_RIGHT_SHIFT] = info.types2actionsArr(
				ATYPE_LEFT_ARC, ATYPE_RIGHT_ARC, ATYPE_SHIFT);
		map[STATE_RIGHT_ROOT_SHIFT] = info.types2actionsArr(
				ATYPE_RIGHT_ARC_ROOT, ATYPE_SHIFT);
		map[STATE_RIGHT_REDUCE] = info.types2actionsArr(
				ATYPE_RIGHT_ARC, ATYPE_REDUCE);
		map[STATE_LEFT_RIGHT] = info.types2actionsArr(
				ATYPE_LEFT_ARC, ATYPE_RIGHT_ARC);
		map[STATE_RIGHT_ROOT] = info.types2actionsArr(
				ATYPE_RIGHT_ARC_ROOT);
		map[STATE_REDUCE] = info.types2actionsArr(ATYPE_REDUCE);
		map[STATE_UNSHIFT] = info.types2actionsArr(ATYPE_UNSHIFT);
		return map;
	}
	
}