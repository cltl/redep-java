package nl.cltl.ulm4.dep;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import nl.cltl.ulm4.nlp.Vocabulary;
import nl.cltl.ulm4.util.Tensors;

public abstract class TransitionSystem {

	ActionsInfo info;
	List<Integer> actions = new ArrayList<>();
	List<Integer> stack = new ArrayList<>();
	List<Integer> buffer = new ArrayList<>();
	int[][] tokens;
	int[][] links;

	public abstract boolean isTerminated();
	
	public abstract int state();
	
	public abstract TransitionSystem execute(int action);

	public abstract TransitionSystem execute_alternative(int action, boolean first);

	public abstract TransitionSystem execute_identified(int action, int identified);

	public TransitionSystem copy() {
		try {
			TransitionSystem new_sys= getClass().newInstance();
			new_sys.info = info;
			new_sys.actions = new LinkedList<>(actions);
			new_sys.stack = new LinkedList<>(stack);
			new_sys.buffer = new LinkedList<>(buffer);
			new_sys.tokens = tokens;
			new_sys.links = Tensors.clone(links);
			return new_sys;
		} catch (InstantiationException|IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public TransitionSystem reset(int[][] tokens) {
		this.tokens = tokens;
		actions.clear();
		stack.clear();
		stack.add(0, 0);
		buffer.clear();
		IntStream.range(1, tokens.length).forEach(i -> buffer.add(i));
		links = new int[tokens.length][2];
		Tensors.fill(links, Integer.MIN_VALUE); // important for dynamic oracle
		Arrays.fill(links[0], -1); // root
		return this;
	}

	public List<Integer> get_executed_actions() {
		return Collections.unmodifiableList(actions);
	}

	public List<Integer> getBuffer() {
		return buffer;
	}
	
	public List<Integer> getStack() {
		return stack;
	}

	public ActionsInfo getInfo() {
		return info;
	}
	
	public int[][] getTokens() {
		return tokens;
	}
	
	public int[][] getLinks() {
		return links;
	}


	
}
