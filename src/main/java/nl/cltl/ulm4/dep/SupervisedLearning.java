package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_WORD;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.tmpname;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import nl.cltl.ulm4.dep.oracle.DeterministicArcEagerOracle;
import nl.cltl.ulm4.dep.oracle.DeterministicArcStandardShortestStackOracle;
import nl.cltl.ulm4.dep.oracle.DeterministicSwapStandardEagerOracle;
import nl.cltl.ulm4.dep.oracle.StaticOracle;
import nl.cltl.ulm4.nlp.CoNLL;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nlp.Vocabulary;
import nl.cltl.ulm4.nn.ChenManningNeuralNetwork;
import nl.cltl.ulm4.util.CollectionUtils;
import nl.cltl.ulm4.util.IntPair;
import nl.cltl.ulm4.util.Tensors;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

public class SupervisedLearning {

	private int trainingBatchSize = 10000;
	private double adaAlpha = 0.01;
	private double adaEps = 1e-6;
	private double initRange = 0.01;
	private double dropProb = 0.5;
	private double regularizationWeight = 1e-8;
	private Random random = new Random(234081570532L);
	private SupervisedDataset batch = new SupervisedDataset();
	private int nPrecompute = 100000;
	private int evalPerIter = 100;
	private Evaluator evaluator;
	
	@Parameter(names={"-less-output"}, description="print less output")
	private boolean lessOutput = false;
	
	@Parameter(names={"-maxIters"}, 
			description="number of iterations (updates) to perform")
	private int maxIters = 20000;
	
	@Parameter(names={"-parser"}, required=true, 
			description="path that the resulting parser will be written to")
	private String parserPath;
	
	@Parameter(names={"-train"}, required=true, description="path to training dataset")
	private String trainPath;
	
	@Parameter(names={"-valid"}, required=true, description="path to valid dataset")
	private String validPath;
	
	@Parameter(names={"-rootLabel"}, description="a string for root label (default: root)")
	private String rootLabel = "root";
	
	@Parameter(names={"-transition"}, description="type of transition system (default: arc-standard)")
	private String transitionType = "arc-standard";
	
	@Parameter(names={"-embed"}, description="path to embedding file")
	private String embedFile = "embeddings/merged.txt";
	
	@Parameter(names={"-measure"}, description="measure of goodness (uas or las)")
	private String measure = "uas";

	public SupervisedLearning() {
	}

	public void readEmbedFile(String embedFile, Vocabulary wordVocab,
			double[][] E) throws IOException {
		if (embedFile != null) {
			Map<String, Integer> embedID = new HashMap<>();
			List<String> lines = Files.readAllLines(Paths.get(embedFile));

			int nWords = lines.size();
			String[] splits = lines.get(0).split("\\s+");

			int dim = splits.length - 1;
			System.err.println("Embedding File " + embedFile + ": #Words = "
					+ nWords + ", dim = " + dim);
			assert_(dim == E[0].length, "Pretrained vectors and embedding matrix are of different dimensionality.");
			double[][] pretrainedVectors = new double[lines.size()][dim];
			
			for (String line : lines) {
				splits = line.split("\\s+");
				String word = splits[0];
				int index = embedID.size();
				embedID.put(word, index);
				for (int j = 0; j < dim; ++j) {
					pretrainedVectors[index][j] = Double.parseDouble(splits[j + 1]);
				}
			}
			Tensors.scale(pretrainedVectors);

			int foundEmbed = 0;
			for (String word : wordVocab.word2index.keySet()) {
				int index = -1;
				if (embedID.containsKey(word)) index = embedID.get(word);
				else if (embedID.containsKey(word.toLowerCase())) 
					index = embedID.get(word.toLowerCase());
				if (index >= 0) {
					System.arraycopy(pretrainedVectors[index], 0, 
							E[wordVocab.getIndex(word)], 0, E[0].length);
					foundEmbed ++;
				}				
			}
			System.err.println("Found embeddings: " + foundEmbed + " / "
					+ wordVocab.size());
		}
	}

	public <T extends TransitionSystem> void train_parser(Parser<T> parser,
			Dataset train_ds, Dataset valid_ds, StaticOracle<T> oracle, 
			int[][] eligibleActionsMap) throws IOException {
		assert_(adaAlpha > 0);
		assert_(adaEps > 0);
		System.out.format("Training %s...\n", parser.getName());
		batch.x = new int[trainingBatchSize][];
		batch.states = new int[trainingBatchSize];
		batch.y = new int[trainingBatchSize];
		File best_path = tmpname();
		long start = System.currentTimeMillis();
		int example_count = 0;
		double best_score = Double.MIN_VALUE;
		int maxRows = train_ds.tokens.length * 2;
		if (oracle instanceof DeterministicSwapStandardEagerOracle) {
			maxRows = (int)(train_ds.tokens.length * 2.5);
		}

		SupervisedDataset ds = SupervisedDataset.build_dataset(train_ds,
				oracle, parser.newTransitionSystem(), eligibleActionsMap,
				parser.getFeatureExtractor(), "train",
				maxRows, parser.getVocabs());
		List<IntPair> toPrecompute = new ArrayList<>( // for serializability
				ds.frequentFeatures.subList(0, nPrecompute));
		ChenManningNeuralNetwork mlp = parser.getNeuralNetwork();
		mlp.lessOutput = lessOutput;
		mlp.initParameters(random, initRange);
		readEmbedFile(embedFile, parser.getVocabs()[VOCAB_WORD], mlp.getEmbeddingWeights());
		mlp.precompute(toPrecompute);
		mlp.initTraining(random, dropProb, regularizationWeight, 
				mlp.new AdaGrad(adaAlpha, adaEps), 1);
		int[] indices = IntStream.range(0, ds.y.length).toArray();
		for (int iterCount = 0; iterCount < maxIters; iterCount++) {
			if (!lessOutput || iterCount % evalPerIter == 0 || iterCount >= maxIters-1) {
				System.out.format("######## Iteration %d\n", iterCount);
			}
			CollectionUtils.randomizeFirstN(indices, trainingBatchSize, random);
			for (int i = 0; i < trainingBatchSize; i++) {
				batch.x[i] = ds.x[indices[i]];
				batch.states[i] = ds.states[indices[i]];
				batch.y[i] = ds.y[indices[i]];
			}
			mlp.training(); // affects dropout layer, if present;
			mlp.trainBatch(batch);
			mlp.updatePrecomputedVectors();
			example_count += trainingBatchSize;
			long elapsedTime = (System.currentTimeMillis() - start);
			double speed = example_count / (double) elapsedTime;

			if (iterCount % evalPerIter == 0 || iterCount >= maxIters-1) {
				mlp.evaluate(); // affects dropout layer, if present;
				double score = evaluator.parse_and_measure(valid_ds, parser);
				System.out.format("%s on development set: %.3f%%\n", 
						evaluator.getName(), score * 100);
				if (score > best_score) {
					System.out.format("Writing best model to %s... ", best_path);
					try (FileOutputStream fos = new FileOutputStream(best_path);
							ObjectOutputStream oos = new ObjectOutputStream(fos);) {
						oos.writeObject(mlp);
					}
					best_score = score;
					System.out.print("Done.\n");
				}
			}

			if (!lessOutput || iterCount % evalPerIter == 0 || iterCount >= maxIters-1) { 
				System.out.format("Finished iter %d (%.2f min, %.2fk examples/s).\n",
						iterCount, elapsedTime / 1000.0 / 60.0, speed);
			}
		}

		System.out.format("Loading from %s... ", best_path);
		try (FileInputStream fis = new FileInputStream(best_path);
				ObjectInputStream ois = new ObjectInputStream(fis);) {
			try {
				parser.setNeuralNetwork((ChenManningNeuralNetwork) ois.readObject());
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Unlikely exception", e);
			}
		}
		System.out.print("Done.\n");

		double elapsedTimeMin = (System.currentTimeMillis() - start) / 1000.0 / 60.0;
		System.out.format("Training %s... Done (%.2f min).\n",
				parser.getName(), elapsedTimeMin);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void run() throws IOException {
		CoNLL conll = new CoNLL();
		int max_rows = (int) (Files.lines(Paths.get(trainPath)).count() + 1);
		conll.prepare(trainPath, 0, "train", max_rows);
		Dataset train_ds = conll.build_dataset(trainPath, "train", max_rows);
		// for debugging
		// ds.sents = Tensors.narrow(ds.sents, 0, 1);
		max_rows = (int) (Files.lines(Paths.get(validPath)).count() + 1);
		Dataset valid_ds = conll.build_dataset(validPath, "valid", max_rows);

		ChenManningFeatureExtractor featureExtractor = 
				new ChenManningFeatureExtractor();
		ActionsInfo info;
		Supplier tss;
		StaticOracle oracle;
		int[][] eligibleActionsMap;
		if ("arc-standard".equals(transitionType)) {
			info = ArcStandard.buildActionsInfo(
					conll.vocabs[VOCAB_LABEL], rootLabel);
			tss = (Supplier<ArcStandard> & Serializable)
					(() -> new ArcStandard(info));
			oracle = new DeterministicArcStandardShortestStackOracle();
			eligibleActionsMap = ArcStandard.eligibleMap(info);
		} else if ("swap-standard".equals(transitionType)) {
			info = SwapStandard.buildActionsInfo(
					conll.vocabs[VOCAB_LABEL], rootLabel);
			tss = (Supplier<SwapStandard> & Serializable)
					(() -> new SwapStandard(info));
			oracle = new DeterministicSwapStandardEagerOracle();
			eligibleActionsMap = SwapStandard.eligibleMap(info);
		} else if ("arc-eager".equals(transitionType)) {
			info = ArcEagerWithTreeConstraint.buildActionsInfo(
					conll.vocabs[VOCAB_LABEL], rootLabel);
			tss = (Supplier<ArcEagerWithTreeConstraint> & Serializable)
					(() -> new ArcEagerWithTreeConstraint(info));
			oracle = new DeterministicArcEagerOracle();
			eligibleActionsMap = ArcEagerWithTreeConstraint.eligibleMap(info);
		} else {
			throw new IllegalArgumentException("Unknown transition type: "
					+ transitionType);
		}
		ChenManningNeuralNetwork mlp = new ChenManningNeuralNetwork(
				Vocabulary.max_index(conll.vocabs) + 1, 50,
				featureExtractor.num(), 200, info.vocab.size());
		mlp.setEligibleActionsMap(eligibleActionsMap);
		Parser parser = new Parser("chen-manning", tss,
				featureExtractor, conll.vocabs, mlp);

		if ("las".equals(measure)) {
			evaluator = new Evaluator.LAS(false);
		} else if ("uas".equals(measure)) {
			evaluator = new Evaluator.UAS(false);
		} else {
			throw new IllegalArgumentException("Unknown measure: " + measure);
		}
		train_parser(parser, train_ds, valid_ds, oracle, eligibleActionsMap);
		try (FileOutputStream fos = new FileOutputStream(parserPath);
				ObjectOutputStream oos = new ObjectOutputStream(fos);) {
			oos.writeObject(parser);
		}
		System.out.println("Parser written to " + parserPath);
	}

	public static void main(String[] args) throws IOException {
		System.out.format("Command line parameters: %s\n", Arrays.toString(args));
		SupervisedLearning sup = new SupervisedLearning();
		new JCommander(sup, args);
		sup.run();
	}

}
