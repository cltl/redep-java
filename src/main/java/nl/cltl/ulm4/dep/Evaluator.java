package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_POS;
import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.nlp.Dataset.COL_POS;

import java.util.stream.IntStream;

import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nlp.Vocabulary;

public abstract class Evaluator {

	private boolean include_punc;
	// use the set of punctuation from Chen & Manning (2014)
	private String[] punctuations = { "``", "''", ".", ",", ":" };

	public Evaluator(boolean include_punc) {
		super();
		this.include_punc = include_punc;
	}

	protected int matchRowLAS(int[] gold, int[] output) {
		return gold[COL_HEAD] == output[0] && gold[COL_LABEL] == output[1] ? 1 : 0;
	}

	protected int matchRowUAS(int[] gold, int[] output) {
		return gold[COL_HEAD] == output[0] ? 1 : 0;
	}
	
	protected IntStream createIndices(int[][] tokens, Vocabulary posVocab) {
		IntStream indices = IntStream.range(0, tokens.length);
		indices = indices.filter(i -> (tokens[i][COL_HEAD] != -1)); // not root
		if (!include_punc) {
			for (String p : punctuations) {
				int index = posVocab.getIndex(p);
				indices = indices.filter(i -> (tokens[i][COL_POS] != index));
			}
		}
		return indices;
	}

	public abstract String getName();
	
	public abstract double parse_and_measure(Dataset ds, 
			Parser<? extends TransitionSystem> parser);
	
	public abstract int score(int tokens[][], int[][] links, Vocabulary posVocab);
	
	public static class UAS extends Evaluator {

		public UAS(boolean includePunc) {
			super(includePunc);
		}
		
		@Override
		public double parse_and_measure(Dataset ds, 
				Parser<? extends TransitionSystem> parser) {
			int[][] output = parser.predict(ds);
			IntStream indices = createIndices(ds.tokens, parser.getVocabs()[VOCAB_POS]);
			return indices.map(i -> matchRowUAS(ds.tokens[i], output[i])).average().getAsDouble();
		}

		@Override
		public int score(int[][] tokens, int[][] links, Vocabulary posVocab) {
			IntStream indices = createIndices(tokens, posVocab);
			return indices.map(i -> matchRowUAS(tokens[i], links[i])).sum();
		}

		@Override
		public String getName() {
			return "UAS";
		}
		
	}
	
	public static class LAS extends	Evaluator {
	
		public LAS(boolean includePunc) {
			super(includePunc);
		}

		@Override
		public double parse_and_measure(Dataset ds, 
				Parser<? extends TransitionSystem> parser) {
			int[][] output = parser.predict(ds);
			IntStream indices = createIndices(ds.tokens, parser.getVocabs()[VOCAB_POS]);
			return indices.map(i -> matchRowLAS(ds.tokens[i], output[i])).average().getAsDouble();
		}
	
		@Override
		public int score(int tokens[][], int[][] links, Vocabulary posVocab) {
			IntStream indices = createIndices(tokens, posVocab);
			return indices.map(i -> matchRowLAS(tokens[i], links[i])).sum();
		}

		@Override
		public String getName() {
			return "LAS";
		}


	}

}
