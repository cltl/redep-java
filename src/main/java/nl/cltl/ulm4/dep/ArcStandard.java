package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.error;
import nl.cltl.ulm4.nlp.Vocabulary;

public class ArcStandard extends TransitionSystem {

	public static final int ATYPE_LEFT = 0;
	public static final int ATYPE_RIGHT = 1;
	public static final int ATYPE_RIGHT_ROOT = 2;
	public static final int ATYPE_SHIFT = 3;
	
	public static final int STATE_TERMINAL = -1;
	public static final int STATE_LEFT_RIGHT_SHIFT = 0;
	public static final int STATE_RIGHT_ROOT_SHIFT = 1;
	public static final int STATE_RIGHT_ROOT = 2;
	public static final int STATE_LEFT_RIGHT = 3;
	public static final int STATE_SHIFT = 4;
	
	ArcStandard() {
		// for cloning
	}

	public ArcStandard(ActionsInfo actionsInfo) {
		this.info = actionsInfo;
	}

	public ArcStandard(ActionsInfo actionsInfo, int[][] tokens) {
		this(actionsInfo);
		reset(tokens);
	}

	@Override
	public boolean isTerminated() {
		return state() == STATE_TERMINAL;
	}

	public int state() {
		if (buffer.size() >= 1) {
			if (stack.size() > 2) {
				return STATE_LEFT_RIGHT_SHIFT;
			} else if (stack.size() == 2) {
				assert_(stack.get(0) != 0 && stack.get(1) == 0,
						"Expecting root followed by something else in the stack");
				return STATE_RIGHT_ROOT_SHIFT;
			} else {
				return STATE_SHIFT;
			}
		} else {
			if (stack.size() > 2) {
				return STATE_LEFT_RIGHT;
			} else if (stack.size() == 2) {
				assert_(stack.get(0) != 0 && stack.get(1) == 0,
						"Expecting root followed by something else in the stack");
				return STATE_RIGHT_ROOT;
			} else {
				return STATE_TERMINAL;
			}
		}
	}

	public TransitionSystem execute(int a) {
		
		if (info.isType(a, ATYPE_LEFT)) {
			assert_(stack.size() >= 2);
			int s1 = stack.get(0), s2 = stack.get(1);
			links[s2][0] = s1;
			links[s2][1] = info.action2label(a);
			stack.remove(1);
			
		} else if (info.isType(a, ATYPE_RIGHT) || info.isType(a, ATYPE_RIGHT_ROOT)) {
			assert_(stack.size() >= 2);
			int s1 = stack.get(0), s2 = stack.get(1);
			links[s1][0] = s2;
			links[s1][1] = info.action2label(a);
			stack.remove(0);

		} else if (info.isType(a, ATYPE_SHIFT)) {
			assert_(buffer.size() >= 1);
			stack.add(0, buffer.remove(0));
		} else {
			error("Unsupported action: " + a);
		}
		actions.add(a);
		return this;
	}

	
	public TransitionSystem execute_identified(int a, int identified){
		
		if (identified == ATYPE_LEFT){
			assert_(stack.size() >= 2);
			int s1 = stack.get(0), s2 = stack.get(1);
			links[s2][0] = s1;
			//default everywhere
			links[s2][1] = 0;
			stack.remove(1);
		} else if (identified == ATYPE_RIGHT){
			assert_(stack.size() >= 2);
			int s1 = stack.get(0), s2 = stack.get(1);
			links[s1][0] = s2;
			links[s1][1] = info.action2label(a);
			stack.remove(0);
		} else if (identified == ATYPE_SHIFT){

			assert_(buffer.size() >= 1);
			stack.add(0, buffer.remove(0));
		}
		//TODO: fix me, record actual action
		actions.add(a);
		
		return this;
	}
	
	public TransitionSystem execute_alternative(int a, boolean first)
	{
		if (info.isType(a, ATYPE_LEFT)){
			//first try to make arc in other direction
			if (first)
			{
				System.out.println("Actually executing ATYPE_RIGHT");
				assert_(stack.size() >= 2);
				int s1 = stack.get(0), s2 = stack.get(1);
				links[s1][0] = s2;
				//FIXME: we'll just do unlabeled ones for now (this now retrieves old label, which will certainly be wrong)
				links[s1][1] = info.action2label(a);
				stack.remove(0);
			}
			//if also increase in loss; shift
			else{
				assert_(buffer.size() >= 1);
				stack.add(0, buffer.remove(0));

				System.out.println("Actually executing SHIFT (2nd)");
				}
		}
		else if (info.isType(a, ATYPE_RIGHT) || info.isType(a, ATYPE_RIGHT_ROOT))
		{
			//first try to make arc in other direction
			if (first)
			{
				assert_(stack.size() >= 2);
				int s1 = stack.get(0), s2 = stack.get(1);
				links[s2][0] = s1;
				links[s2][1] = info.action2label(a);
				stack.remove(1);
				System.out.println("Actually executing ATYPE_LEFT");
			}
			else
			{
				assert_(buffer.size() >= 1);
				stack.add(0, buffer.remove(0));

				System.out.println("Actually executing SHIFT (2nd)");
			}
		}
		else if (info.isType(a, ATYPE_SHIFT)) {
			if (first)
			{
				assert_(stack.size() >= 2);
				int s1 = stack.get(0), s2 = stack.get(1);
				links[s2][0] = s1;
				links[s2][1] = 0;
				stack.remove(1);
				System.out.println("Actually executing ATYPE_LEFT");
			}
			else
			{
				assert_(stack.size() >= 2);
				int s1 = stack.get(0), s2 = stack.get(1);
				links[s1][0] = s2;
				//FIXME: we'll just do unlabeled ones for now (this now retrieves old label, which will certainly be wrong
				links[s1][1] = 0;
				stack.remove(0);
				System.out.println("Actually executing ATYPE_RIGHT (2nd)");
			}
		} 
		else 
		{
				error("Unsupported action: " + a);
		}
		
		
		return this;
	}
	
	
	public static ActionsInfo buildActionsInfo(Vocabulary labelVocab, String root_dep_label) {
		return buildActionsInfo(labelVocab.word2index.keySet(), root_dep_label, labelVocab);
	}

	public static ActionsInfo buildActionsInfo(Iterable<String> labels, String rootLabel, 
			Vocabulary labelVocab) {
		ActionsInfo.Builder builder = new ActionsInfo.Builder(labelVocab);
		// keep the same order as makeTransitions in Stanford's parser 
	    for (String label : labels) {
	        if (!label.equals("__NONE__")) {
	            builder.register(label, "L(" + label + ")", ATYPE_LEFT);
	        }
	    }
	    boolean rootFound = false;
	    for (String label : labels) {
	        if (!label.equals("__NONE__")) {
	        	if (label.equals(rootLabel)) {
	        		builder.register(label, "R(" + label + ")", ATYPE_RIGHT_ROOT);
	        		rootFound = true;
	        	} else {
	        		builder.register(label, "R(" + label + ")", ATYPE_RIGHT);
	        	}
	        }
	    }
	    assert_(rootFound, "Can't find " + rootLabel + ". Wrong root label, maybe?");
	    builder.register(null, "S", ATYPE_SHIFT);
	    return builder.build();
	}

	public static int[][] eligibleMap(ActionsInfo info) {
		int[][] map = new int[5][];
		map[STATE_LEFT_RIGHT_SHIFT] = info.types2actionsArr(ATYPE_LEFT, ATYPE_RIGHT, ATYPE_SHIFT);
		map[STATE_SHIFT] = info.types2actionsArr(ATYPE_SHIFT);
		map[STATE_LEFT_RIGHT] = info.types2actionsArr(ATYPE_LEFT, ATYPE_RIGHT);
		map[STATE_RIGHT_ROOT_SHIFT] = info.types2actionsArr(ATYPE_RIGHT_ROOT, ATYPE_SHIFT);
		map[STATE_RIGHT_ROOT] = info.types2actionsArr(ATYPE_RIGHT_ROOT);
		return map;
	}
	
}