package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.util.CollectionUtils.toArray;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.defaultdict;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import nl.cltl.ulm4.nlp.Vocabulary;

public class ActionsInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Vocabulary vocab;
	private Map<Integer, List<Integer>> type2actions;
	private Map<Integer, Integer> action2type;
	private Map<Integer, Integer> action2label;
	private Map<Integer, Map<Integer, Integer>> label2action;

	public ActionsInfo() {
		super();
	}
	
	public ActionsInfo(Vocabulary vocab,
			Map<Integer, List<Integer>> type2actions,
			Map<Integer, Integer> action2type,
			Map<Integer, Integer> action2label,
			Map<Integer, Map<Integer, Integer>> label2action) {
		super();
		this.vocab = vocab;
		this.type2actions = type2actions;
		this.action2type = action2type;
		this.action2label = action2label;
		this.label2action = label2action;
	}

	public Vocabulary getVocabulary() {
		return vocab;
	}
	
	public boolean isType(int action, int type) {
		Integer myType = action2type.get(action);
		return myType != null && myType.equals(type);
	}
	
	public Integer action2type(int action){
		return action2type.get(action);
	}
	
	public int[] types2actionsArr(int... types) {
		List<Integer> actions = new ArrayList<>();
		for (int type : types) {
			actions.addAll(type2actions(type));
		}
		return toArray(actions);
	}
	
	public List<Integer> type2actions(int type) {
		return type2actions.get(type);
	}
	
	public Integer action2label(int action) {
		return action2label.get(action);
	}
	
	public int label2action(int label, int... types) {
		Integer action = null;
		for (int type : types) {
			Integer foundAction = label2action.get(label).getOrDefault(type, null);
			if (foundAction != null) {
				if (action == null) {
					action = foundAction;
				} else {
					throw new IllegalStateException("More than one actions found.");
				}
			}
		}
		if (action == null) {
			throw new RuntimeException("No such action: label=" + label + 
					", types=" + Arrays.toString(types));
		}
		return action;
	}
	
	public static class Builder {

		private Vocabulary labelVocab;
		private Vocabulary actionVocab = new Vocabulary();
		private Map<Integer, List<Integer>> type2actions = defaultdict(
				(Supplier<List<Integer>>) 
				(Supplier<List<Integer>> & Serializable) () -> new ArrayList<>());
		private Map<Integer, Integer> action2type = new HashMap<>();
		private Map<Integer, Integer> action2label = new HashMap<>();
		private Map<Integer, Map<Integer, Integer>> label2action = defaultdict(
				(Supplier<Map<Integer, Integer>>) 
				(Supplier<Map<Integer, Integer>> & Serializable) () -> new HashMap<>());
		private boolean sealed;
		
		public Builder(Vocabulary labelVocab) {
			this.labelVocab = labelVocab;
		}
		
		public Builder register(String label, String action, int type) {
			assert_(!sealed, "This builder is already sealed.");
			int actionIndex = actionVocab.getIndex(action);
			if (label == null) {
	            type2actions.get(type).add(actionIndex);
	            action2type.put(actionIndex, type);
			} else {
				int labelIndex = labelVocab.getIndex(label);
				label2action.get(labelIndex).put(type, actionIndex);
	            type2actions.get(type).add(actionIndex);
	            action2type.put(actionIndex, type);
	            action2label.put(actionIndex, labelIndex);
			}
            return this;
		}
		
		public ActionsInfo build() {
			sealed = true;
			return new ActionsInfo(actionVocab, type2actions, action2type,
					action2label, label2action);
		}
		
	}
	
}