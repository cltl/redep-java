package nl.cltl.ulm4.dep.re;

import static nl.cltl.ulm4.nlp.Dataset.COL_LENGTH;
import static nl.cltl.ulm4.nlp.Dataset.COL_START;

import java.util.function.IntUnaryOperator;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import nl.cltl.ulm4.dep.Parser;
import nl.cltl.ulm4.dep.TransitionSystem;
import nl.cltl.ulm4.dep.oracle.StaticOracle;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.util.Tensors;

public class OracleOnlySampler<T extends TransitionSystem> extends Sampler<T> {

	private Supplier<StaticOracle<T>> oracleSupplier;
	private ThreadLocal<StaticOracle<T>> oracleLocal = new ThreadLocal<StaticOracle<T>>() {
		protected nl.cltl.ulm4.dep.oracle.StaticOracle<T> initialValue() {
			return oracleSupplier.get();
		};
	};

	public OracleOnlySampler(Supplier<StaticOracle<T>> oracleSupplier, 
			Parser<T> parser, int trainingBatchSize) {
		super(parser, 1, trainingBatchSize);
		this.oracleSupplier = oracleSupplier;
	}

    public void sample(Dataset ds) {
        reset(ds);
		IntStream.range(0, ds.sents.length).parallel()
				.map(new IntUnaryOperator() {
			
			@Override
			public int applyAsInt(int s) {
	            int tokenStart = ds.sents[s][COL_START];
				int tokenCount = ds.sents[s][COL_LENGTH];
				int[][] tokens = Tensors.narrow(ds.tokens, tokenStart, tokenCount);
            	T sys = parser.newTransitionSystem();
            	oracleLocal.get().run(sys, tokens);
                int reward = tokens.length - 1;
                convert(reward, s, tokens, sys.get_executed_actions());
                baselines.put(s, 0.5 * (tokens.length - 1));
				return s;
			}
		}).count();
    }


}
