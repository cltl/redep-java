package nl.cltl.ulm4.dep.re;

import static nl.cltl.ulm4.nlp.Dataset.COL_LENGTH;
import static nl.cltl.ulm4.util.Pythonic.assert_;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import nl.cltl.ulm4.dep.Parser;
import nl.cltl.ulm4.dep.SupervisedDataset;
import nl.cltl.ulm4.dep.TransitionSystem;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.util.Tensors;

public abstract class Sampler<T extends TransitionSystem> {

	public static final int COL_REWARD = 0;
	public static final int COL_SENTENCE_ID = 1;
	public static final int COL_ACTIONS_START = 2;
	public static final int COL_ACTIONS_LENGTH = 3;

	protected int sampleSize;
	private SupervisedDataset dataset;
	private AtomicInteger actionCount = new AtomicInteger();
	private List<int[]> outputParses = Collections.synchronizedList(new ArrayList<>());
	protected Map<Integer, Double> baselines = Collections.synchronizedMap(new HashMap<>());
	private int trainingBatchSize;
	protected Parser<T> parser;
	private int max_rows;

	public Sampler(Parser<T> parser, int sampleSize, int trainingBatchSize) {
        this.parser = parser;
        this.sampleSize = sampleSize;
		this.trainingBatchSize = trainingBatchSize;
        dataset = new SupervisedDataset();
    }

    void reset(Dataset ds) {
    	int maxTokens = Math.min(ds.tokens.length,
    			trainingBatchSize * Tensors.maxOfColumn(ds.sents, COL_LENGTH));
		max_rows = maxTokens * 2 * sampleSize;
		if (dataset.y == null || max_rows > dataset.y.length) {
			dataset.x = new int[max_rows][parser.getFeatureExtractor().num()];
			dataset.y = new int[max_rows];
			dataset.states = new int[max_rows];
		}
        outputParses.clear();
        actionCount.set(0);
    }

    /** 
     * Convert actions into training examples. This method is threadsafe.
     * @param reward
     * @param sentenceID 
     * @param tokens
     * @param executed_actions
     */
	public void convert(int reward, int sentenceID, int[][] tokens,
			List<Integer> executed_actions) {
		T transSystem = parser.newTransitionSystem();
		transSystem.reset(tokens);
		int startIndex = actionCount.getAndAdd(executed_actions.size());
		int index = startIndex;
		for (int action : executed_actions) {
			dataset.states[index] = transSystem.state();
			parser.getFeatureExtractor().extract(dataset.x[index],
					transSystem, parser.getVocabs());
			dataset.y[index] = action;
			transSystem.execute(action);
			index = index + 1;
		}
		assert_(transSystem.isTerminated(), "Unfinished parse");
		outputParses.add(new int[] { reward, sentenceID, startIndex, index-startIndex });
	}

    protected static <K extends TransitionSystem> int find_parse(K needle, List<K> haystack) {
        List<Integer> actions = needle.get_executed_actions();
        int index = 0;
        for (TransitionSystem parser : haystack) {
            if (parser.get_executed_actions().equals(actions)) {
                return index;
            }
            index++;
        }
        return -1;
    }

	public abstract void sample(Dataset ds);

	public int getActionCount() {
		return actionCount.get();
	}
	
	public SupervisedDataset getDataset() {
		return dataset;
	}
	
	public List<int[]> getOutputParses() {
		return outputParses;
	}

	public Map<Integer, Double> getBaselines() {
		return baselines;
	}

	public int getMaxRows() {
		return max_rows;
	}

}


