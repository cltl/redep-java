package nl.cltl.ulm4.dep.re;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_POS;
import static nl.cltl.ulm4.nlp.Dataset.COL_ID;
import static nl.cltl.ulm4.nlp.Dataset.COL_LENGTH;
import static nl.cltl.ulm4.nlp.Dataset.COL_START;
import static nl.cltl.ulm4.util.CollectionUtils.minIndexRandomTieBreaking;
import static nl.cltl.ulm4.util.Pythonic.defaultdict;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import nl.cltl.ulm4.dep.Evaluator;
import nl.cltl.ulm4.dep.Parser;
import nl.cltl.ulm4.dep.TransitionSystem;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.util.Tensors;

public class MemorySampler<T extends TransitionSystem> extends
		RandomSampler<T> {

	private Map<Integer, List<Integer>> rewards = 
			Collections.synchronizedMap(defaultdict(() -> new ArrayList<>()));
	private Map<Integer, List<List<Integer>>> actions =
			Collections.synchronizedMap(defaultdict(() -> new ArrayList<>()));
	private double forgetProb;
	private ThreadLocal<Worker> workerLocal = new ThreadLocal<Worker>() {
		@Override
		protected Worker initialValue() {
			return new Worker();
		}
	};

	public MemorySampler(Parser<T> parser, Evaluator evaluator, int sampleSize,
			int trainingBatchSize, double forgetProb) {
		super(parser, evaluator, sampleSize, trainingBatchSize);
		this.forgetProb = forgetProb;
	}

	public void sample(Dataset ds) {
		reset(ds);
        IntStream.range(0, ds.sents.length).parallel()
				.forEach(s -> workerLocal.get().sample(ds, s));
	}

	protected class Worker extends RandomSampler<T>.Worker {
		
		public void sample(Dataset ds, int s) {
			int[][] myTokens = Tensors.narrow(ds.tokens,
					ds.sents[s][COL_START], ds.sents[s][COL_LENGTH]);
			// initialize memory
			int sentID = ds.sents[s][COL_ID];
			List<Integer> myRewards = rewards.get(sentID);
			List<List<Integer>> myActions = actions.get(sentID);
			// forget memories with a given probability
			for (int k = myRewards.size() - 1; k >= 0; k--) {
				if (random.nextDouble() < forgetProb) {
					myRewards.remove(k);
					myActions.remove(k);
				}
			}
			// sample new parses
			List<T> parses = sample_without_replacement(myTokens);
			// remember new parses
			for (T parse : parses) {
				List<Integer> executed_actions = parse.get_executed_actions();
				if (myActions.indexOf(executed_actions) < 0) {
					int reward = evaluator.score(myTokens,
							parse.getLinks(), parser.getVocabs()[VOCAB_POS]);
					if (myRewards.size() < sampleSize) {
						myRewards.add(reward);
						myActions.add(executed_actions);
					} else {
						int minIndex = minIndexRandomTieBreaking(myRewards);
						if (reward >= myRewards.get(minIndex)) {
							myRewards.set(minIndex, reward);
							myActions.set(minIndex, executed_actions);
						}
					}
				}
			}
			// convert
			for (int i = 0; i < myRewards.size(); i++) {
				convert(myRewards.get(i), s, myTokens, myActions.get(i));
			}
			baselines.put(s, 0.5*(myTokens.length-1));
		}
		
	}
	
}
