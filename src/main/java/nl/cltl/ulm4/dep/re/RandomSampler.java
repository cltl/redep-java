package nl.cltl.ulm4.dep.re;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_POS;
import static nl.cltl.ulm4.nlp.Dataset.COL_LENGTH;
import static nl.cltl.ulm4.nlp.Dataset.COL_START;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import nl.cltl.ulm4.dep.Evaluator;
import nl.cltl.ulm4.dep.Parser;
import nl.cltl.ulm4.dep.TransitionSystem;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nn.ChenManningNeuralNetwork;
import nl.cltl.ulm4.util.CollectionUtils;
import nl.cltl.ulm4.util.IntPair;
import nl.cltl.ulm4.util.Tensors;

public class RandomSampler<T extends TransitionSystem> extends Sampler<T> {

	Evaluator evaluator;
	transient ThreadLocal<Worker> workerLocal = new ThreadLocal<Worker>() {
		@Override
		protected Worker initialValue() {
			return new Worker();
		}
	};
	
	public RandomSampler(Parser<T> parser, Evaluator evaluator, int sampleSize,
			int trainingBatchSize) {
		super(parser, sampleSize, trainingBatchSize);
		this.evaluator = evaluator;
	}

	public void sample(Dataset ds) {
		reset(ds);
        IntStream.range(0, ds.sents.length).parallel()
				.forEach(s -> workerLocal.get().sample(ds, s));
	}
	
	protected class Worker {
		
		private int[] x;
		protected Random random = new Random();
		private double[] parse_probs;
		private ChenManningNeuralNetwork.Worker nnWorker;
		private List<IntPair> alternatives = new ArrayList<>();
		private List<Double> probs = new ArrayList<>();

		public Worker() {
			x = new int[parser.getFeatureExtractor().num()];
			parse_probs = new double[sampleSize];
			nnWorker = parser.getNeuralNetwork().newWorker();
		}

		public void sample(Dataset ds, int s) {
			int[][] myTokens = Tensors.narrow(ds.tokens,
					ds.sents[s][COL_START], ds.sents[s][COL_LENGTH]);
			List<T> parses = sample_without_replacement(myTokens);
			for (T parse : parses) {
				int reward = evaluator.score(myTokens,
						parse.getLinks(), parser.getVocabs()[VOCAB_POS]);
				convert(reward, s, myTokens, parse.get_executed_actions());
			}
			baselines.put(s, 0.5*(myTokens.length-1));
		}

		@SuppressWarnings("unchecked")
		public List<T> sample_without_replacement(int[][] tokens) {
			int[][] eligibleActionsMap = parser.getNeuralNetwork().getMasks();
			List<T> parses = new ArrayList<>(sampleSize);
			List<T> new_parses = new ArrayList<>(sampleSize);
			List<T> terminatedParses = new ArrayList<>(sampleSize);
			parses.add((T) parser.newTransitionSystem().reset(tokens));
			parse_probs[0] = 1;
			while (!parses.isEmpty()) {
				alternatives.clear();
				probs.clear();
				for (int i = 0; i < parses.size(); i++) {
					T parse = parses.get(i);
					if (parse.isTerminated()) {
						terminatedParses.add(parse);
					} else {
						parser.getFeatureExtractor().extract(x, parse,
								parser.getVocabs());
						int state = parse.state();
						nnWorker.forward(x, state);
						for (int j : eligibleActionsMap[state]) {
							if (nnWorker.getProbs()[j] > 0) {
								alternatives.add(new IntPair(i, j));
								probs.add(parse_probs[i] * nnWorker.getProbs()[j]);
							}
						}
					}
				}
				new_parses.clear();
				if (!alternatives.isEmpty()) {
					int[] indices = CollectionUtils.sample(probs, sampleSize, random);
					Tensors.fill(parse_probs, 0);
					for (int j : indices) {
						int parent_index = alternatives.get(j).a;
						int action_index = alternatives.get(j).b;
						parse_probs[new_parses.size()] = probs.get(j);
						new_parses.add((T) parses.get(parent_index).copy().execute(action_index));
					}
					Tensors.div(parse_probs, Tensors.sum(parse_probs));
				}
				// switch
				List<T> temp = parses;
				parses = new_parses;
				new_parses = temp;
			}
			return terminatedParses;
		}
	}

}
