package nl.cltl.ulm4.dep.re;

import static nl.cltl.ulm4.dep.DependencyUtils.is_projective;
import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LENGTH;
import static nl.cltl.ulm4.nlp.Dataset.COL_START;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.tmpname;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import nl.cltl.ulm4.dep.ArcEager;
import nl.cltl.ulm4.dep.ArcStandard;
import nl.cltl.ulm4.dep.Evaluator;
import nl.cltl.ulm4.dep.Parser;
import nl.cltl.ulm4.dep.SwapStandard;
import nl.cltl.ulm4.dep.TransitionSystem;
import nl.cltl.ulm4.dep.oracle.DeterministicArcEagerOracle;
import nl.cltl.ulm4.dep.oracle.DeterministicArcStandardShortestStackOracle;
import nl.cltl.ulm4.dep.oracle.DeterministicSwapStandardEagerOracle;
import nl.cltl.ulm4.dep.oracle.StaticOracle;
import nl.cltl.ulm4.nlp.CoNLL;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nn.ChenManningNeuralNetwork;
import nl.cltl.ulm4.util.CollectionUtils;
import nl.cltl.ulm4.util.Tensors;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

public class ReinforcementLearning {

	static int trainingBatchSize = 512;
	private boolean includePunc = false;
	private Evaluator evaluator;
	private Random random = new Random();
	private File bestPath = tmpname();
	private int evalPerIter = 5;

	@Parameter(names={"-adaAlpha"}, description="initial learning rate")
	private double adaAlpha = 1e-3;

	@Parameter(names={"-adaEps"}, description="adaEps")
	private double adaEps = 1e-20;
	
	@Parameter(names={"-maxIters"}, description="maximum iterations (updates) to perform")
	private int maxIters = 1000;
	
	@Parameter(names={"-dropProb"}, description="probability of dropping a hidden node")
	private double dropProb = 0;
	
	@Parameter(names={"-l2"}, description="the strength of L2 regularization")
	private double regularizationWeight = 1e-8;
	
	@Parameter(names={"-measure"}, description="measure of goodness (uas or las)")
	private Object measure = "las";
	
	@Parameter(names={"-train"}, description="path to training dataset", required=true)
	private String trainPath;

	@Parameter(names={"-valid"}, description="path to valid dataset", required=true)
	private String validPath;
	
	@Parameter(names={"-pretrained"}, required=true, 
			description="path that a pretrained parser will be loaded from")
	private String pretrainedPath;
	
	@Parameter(names={"-rootLabel"}, description="a string for root label")
	private String rootLabel = "root";
	
	@Parameter(names={"-sampler"}, description="type of sampler to use")
	private String samplerName;
	
	@Parameter(names={"-sampleSize"}, description="number of samples for each sentence")
	private int sampleSize = 8;
	
	@Parameter(names={"-parser"}, required=true, 
			description="path that the resulting parser will be written to")
	private String parserPath;

	@Parameter(names={"-reinforce"}, description="mimic REINFOCE algorithm")
	private boolean reinforce = false;
	
	public <T extends TransitionSystem> void reinforce_parser(
			Parser<T> parser, Sampler<T> sampler, Dataset train_ds,
			Dataset valid_ds) throws IOException {
		System.out.println("Training by reinforcement learning...");

		int sentCount = 0;
		ChenManningNeuralNetwork mlp = parser.getNeuralNetwork();
		mlp.updatePrecomputedVectors();
		mlp.evaluate(); // affects dropout layer, if present;
		double supervisedScore = evaluator.parse_and_measure(valid_ds, parser);
		System.out.format("%s on development set before training: %f%%\n",
				evaluator.getName(), supervisedScore * 100);

		double bestScore = 0;
		mlp.initTraining(random, dropProb, regularizationWeight, 
				mlp.new AdaGrad(adaAlpha, adaEps), 500);
		int[][] projectiveSentences = Arrays
				.stream(train_ds.sents)
				.filter(sent -> is_projective(Tensors.narrow(train_ds.tokens,
						sent[COL_START], sent[COL_LENGTH]), COL_HEAD))
				.toArray(l -> new int[l][]);
		int[] indices = IntStream.range(0, projectiveSentences.length).toArray();
		Dataset batch = new Dataset(new int[trainingBatchSize][], train_ds.tokens);
		long start = System.currentTimeMillis();
		for (int iterCount = 0; iterCount < maxIters; iterCount++) {
			System.out.format("######## Iteration %d\n", iterCount);
			CollectionUtils.randomizeFirstN(indices, trainingBatchSize, random);
			for (int i = 0; i < trainingBatchSize; i++) {
				batch.sents[i] = projectiveSentences[indices[i]];
			}

			mlp.training(); 
			sampler.sample(batch);
			System.out.format("Sampled %d actions of %d sentences.\n",
					sampler.getActionCount(), batch.sents.length);
			mlp.trainBatchRL(sampler.getDataset(), sampler.getOutputParses(),
					sampler.getBaselines(), trainingBatchSize, !reinforce);
			mlp.updatePrecomputedVectors();

			sentCount = sentCount + trainingBatchSize;
			double elapsedTimeSec = (System.currentTimeMillis() - start) / 1000.0;
			double speed = sentCount / elapsedTimeSec;

			if (iterCount % evalPerIter == 0 || iterCount >= maxIters-1) {
				mlp.evaluate(); // affects dropout layer, if present;
				double score = evaluator.parse_and_measure(valid_ds, parser);
				System.out.format("%s on development set: %f%%\n",
						evaluator.getName(), score * 100);
				if (score > bestScore) {
					System.out.format("Writing best model to %s... ", bestPath);
					try (FileOutputStream fos = new FileOutputStream(bestPath);
							ObjectOutputStream oos = new ObjectOutputStream(fos);) {
						oos.writeObject(mlp);
					}
					bestScore = score;
					System.out.print("Done.\n");
				}
			}
			
			System.out.format("Finished iter %d (%.2f min, %.2f sentences/s)\n", 
					iterCount, elapsedTimeSec/60.0, speed);
		}

		if (bestPath.exists()) {
			System.out.format("Loading from %s... ", bestPath);
			try (FileInputStream fis = new FileInputStream(bestPath);
					ObjectInputStream ois = new ObjectInputStream(fis);) {
				try {
					parser.setNeuralNetwork((ChenManningNeuralNetwork) ois.readObject());
				} catch (ClassNotFoundException e) {
					throw new RuntimeException("Unlikely exception", e);
				}
			}
			System.out.print("Done.\n");
		}

		double elapsedTimeMin = (System.currentTimeMillis() - start) / 1000.0 / 60.0;
		System.out.format(
				"Training %s by reinforcement learning... Done (%.2f min).\n",
				parser.getName(), elapsedTimeMin);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void run() throws IOException, ClassNotFoundException {
		if (reinforce && (sampleSize != 1 || !"random".equals(samplerName))) {
			System.out.println("Notice! REINFORCE mode forces sampleSize to be "
					+ "1 and sampler to be random");
			sampleSize = 1;
			samplerName = "random";
		}
		assert_(samplerName != null, "command line parameter -sampler is required");
		
		Parser parser;
		if (pretrainedPath.endsWith(".txt") || pretrainedPath.endsWith(".txt.gz")) {
			parser = Parser.load_stanford_model(pretrainedPath, rootLabel);
		} else {
			try (FileInputStream fis = new FileInputStream(pretrainedPath);
					ObjectInputStream ois = new ObjectInputStream(fis);) {
				parser = (Parser) ois.readObject();
			}
		}

		TransitionSystem sys = parser.newTransitionSystem();
		Supplier<StaticOracle> oracleSupplier = null;
		if (sys instanceof ArcStandard) {
			oracleSupplier = () -> new DeterministicArcStandardShortestStackOracle();
		} else if (sys instanceof ArcEager) {
			oracleSupplier = () -> new DeterministicArcEagerOracle();
		} else if (sys instanceof SwapStandard) {
			oracleSupplier = () -> new DeterministicSwapStandardEagerOracle();
		} 
		if (oracleSupplier == null) {
			throw new IllegalArgumentException(
					"Don't have a static oracle " + "for transition type: " + sys.getClass());
		}

		if ("las".equals(measure)) {
			evaluator = new Evaluator.LAS(includePunc);
		} else if ("uas".equals(measure)) {
			evaluator = new Evaluator.UAS(includePunc);
		} else {
			throw new IllegalArgumentException("Unknown measure: " + measure);
		}
		
		CoNLL conll = new CoNLL(parser.getVocabs());
		int max_rows = (int) (Files.lines(Paths.get(trainPath)).count() + 1);
		conll.prepare(trainPath, 0, "train", max_rows);
		Dataset train_ds = conll.build_dataset(trainPath, "train", max_rows);
		// for debugging
		// ds.sents = Tensors.narrow(ds.sents, 0, 1);
		max_rows = (int) (Files.lines(Paths.get(validPath)).count() + 1);
		Dataset valid_ds = conll.build_dataset(validPath, "valid", max_rows);

		Sampler sampler;
		if ("oracle".equals(samplerName)) {
			sampler = new OracleOnlySampler(oracleSupplier, parser,
					trainingBatchSize);
		} else if ("random".equals(samplerName)) {
			sampler = new RandomSampler(parser, evaluator,
					sampleSize, trainingBatchSize);
		} else if ("memory".equals(samplerName)) {
			sampler = new MemorySampler(parser, evaluator, 
					sampleSize, trainingBatchSize, 0.01);
		} else {
			throw new IllegalArgumentException(
					"Unsupported sampling method: " + samplerName);
		}

		reinforce_parser(parser, sampler, train_ds, valid_ds);

		try (FileOutputStream fos = new FileOutputStream(parserPath);
				ObjectOutputStream oos = new ObjectOutputStream(fos);) {
			oos.writeObject(parser);
		}
		System.out.println("Parser written to " + parserPath);
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		System.out.format("Command line parameters: %s\n", Arrays.toString(args));
		ReinforcementLearning rein = new ReinforcementLearning();
		new JCommander(rein, args);
		rein.run();
	}
}
