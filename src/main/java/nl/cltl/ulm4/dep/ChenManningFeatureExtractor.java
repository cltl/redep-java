package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_POS;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_WORD;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import nl.cltl.ulm4.nlp.FeatureExtractor;
import nl.cltl.ulm4.nlp.Vocabulary;

public class ChenManningFeatureExtractor implements
		FeatureExtractor<TransitionSystem>, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final int POS_OFFSET = 18;
	private static final int DEP_OFFSET = 36;

	public void extract(int[] x, TransitionSystem sys, Vocabulary[] vocabs) {
		// this method must match that of Stanford nndep parser so that
		// we can load Stanford's model into our parser
		// see
		// https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/parser/nndep/DependencyParser.java#L206
		int[][] tokens = sys.getTokens();
		int[][] links = sys.getLinks();
		List<Integer> stack = sys.getStack();
		List<Integer> buffer = sys.getBuffer();
		Arrays.fill(x, 0, POS_OFFSET, vocabs[VOCAB_WORD].getIndex("__NONE__"));
		Arrays.fill(x, POS_OFFSET, DEP_OFFSET, vocabs[VOCAB_POS].getIndex("__NONE__"));
		Arrays.fill(x, DEP_OFFSET, 48, vocabs[VOCAB_LABEL].getIndex("__NONE__"));

		if (stack.size() >= 1) {
			Integer s1 = stack.get(0);
			x[2] = tokens[s1][0]; // word(s1);
			x[20] = tokens[s1][1]; // pos(s1);

			int left11 = DependencyUtils.getLeftDependent(links, s1, 1, 0);
			if (left11 >= 0) {
				x[6] = tokens[left11][0]; // word(lc1(s1));
				x[24] = tokens[left11][1]; // pos(lc1(s1));
				x[36] = links[left11][1];
				int left_left = DependencyUtils.getLeftDependent(links, left11, 1, 0);
				if (left_left >= 0) {
					// System.out.println(left_left)
					x[10] = tokens[left_left][0]; // word(lc1(lc1(s1)));
					x[28] = tokens[left_left][1]; // pos(lc1(lc1(s1)));
					x[40] = links[left_left][1];
				}
				int left12 = DependencyUtils.getLeftDependent(links,
						s1, 2, 0);
				if (left12 >= 0) {
					x[8] = tokens[left12][0]; // word(lc2(s1));
					x[26] = tokens[left12][1]; // pos(lc2(s1));
					x[38] = links[left12][1];
				}
			}

			int right11 = DependencyUtils.getRightDependent(links, s1, 1, 0);
			if (right11 >= 0) {
				x[7] = tokens[right11][0]; // word(rc1(s1));
				x[25] = tokens[right11][1]; // pos(rc1(s1));
				x[37] = links[right11][1];
				int right_right = DependencyUtils.getRightDependent(links, right11,
						1, 0);
				if (right_right >= 0) {
					x[11] = tokens[right_right][0]; // word(rc1(rc1(s1)));
					x[29] = tokens[right_right][1]; // pos(rc1(rc1(s1)));
					x[41] = links[right_right][1];
				}
				int right12 = DependencyUtils.getRightDependent(links, s1, 2, 0);
				if (right12 >= 0) {
					x[9] = tokens[right12][0]; // word(rc2(s1));
					x[27] = tokens[right12][1]; // pos(rc2(s1));
					x[39] = links[right12][1];
				}
			}

			if (stack.size() >= 2) {
				Integer s2 = stack.get(1);
				x[1] = tokens[s2][0]; // word(s2);
				x[19] = tokens[s2][1]; // pos(s2);

				int left21 = DependencyUtils.getLeftDependent(links, s2, 1, 0);
				if (left21 >= 0) {
					x[12] = tokens[left21][0]; // word(lc1(s2));
					x[30] = tokens[left21][1]; // pos(lc1(s2));
					x[42] = links[left21][1];
					int left_left = DependencyUtils.getLeftDependent(links, left21, 1, 0);
					if (left_left >= 0) {
						x[16] = tokens[left_left][0]; // word(lc1(lc1(s2)));
						x[34] = tokens[left_left][1]; // pos(lc1(lc1(s2)));
						x[46] = links[left_left][1];
					}
					int left22 = DependencyUtils.getLeftDependent(links,
							s2, 1, 0);
					if (left22 >= 0) {
						x[14] = tokens[left22][0]; // word(lc2(s2));
						x[32] = tokens[left22][1]; // pos(lc2(s2));
						x[44] = links[left22][1];
					}
				}

				int right21 = DependencyUtils.getRightDependent(links, s2, 1, 0);
				if (right21 >= 0) {
					x[13] = tokens[right21][0]; // word(rc1(s2));
					x[31] = tokens[right21][1]; // pos(rc1(s2));
					x[43] = links[right21][1];

					int right_right = DependencyUtils.getRightDependent(links,
							right21, 1, 0);
					if (right_right >= 0) {
						x[17] = tokens[right_right][0]; // word(rc1(rc1(s2)));
						x[35] = tokens[right_right][1]; // pos(rc1(rc1(s2)));
						x[47] = links[right_right][1];
					}
					int right22 = DependencyUtils.getRightDependent(links, s2, 2, 0);
					if (right22 >= 0) {
						x[15] = tokens[right22][0]; // word(rc2(s2));
						x[33] = tokens[right22][1]; // pos(rc2(s2));
						x[45] = links[right22][1];
					}
				}

				if (stack.size() >= 3) {
					int s3 = stack.get(2);
					x[0] = tokens[s3][0]; // word(s3);
					x[18] = tokens[s3][1]; // pos(s3);
				}
			}
		}
		for (int j = 0; j < Math.min(3, buffer.size()); j++) {
			x[3 + j] = tokens[buffer.get(j)][0]; // word(b_j);
			x[POS_OFFSET + 3 + j] = tokens[buffer.get(j)][1]; // pos(b_j);
		}
	}

	public int num() {
		return 48;
	}

}
