package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.util.Pythonic.assert_;

import java.util.ArrayList;
import java.util.List;

import nl.cltl.ulm4.nlp.Indexer;
import nl.cltl.ulm4.nlp.Vocabulary;

public final class DependencyUtils {

	// check if the tree is legal, O(n)
	public static boolean isTree(int[][] heads, int col) {
		List<Integer> h = new ArrayList<>();
		h.add(-1);
		for (int i = 1; i < heads.length; i++) {
			if (heads[i][col] < 0 || heads[i][col] >= heads.length) {
				return false;
			}
			h.add(-1);
		}
		for (int i = 1; i < heads.length; i++) {
			int k = i;
			while (k > 0) {
				if (h.get(k) >= 0 && h.get(k) < i) {
					break;
				}
				if (h.get(k) == i) {
					return false;
				}
				h.set(k, i);
				k = heads[k][col];
			}
		}
		return true;
	}

	private static class ProjectivityVisitor {
		private int counter = -1;
		private int[][] heads;
		private int col;
		
		ProjectivityVisitor(int[][] heads, int col) {
			this.heads = heads;
			this.col = col;
		}

		boolean visit_tree(int w) {
			for (int i = 1; i < w; i++) {
				if (heads[i][col] == w && !visit_tree(i)) {
					return false;
				}
			}
			counter = counter + 1;
			if (w != counter) {
				return false;
			}
			for (int i = w + 1; i < heads.length; i++) {
				if (heads[i][col] == w && !visit_tree(i)) {
					return false;
				}
			}
			return true;
		}
	}

	public static boolean is_projective(int[][] heads, int col) {
		if (!isTree(heads, col)) {
			return false;
		}
		return new ProjectivityVisitor(heads, col).visit_tree(0);
	}

	/**
	 * 
	 * @param links
	 * @param wid
	 * @param index starts from 1
	 * @param col
	 * @return
	 */
	public static int getLeftDependent(int[][] links, int wid, int index, int col) {
		assert_(index > 0);
		for (int i = 0; i < wid; i++) {
			if (links[i][col] == wid) {
				index = index - 1;
			}
			if (index <= 0) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * 
	 * @param links
	 * @param wid
	 * @param index starts from 1
	 * @param col
	 * @return
	 */
	public static int getRightDependent(int[][] links, int wid, int index, int col) {
		assert_(index >= 0);
	    for (int i = links.length-1; i > wid; i--) {
	        if (links[i][col] == wid) {
	            index = index - 1;
	        }
	        if (index <= 0) {
	            return i;
	        }
	    }
	    return -1;
	}
	
	private static class InOrderVisitor {

		private int counter = -1;
		private int[][] links;
		private int col;
		private int[] order;

		public InOrderVisitor(int[][] links, int col) {
			this.links = links;
			this.col = col;
			order = new int[links.length];;
		}

		public int[] visit(int w) {
			// left
			for (int i = 0; i < w; i++) {
				if (links[i][col] == w) {
					visit(i);
				}
			}
			order[w] = ++counter;
			// right
			for (int i = w+1; i < links.length; i++) {
				if (links[i][col] == w) {
					visit(i);
				}
			}
			return order;
		}
		
	}
	
	public static int[] getTreeInOrder(int[][] links, int col) {
		return new InOrderVisitor(links, col).visit(0);
	}
	
	public static int[] countChildren(int[][] links, int col) {
		int[] count = new int[links.length];
		for (int i = 1; i < count.length; i++) {
			count[links[i][col]]++;
		}
		return count;
	}

	public static final int VOCAB_WORD = 0;
	public static final int VOCAB_POS = 1;
	public static final int VOCAB_LABEL = 2;
	
	public static Vocabulary[] newVocabularies() {
		return newVocabularies(new Indexer());
	}

	public static Vocabulary[] newVocabularies(Indexer indexer) {
		Vocabulary[] vocabs = new Vocabulary[3];
		for (int i = 0; i < vocabs.length; i++) {
			vocabs[i] = new Vocabulary(indexer);
		}
		return vocabs;
	}

}
