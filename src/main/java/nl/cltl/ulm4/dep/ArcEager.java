package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.error;
import nl.cltl.ulm4.nlp.Vocabulary;

public class ArcEager extends TransitionSystem {

	public static final int ATYPE_LEFT_ARC = 0;
	public static final int ATYPE_RIGHT_ARC = 1;
	public static final int ATYPE_RIGHT_ARC_ROOT = 2;
	public static final int ATYPE_REDUCE = 3;
	public static final int ATYPE_SHIFT = 4;
	
	public static final int STATE_TERMINAL = -1;
	public static final int STATE_RIGHT_REDUCE_SHIFT = 0;
	public static final int STATE_LEFT_RIGHT_SHIFT = 1;
	public static final int STATE_RIGHT_ROOT_SHIFT = 2;
	public static final int STATE_REDUCE = 3;

	ArcEager() { 
		// for cloning
	}
	
	public ArcEager(ActionsInfo actionsInfo) {
		this.info = actionsInfo;
	}

	public ArcEager(ActionsInfo actionsInfo, int[][] tokens) {
		this(actionsInfo);
		reset(tokens);
	}

	@Override
	public boolean isTerminated() {
		return state() == STATE_TERMINAL;
	}

	public int state() {
		if (buffer.size() >= 1) {
			if (stack.size() >= 2) {
				if (links[stack.get(0)][0] >= 0) {
					return STATE_RIGHT_REDUCE_SHIFT;
				} else {
					return STATE_LEFT_RIGHT_SHIFT;
				}
			} else {
				assert_(stack.size() == 1 && stack.get(0) == 0,
						"Root disappear! Where's my root???");
				return STATE_RIGHT_ROOT_SHIFT;
			}
		} else {
			if (stack.size() >= 2) {
				if (links[stack.get(0)][0] >= 0) {
					return STATE_REDUCE;
				} else {
					return STATE_TERMINAL;
				}
			} else {
				assert_(stack.size() == 1 && stack.get(0) == 0,
						"Root disappear! Where's my root???");
				return STATE_TERMINAL;
			}
		}
	}

	public TransitionSystem execute(int a) {
		if (info.isType(a, ATYPE_LEFT_ARC)) {
			assert_(stack.size() >= 1);
			int s0 = stack.get(0);
			int b0 = buffer.get(0);
			links[s0][0] = b0;
			links[s0][1] = info.action2label(a);
			stack.remove(0);
		} else if (info.isType(a, ATYPE_RIGHT_ARC) || 
				info.isType(a, ATYPE_RIGHT_ARC_ROOT)) {
			assert_(stack.size() >= 1);
			int s0 = stack.get(0);
			int b0 = buffer.get(0);
			links[b0][0] = s0;
			links[b0][1] = info.action2label(a);
			stack.add(0, buffer.remove(0));
		} else if (info.isType(a, ATYPE_REDUCE)) {
			assert_(stack.size() >= 2, "Can't reduce root.");
			stack.remove(0);
		} else if (info.isType(a, ATYPE_SHIFT)) {
			assert_(buffer.size() >= 1, "The last word is already consumed.");
			stack.add(0, buffer.remove(0));
		} else {
			error("Unsupported action: " + a);
		}
		actions.add(a);
		return this;
	}
	
	public static ActionsInfo buildActionsInfo(Vocabulary labelVocab, String root_dep_label) {
		return buildActionsInfo(labelVocab.word2index.keySet(), root_dep_label, labelVocab);
	}

	public static ActionsInfo buildActionsInfo(Iterable<String> labels, String rootLabel, 
			Vocabulary labelVocab) {
		ActionsInfo.Builder builder = new ActionsInfo.Builder(labelVocab);
	    registerActionsInfo(builder, labels, rootLabel);
	    return builder.build();
	}

	protected static void registerActionsInfo(ActionsInfo.Builder builder,
			Iterable<String> labels, String rootLabel) {
		for (String label : labels) {
	        if (!label.equals("__NONE__")) {
	            builder.register(label, "L(" + label + ")", ATYPE_LEFT_ARC);
	        }
	    }
	    boolean rootFound = false;
	    for (String label : labels) {
	        if (!label.equals("__NONE__")) {
	        	if (label.equals(rootLabel)) {
	        		builder.register(label, "R(" + label + ")", ATYPE_RIGHT_ARC_ROOT);
	        		rootFound = true;
	        	} else {
	        		builder.register(label, "R(" + label + ")", ATYPE_RIGHT_ARC);
	        	}
	        }
	    }
	    assert_(rootFound, "Can't find " + rootLabel + ". Wrong root label, maybe?");
	    builder.register(null, "SH", ATYPE_SHIFT);
	    builder.register(null, "RE", ATYPE_REDUCE);
	}

	public static int[][] eligibleMap(ActionsInfo info) {
		int[][] map = new int[4][];
		map[STATE_RIGHT_REDUCE_SHIFT] = info.types2actionsArr(
				ATYPE_RIGHT_ARC, ATYPE_REDUCE, ATYPE_SHIFT);
		map[STATE_LEFT_RIGHT_SHIFT] = info.types2actionsArr(
				ATYPE_LEFT_ARC, ATYPE_RIGHT_ARC, ATYPE_SHIFT);
		map[STATE_RIGHT_ROOT_SHIFT] = info.types2actionsArr(
				ATYPE_RIGHT_ARC_ROOT, ATYPE_SHIFT);
		map[STATE_REDUCE] = info.types2actionsArr(ATYPE_REDUCE);
		return map;
	}

	@Override
	public TransitionSystem execute_alternative(int action, boolean first) {
		throw new UnsupportedOperationException();
	}
	
	public TransitionSystem execute_identified(int action, int first) {
		throw new UnsupportedOperationException();
	}
}