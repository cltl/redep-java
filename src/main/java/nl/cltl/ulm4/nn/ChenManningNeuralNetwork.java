package nl.cltl.ulm4.nn;

import static nl.cltl.ulm4.util.Pythonic.assert_;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import nl.cltl.ulm4.dep.SupervisedDataset;
import nl.cltl.ulm4.dep.re.Sampler;
import nl.cltl.ulm4.util.IntPair;
import nl.cltl.ulm4.util.Tensors;

public class ChenManningNeuralNetwork implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	double[][] E;
	double[][] W1;
	double[] b1;
	double[][] W2;
	private int[][] masks;

	private int nOutput;
	private int hSize;
	private int vSize;
	private int eSize;
	private int nTokens;
	
	private int capability;
	
	// precomputed activations
	public List<IntPair> toPreCompute;
	private transient double[][][] saved;

	private transient boolean isTraining;
	private transient double dropProb;
	private transient double l2;
	private transient Random random;

	transient Optimization optim;
	private transient List<Worker> tWorkers;
	private transient ThreadLocal<Worker> tWorkerLocal;

	public boolean lessOutput;

	// for deserialization
	public ChenManningNeuralNetwork() {
	}

	public ChenManningNeuralNetwork(int vSize, int eSize, int nTokens,
			int hSize, int nOutput) {
		this.vSize = vSize;
		this.eSize = eSize;
		this.nTokens = nTokens;
		this.hSize = hSize;
		this.nOutput = nOutput;

		E = new double[vSize][eSize];
		W1 = new double[hSize][nTokens * eSize];
		b1 = new double[hSize];
		W2 = new double[nOutput][hSize];
	}

	private void validateTraining() {
		if (!isTraining)
			throw new IllegalStateException(
					"Not training, or training was already finalized");
		if (tWorkerLocal == null) {
			tWorkers = Collections.synchronizedList(new ArrayList<>());
			tWorkerLocal = new ThreadLocal<Worker>() {
				@Override
				protected Worker initialValue() {
					Worker w = new Worker(capability);
					w.initTraining(new Random());
					tWorkers.add(w);
					return w;
				}
			};
		}
	}

	/* (non-Javadoc)
	 * @see nl.cltl.ulm4.nn.NeuralNetwork#precompute(java.util.List)
	 */
	
	public void precompute(List<IntPair> preComputed) {
		this.toPreCompute = preComputed;
		updatePrecomputedVectors();
	}

	public void setPrecomputedFeatures(List<IntPair> preComputed) {
		this.toPreCompute = preComputed;
	}
	
	public void updatePrecomputedVectors() {
		long startTime = System.currentTimeMillis();
		// when deserialized, saved will be null so it's important to 
		// always check before we update vectors
		if (saved == null) { 
			saved = new double[vSize][nTokens][];
			for (IntPair p : toPreCompute) {
				int tok = p.a;
				int pos = p.b;
				saved[tok][pos] = new double[hSize];
			}
		}
		Consumer<IntPair> action = new Consumer<IntPair>() {
			public void accept(IntPair p) {
				int tok = p.a;
				int pos = p.b;
				double[] arr = saved[tok][pos];
				Arrays.fill(arr, 0);
				for (int j = 0; j < hSize; ++j)
					for (int k = 0; k < eSize; ++k)
						arr[j] += W1[j][pos * eSize + k] * E[tok][k];
			}
		};
		toPreCompute.stream().parallel().forEach(action);
		double elapsedTime = (System.currentTimeMillis() - startTime) / 1000.0;
		if (!lessOutput) {
			System.out.println("PreComputed " + toPreCompute.size()
					+ ", Elapsed Time: " + elapsedTime + " (s)");
		}
	}
	
	
	public Worker newWorker() {
		return this.new Worker();
	}

	public void initTraining(Random random, double dropProb,
			double l2, Optimization optim,
			int capability) {
		this.dropProb = dropProb;
		this.l2 = l2;
		this.optim = optim;
		this.capability = capability;
		optim.init();
		isTraining = true;
	}

	/* (non-Javadoc)
	 * @see nl.cltl.ulm4.nn.NeuralNetwork#initParameters(java.util.Random, double)
	 */
	
	public void initParameters(Random random, double initRange) {
		// Randomly initialize weight matrices / vectors
		for (int i = 0; i < E.length; ++i) {
			for (int j = 0; j < E[i].length; ++j) {
				E[i][j] = random.nextDouble() * 2 * initRange - initRange;
			}
		}
		for (int i = 0; i < W1.length; ++i)
			for (int j = 0; j < W1[i].length; ++j)
				W1[i][j] = random.nextDouble() * 2 * initRange - initRange;

		// according to this, biases can and often be initialized to zero
		// http://cs231n.github.io/neural-networks-2/
		Arrays.fill(b1, 0);

		for (int i = 0; i < W2.length; ++i)
			for (int j = 0; j < W2[i].length; ++j)
				W2[i][j] = random.nextDouble() * 2 * initRange - initRange;
	}

	/* (non-Javadoc)
	 * @see nl.cltl.ulm4.nn.NeuralNetwork#trainBatch(nl.cltl.ulm4.dep.SupervisedDataset)
	 */
	
	public double trainBatch(SupervisedDataset ds) {
		validateTraining();
		tWorkers.stream().parallel().forEach(w -> w.resetGradParams());
		IntStream.range(0, ds.y.length).parallel()
				.forEach(s -> tWorkerLocal.get().forwardBackward(ds, s));
		Worker lastWorker = tWorkers.stream().parallel()
				.peek(w -> w.backpropSaved()) 
				.reduce((a, b) -> a.merge(b)).get();
		lastWorker.addL2Regularization();
		optim.step(lastWorker);
		if (!lessOutput) {
			System.out.format("Cost: %f, correct: %.2f%%\n", lastWorker.cost,
					100.0 * lastWorker.correct);
		}
		return lastWorker.cost;
	}

	/* (non-Javadoc)
	 * @see nl.cltl.ulm4.nn.NeuralNetwork#trainBatch(nl.cltl.ulm4.dep.SupervisedDataset)
	 */
	
	public double trainBatchRL(SupervisedDataset ds, List<int[]> episodes,
			Map<Integer, Double> baselines, int batchSize, boolean scaledByProb) {
		validateTraining();
		tWorkers.stream().parallel().forEach(w -> w.resetGradParams());
		double[] scales = new double[ds.y.length];
		IntStream.range(0, episodes.size()).parallel()
				.forEach(e -> tWorkerLocal.get().forwardBackwardRL(
						ds, baselines, scales, batchSize, 
						episodes.get(e), scaledByProb));
		Worker lastWorker = tWorkers.stream().parallel()
				.peek(w -> w.backpropSaved())
				.reduce((a, b) -> a.merge(b)).get();
		lastWorker.addL2Regularization();

		optim.step(lastWorker);
		return lastWorker.cost;
	}

	
	public double[][] getEmbeddingWeights() {
		return E;
	}

	
	public double[][] getHiddenLayerWeight() {
		return W1;
	}

	
	public double[] getHiddenyLayerBias() {
		return b1;
	}

	
	public double[][] getOutputWeight() {
		return W2;
	}

	/* (non-Javadoc)
	 * @see nl.cltl.ulm4.nn.NeuralNetwork#getMasks()
	 */
	
	public int[][] getMasks() {
		return masks;
	}

	/* (non-Javadoc)
	 * @see nl.cltl.ulm4.nn.NeuralNetwork#setMasks(int[][])
	 */
	
	public void setEligibleActionsMap(int[][] masks) {
		this.masks = masks;
	}

	/* (non-Javadoc)
	 * @see nl.cltl.ulm4.nn.NeuralNetwork#training()
	 */
	
	public void training() {
		isTraining = true;
	}

	/* (non-Javadoc)
	 * @see nl.cltl.ulm4.nn.NeuralNetwork#evaluate()
	 */
	
	public void evaluate() {
		isTraining = false;
	}

	
	public String toString() {
		return String.format("%dx%d -> %d -> %d", nTokens, eSize, hSize,
				nOutput);
	}

	public class Worker {

		private final double[][] scores;
		public final double[][] probs;
		private final double[][] hidden;
		private final double[][] hidden3;
		private final int[][] activeHiddenUnits;

		private double[] gradHidden3;
		double[] gradHidden;
		double[][] gradW1;
		double[] gradb1;
		double[][] gradW2;
		double[][] gradE;
		double[][][] gradSaved;
		Random random;
		double cost;
		double correct;

		public Worker() {
			this(1);
		}

		public Worker(int capability) {
			scores = new double[capability][nOutput];
			probs = new double[capability][nOutput];
			hidden = new double[capability][hSize];
			hidden3 = new double[capability][hSize];
			activeHiddenUnits = new int[capability][];
		}

		
		public void initTraining(Random random) {
			this.random = random;
			gradW1 = new double[W1.length][W1[0].length];
			gradb1 = new double[b1.length];
			gradW2 = new double[W2.length][W2[0].length];
			gradE = new double[E.length][E[0].length];
			if (saved != null) gradSaved = new double[saved.length][nTokens][];
			gradHidden3 = new double[hSize];
			gradHidden = new double[hSize];
			cost = 0;
			correct = 0;
		}

		public void resetGradParams() {
			Tensors.fill(gradW1, 0);
			Arrays.fill(gradb1, 0);
			Tensors.fill(gradW2, 0);
			Tensors.fill(gradE, 0);
			if (gradSaved != null) Tensors.fill(gradSaved, null);
			cost = 0;
			correct = 0;
		}
		
		public int forward(int[] x, int s) {
			Tensors.fill(hidden, 0);
			int offset = 0;
			for (int j = 0; j < nTokens; ++j) {
				int tok = x[j];

				if (saved != null && saved[tok][j] != null) {
					for (int nodeIndex = 0; nodeIndex < hSize; nodeIndex++)
						hidden[0][nodeIndex] += saved[tok][j][nodeIndex];
				} else {
					for (int nodeIndex = 0; nodeIndex < hSize; nodeIndex++) {
						for (int k = 0; k < eSize; ++k)
							hidden[0][nodeIndex] += W1[nodeIndex][offset + k]
									* E[tok][k];
					}
				}
				offset += eSize;
			}
			// System.out.println(Arrays.toString(hidden));

			// Add bias term and apply activation function
			for (int nodeIndex = 0; nodeIndex < hSize; nodeIndex++) {
				hidden[0][nodeIndex] += b1[nodeIndex];
				double h = hidden[0][nodeIndex];
				hidden3[0][nodeIndex] = h * h * h;
			}
			// System.out.println(Arrays.toString(hidden3));

			// Feed forward to softmax layer (no activation yet)
			Tensors.fill(scores, 0);
			int optLabel = -1;
			for (int i : masks[s]) {
				for (int nodeIndex = 0; nodeIndex < hSize; nodeIndex++)
					scores[0][i] += W2[i][nodeIndex] * hidden3[0][nodeIndex];

				if (optLabel < 0 || scores[0][i] > scores[0][optLabel])
					optLabel = i;
			}

			// sum1 is removed
			double sum2 = 0.0;
			double maxScore = scores[0][optLabel];
			for (int i : masks[s]) {
				probs[0][i] = Tensors.exp(scores[0][i] - maxScore);
				sum2 += probs[0][i];
			}
			for (int i : masks[s]) {
				probs[0][i] /= sum2;
			}
			return optLabel;
		}
		
		/**
		 * Forward-propagate and return the probability for the target output.
		 * Designed for reinforcement learning, to be used in conjunction with
		 * #backwardRL().
		 * 
		 * @param x
		 * @param s
		 * @param y
		 * @return
		 */
		public void forwardRL(SupervisedDataset ds, int start, int count) {
			Tensors.fill(hidden, 0);
			Tensors.fill(scores, 0);
			for (int exampleIndex = 0; exampleIndex < count; exampleIndex++) {
				int[] x = ds.x[start+exampleIndex];
				int s = ds.states[start+exampleIndex];
				int[] eligibleOutputs = masks[s];
				int[] ls = IntStream.range(0, hSize)
						.filter(n -> (random.nextDouble() > dropProb))
						.toArray();
				activeHiddenUnits[exampleIndex] = ls;

				int offset = 0;
				for (int j = 0; j < nTokens; ++j) {
					int tok = x[j];
					if (saved != null && saved[tok][j] != null) {
						for (int nodeIndex : ls)
							hidden[exampleIndex][nodeIndex] += saved[tok][j][nodeIndex];
					} else {
						for (int nodeIndex : ls)
							for (int k = 0; k < eSize; ++k)
								hidden[exampleIndex][nodeIndex] += W1[nodeIndex][offset + k]
										* E[tok][k];
					}
					offset += eSize;
				}

				// Add bias term and apply activation function
				for (int nodeIndex : ls) {
					hidden[exampleIndex][nodeIndex] += b1[nodeIndex];
					double h = hidden[exampleIndex][nodeIndex];
					hidden3[exampleIndex][nodeIndex] = h * h * h;
				}

				// Feed forward to softmax layer (no activation yet)
				int optLabel = -1;
				for (int i : eligibleOutputs) {
					for (int nodeIndex : ls)
						scores[exampleIndex][i] += W2[i][nodeIndex] * 
								hidden3[exampleIndex][nodeIndex];

					if (optLabel < 0 || scores[exampleIndex][i] > 
							scores[exampleIndex][optLabel])
						optLabel = i;
				}

				// sum1 is removed
				double sum2 = 0.0;
				double maxScore = scores[exampleIndex][optLabel];
				for (int i : eligibleOutputs) {
					scores[exampleIndex][i] = Tensors.exp(scores[exampleIndex][i] - maxScore);
					sum2 += scores[exampleIndex][i];
				}

				for (int i : eligibleOutputs) {
					probs[exampleIndex][i] = scores[exampleIndex][i] / sum2;
				}
			}
		}

		/**
		 * Backpropagate to compute gradients. Everything is scaled, e.g.
		 * proportionally to the reward and probability of the parse. Designed
		 * for reinforcement learning, assume that one calls forwardRL right
		 * before this method (the state of hidden units are carried along).
		 * #backwardRL().
		 * 
		 * @param x
		 * @param s
		 * @param y
		 * @return
		 */
		public void backwardRL(SupervisedDataset ds, double[] scales,
				int start, int count) {
			for (int exampleIndex = 0; exampleIndex < count; exampleIndex++) {
				int[] x = ds.x[start+exampleIndex];
				int s = ds.states[start+exampleIndex];
				int y = ds.y[start+exampleIndex];
				double scale = scales[start+exampleIndex];
				int[] eligibleOutputs = masks[s];
				int[] ls = activeHiddenUnits[exampleIndex];

				Arrays.fill(gradHidden3, 0);
				for (int i : eligibleOutputs) {
					int label = (i == y ? 1 : 0);
					double delta = -scale * (label - probs[exampleIndex][i]);
					for (int nodeIndex : ls) {
						gradW2[i][nodeIndex] += delta * hidden3[exampleIndex][nodeIndex];
						gradHidden3[nodeIndex] += delta * W2[i][nodeIndex];
					}
				}

				for (int nodeIndex : ls) {
					double h = hidden[exampleIndex][nodeIndex];
					gradHidden[nodeIndex] = gradHidden3[nodeIndex] * 3 * h * h;
					gradb1[nodeIndex] += gradHidden[nodeIndex];
				}

				int offset = 0;
				for (int j = 0; j < nTokens; ++j) {
					int tok = x[j];
					if (saved != null && saved[tok][j] != null) {
						for (int nodeIndex : ls) {
							if (gradSaved[tok][j] == null)
								gradSaved[tok][j] = new double[hSize];
							gradSaved[tok][j][nodeIndex] += gradHidden[nodeIndex];
						}
					} else {
						for (int nodeIndex : ls) {
							for (int k = 0; k < eSize; ++k) {
								gradW1[nodeIndex][offset + k] += 
										gradHidden[nodeIndex] * E[tok][k];
								gradE[tok][k] += gradHidden[nodeIndex]
										* W1[nodeIndex][offset + k];
							}
						}
					}
					offset += eSize;
				}
			}
		}

		void forwardBackward(SupervisedDataset ds, int exampleIndex) {
			int[] x = ds.x[exampleIndex];
			int s = ds.states[exampleIndex];
			int y = ds.y[exampleIndex];
			int[] eligibleOutputs = masks[s];

			int[] ls = IntStream.range(0, hSize)
					.filter(n -> (random.nextDouble() > dropProb))
					.toArray();

			Tensors.fill(hidden, 0);
			int offset = 0;
			for (int j = 0; j < nTokens; ++j) {
				int tok = x[j];
				if (saved != null && saved[tok][j] != null) {
					for (int nodeIndex : ls)
						hidden[0][nodeIndex] += saved[tok][j][nodeIndex];
				} else {
					for (int nodeIndex : ls)
						for (int k = 0; k < eSize; ++k)
							hidden[0][nodeIndex] += W1[nodeIndex][offset + k]
									* E[tok][k];
				}
				offset += eSize;
			}

			// Add bias term and apply activation function
			for (int nodeIndex : ls) {
				hidden[0][nodeIndex] += b1[nodeIndex];
				double h = hidden[0][nodeIndex];
				hidden3[0][nodeIndex] = h * h * h;
			}

			// Feed forward to softmax layer (no activation yet)
			Tensors.fill(scores, 0);
			int optLabel = -1;
			for (int i : eligibleOutputs) {
				for (int nodeIndex : ls)
					scores[0][i] += W2[i][nodeIndex] * hidden3[0][nodeIndex];

				if (optLabel < 0 || scores[0][i] > scores[0][optLabel])
					optLabel = i;
			}

			double sum1 = 0.0;
			double sum2 = 0.0;
			double maxScore = scores[0][optLabel];
			boolean yAmongEligibleOutputs = false;
			for (int i : eligibleOutputs) {
				scores[0][i] = Tensors.exp(scores[0][i] - maxScore);
				if (i == y) {
					yAmongEligibleOutputs = true;
					sum1 += scores[0][i];
				}
				sum2 += scores[0][i];
			}
			assert_(yAmongEligibleOutputs);

			cost += (Math.log(sum2) - Math.log(sum1)) / ds.y.length;
			if (Double.isInfinite(cost)) {
				System.err.println("Infinite cost detected. Skipped.");
				return;
			}
			if (optLabel == y)
				correct += +1.0 / ds.y.length;

			Arrays.fill(gradHidden3, 0);
			for (int i : eligibleOutputs) {
				int label = (i == y ? 1 : 0);
				double delta = -(label - scores[0][i] / sum2) / ds.y.length;
				// System.out.format("%d:%f\t", i, delta);
				for (int nodeIndex : ls) {
					gradW2[i][nodeIndex] += delta * hidden3[0][nodeIndex];
					gradHidden3[nodeIndex] += delta * W2[i][nodeIndex];
					// System.out.format("%d:%s\t", nodeIndex,
					// gradW2[i][nodeIndex]);
				}
				// System.out.println();
			}
			// System.out.println();

			for (int nodeIndex : ls) {
				double h = hidden[0][nodeIndex];
				gradHidden[nodeIndex] = gradHidden3[nodeIndex] * 3 * h * h;
				gradb1[nodeIndex] += gradHidden[nodeIndex];
			}

			offset = 0;
			for (int j = 0; j < nTokens; ++j) {
				int tok = x[j];
				// System.out.format("%d\t", tok);
				if (saved != null && saved[tok][j] != null) {
					for (int nodeIndex : ls) {
						if (gradSaved[tok][j] == null)
							gradSaved[tok][j] = new double[hSize];
						gradSaved[tok][j][nodeIndex] += gradHidden[nodeIndex];
					}
				} else {
					for (int nodeIndex : ls) {
						// System.out.format("%d\t", nodeIndex);
						for (int k = 0; k < eSize; ++k) {
							gradW1[nodeIndex][offset + k] += gradHidden[nodeIndex]
									* E[tok][k];
							gradE[tok][k] += gradHidden[nodeIndex]
									* W1[nodeIndex][offset + k];
						}
					}
					// System.out.println();
				}
				// System.out.format("%d: %s\n", tok,
				// Arrays.toString(gradE[tok]));
				offset += eSize;
			}
			// System.out.println();
			// System.out.println("gradE[0]: " + Arrays.toString(gradE[0]));
		}

		public void forwardBackwardRL(SupervisedDataset ds,
				Map<Integer, Double> baselines, double[] scales, int batchSize,
				int[] episode, boolean scaledByProb) {
			assert_(ds.x.length == ds.states.length);
			assert_(ds.x.length == ds.y.length);
        	int reward = episode[Sampler.COL_REWARD];
        	int sentenceID = episode[Sampler.COL_SENTENCE_ID];
        	int actionStart = episode[Sampler.COL_ACTIONS_START];
        	int actionCount = episode[Sampler.COL_ACTIONS_LENGTH];
        	forwardRL(ds, actionStart, actionCount);
        	double parseProb = 1;
        	if (scaledByProb) {
        		double logParseProb = 0;
        		for (int j = 0; j < actionCount; j++) {
        			int action = ds.y[actionStart+j];
        			logParseProb += Math.log(probs[j][action]);
        		}
        		parseProb = Tensors.exp(logParseProb);
			}
            double scale = (reward - baselines.get(sentenceID)) * parseProb / batchSize;
        	for (int j = actionStart; j < actionStart+actionCount; j++) {
                scales[j] = scale;
        	}
        	backwardRL(ds, scales, actionStart, actionCount);
		}

		/**
		 * Backpropagate gradient values from gradSaved into the gradients for
		 * the E vectors that generated them.
		 *
		 * @param featuresSeen
		 *            Feature IDs observed during training for which gradSaved
		 *            values need to be backprop'd into gradE
		 */
		private void backpropSaved() {
			if (gradSaved == null) return;
			for (IntPair p : toPreCompute) {
				int tok = p.a;
				int pos = p.b;
				if (gradSaved[tok][pos] != null) {
					for (int j = 0; j < hSize; ++j) {
						double delta = gradSaved[tok][pos][j];
						for (int k = 0; k < eSize; ++k) {
							gradW1[j][pos * eSize + k] += delta * E[tok][k];
							gradE[tok][k] += delta * W1[j][pos * eSize + k];
						}
					}
				}
			}
		}

		/**
		 * Add L2 regularization cost to the gradients associated with this
		 * instance.
		 * 
		 * @return
		 */
		public double addL2Regularization() {
			for (int i = 0; i < W1.length; ++i) {
				for (int j = 0; j < W1[i].length; ++j) {
					cost += l2 * W1[i][j] * W1[i][j] / 2.0;
					gradW1[i][j] += l2 * W1[i][j];
				}
			}

			for (int i = 0; i < W2.length; ++i) {
				for (int j = 0; j < W2[i].length; ++j) {
					cost += l2 * W2[i][j] * W2[i][j] / 2.0;
					gradW2[i][j] += l2 * W2[i][j];
				}
			}

			for (int i = 0; i < E.length; ++i) {
				for (int j = 0; j < E[i].length; ++j) {
					cost += l2 * E[i][j] * E[i][j] / 2.0;
					gradE[i][j] += l2 * E[i][j];
				}
			}
			return cost;
		}

		/**
		 * Merge the given cost and gradient data with the data in this
		 * instance.
		 *
		 * @param other
		 */
		
		public Worker merge(Worker other) {
			this.cost += other.cost;
			this.correct += other.correct;

			Tensors.addInPlace(gradW1, other.gradW1);
			Tensors.addInPlace(gradb1, other.gradb1);
			Tensors.addInPlace(gradW2, other.gradW2);
			Tensors.addInPlace(gradE, other.gradE);
			return this;
		}

		
		public double[] getProbs() {
			assert_(probs.length == 1);
			return probs[0];
		}
	}

	public abstract class Optimization {
		
		public abstract void init();
	
		public abstract void step(Worker worker);
		
	}

	public class AdaGrad extends Optimization {
		
		private double adaAlpha;
		private double adaEps;

		// gradient history
		private double[][] eg2W1, eg2W2, eg2E;
		private double[] eg2b1;

		public AdaGrad(double adaAlpha, double adaEps) {
			super();
			this.adaAlpha = adaAlpha;
			this.adaEps = adaEps;
		}

		
		public void init() {
			eg2E = new double[E.length][E[0].length];
			eg2W1 = new double[W1.length][W1[0].length];
			eg2b1 = new double[b1.length];
			eg2W2 = new double[W2.length][W2[0].length];
		}

		/**
		 * Update classifier weights using the given training cost information.
		 *
		 * @param worker
		 *            Cost information as returned by
		 *            {@link #computeCostFunction(int, double, double)}.
		 * @param adaAlpha
		 *            Global AdaGrad learning rate
		 * @param adaEps
		 *            Epsilon value for numerical stability in AdaGrad's division
		 */
		public void step(Worker worker) {
			validateTraining();

			double[][] gradW1 = ((Worker)worker).gradW1;
			double[][] gradW2 = ((Worker)worker).gradW2;
			double[][] gradE = ((Worker)worker).gradE;
			double[] gradb1 = ((Worker)worker).gradb1;

			for (int i = 0; i < W1.length; ++i) {
				for (int j = 0; j < W1[i].length; ++j) {
					eg2W1[i][j] += gradW1[i][j] * gradW1[i][j];
					W1[i][j] -= adaAlpha * gradW1[i][j]
							/ Math.sqrt(eg2W1[i][j] + adaEps);
				}
			}

			for (int i = 0; i < b1.length; ++i) {
				eg2b1[i] += gradb1[i] * gradb1[i];
				b1[i] -= adaAlpha * gradb1[i] / Math.sqrt(eg2b1[i] + adaEps);
			}

			for (int i = 0; i < W2.length; ++i) {
				for (int j = 0; j < W2[i].length; ++j) {
					eg2W2[i][j] += gradW2[i][j] * gradW2[i][j];
					W2[i][j] -= adaAlpha * gradW2[i][j]
							/ Math.sqrt(eg2W2[i][j] + adaEps);
				}
			}

			for (int i = 0; i < E.length; ++i) {
				for (int j = 0; j < E[i].length; ++j) {
					eg2E[i][j] += gradE[i][j] * gradE[i][j];
					E[i][j] -= adaAlpha * gradE[i][j]
							/ Math.sqrt(eg2E[i][j] + adaEps);
				}
			}
		}
	}
	
	public class AdaDelta extends Optimization {

		private double adaRho;
		private double adaEps;

		// gradient history
		private double[][] eg2W1, eg2W2, eg2E;
		private double[] eg2b1;

		private double[][] ed2E;
		private double[][] ed2W1;
		private double[] ed2b1;
		private double[][] ed2W2;

		
		public AdaDelta(double adaRho, double adaEps) {
			this.adaRho = adaRho;
			this.adaEps = adaEps;
		}

		public void init() {
			// init gradient history
			eg2E = new double[E.length][E[0].length];
			eg2W1 = new double[W1.length][W1[0].length];
			eg2b1 = new double[b1.length];
			eg2W2 = new double[W2.length][W2[0].length];
			ed2E = new double[E.length][E[0].length];
			ed2W1 = new double[W1.length][W1[0].length];
			ed2b1 = new double[b1.length];
			ed2W2 = new double[W2.length][W2[0].length];
		}

		public void step(Worker worker) {
			validateTraining();

			double[][] gradW1 = ((Worker)worker).gradW1;
			double[][] gradW2 = ((Worker)worker).gradW2;
			double[][] gradE = ((Worker)worker).gradE;
			double[] gradb1 = ((Worker)worker).gradb1;

			for (int i = 0; i < W1.length; ++i) {
				for (int j = 0; j < W1[i].length; ++j) {
					eg2W1[i][j] = adaRho * eg2W1[i][j] + (1-adaRho) * gradW1[i][j] * gradW1[i][j];
					double delta = - gradW1[i][j] 
							* Math.sqrt(ed2W1[i][j] + adaEps)
							/ Math.sqrt(eg2W1[i][j] + adaEps);
					ed2W1[i][j] = adaRho * ed2W1[i][j] + (1-adaRho) * delta * delta;
					W1[i][j] += delta;
				}
			}

			for (int i = 0; i < b1.length; ++i) {
				eg2b1[i] = adaRho * eg2b1[i] + (1-adaRho) * gradb1[i] * gradb1[i];
				double delta = - gradb1[i]
						* Math.sqrt(ed2b1[i] + adaEps)
						/ Math.sqrt(eg2b1[i] + adaEps);
				ed2b1[i] = adaRho * ed2b1[i] + (1-adaRho) * delta * delta; 
				b1[i] += delta;
			}

			for (int i = 0; i < W2.length; ++i) {
				for (int j = 0; j < W2[i].length; ++j) {
					eg2W2[i][j] = adaRho * eg2W2[i][j] + (1-adaRho) * gradW2[i][j] * gradW2[i][j];
					double delta = - gradW2[i][j]
							* Math.sqrt(ed2W2[i][j] + adaEps)
							/ Math.sqrt(eg2W2[i][j] + adaEps);
					ed2W2[i][j] = adaRho * ed2W2[i][j] + (1-adaRho) * delta * delta; 
					W2[i][j] += delta;
				}
			}

			for (int i = 0; i < E.length; ++i) {
				for (int j = 0; j < E[i].length; ++j) {
					eg2E[i][j] = adaRho * eg2E[i][j] + (1-adaRho) * gradE[i][j] * gradE[i][j];
					double delta = - gradE[i][j]
							* Math.sqrt(ed2E[i][j] + adaEps)
							/ Math.sqrt(eg2E[i][j] + adaEps);
					ed2E[i][j] = adaRho * ed2E[i][j] + (1-adaRho) * delta * delta;
					E[i][j] += delta;
				}
			}
		}
		
	}
}
