package nl.cltl.ulm4.nlp;

import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.exec;
import static nl.cltl.ulm4.util.Pythonic.findIter;
import static nl.cltl.ulm4.util.Pythonic.iter_size;
import static nl.cltl.ulm4.util.Pythonic.tmpname;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PennTreebank {

	public static List<String> penn_sentences(String path) throws IOException {
		int bracket_count = 0;
		List<String> sentences = new ArrayList<>();
		List<String> sentence = new ArrayList<>();
		try (FileReader r = new FileReader(path);
				BufferedReader f = new BufferedReader(r);) {
			String line;
			while ((line = f.readLine()) != null) {
				sentence.add(line.trim());
				bracket_count = bracket_count + iter_size(findIter("(", line));
				bracket_count = bracket_count - iter_size(findIter(")", line));
				if (bracket_count == 0) {
					String sentenceStr = String.join(" ", sentence);
					if (!"".equals(sentenceStr)) {
						sentences.add(sentenceStr);
					}
					sentence.clear();
				}
			}
		}
		return sentences;
	}

	public static void jackknife(String path, String jkf_path, String err_path,
			String model_path, boolean append) throws IOException {
		int changed = 0;
		int total = 0;
		new File(jkf_path).getParentFile().mkdirs();
		File tmp = tmpname();
		String spt = "stanford-postagger-2015-12-09";
		String class_path = spt + "/stanford-postagger-3.6.0.jar:" + spt
				+ "/lib/slf4j-simple.jar:" + spt + "/lib/slf4j-api.jar";
		exec("java -classpath " + class_path
				+ " edu.stanford.nlp.tagger.maxent.MaxentTagger "
				+ "-props stanford-postagger-2015-12-09/penn-treebank.props "
				+ "-model " + model_path + " -testFile format=TREES," + path
				+ " > " + tmp + " 2> " + err_path);
		try (FileWriter out = new FileWriter(jkf_path, append);
				PrintWriter f = new PrintWriter(out);
				FileReader tmpInp = new FileReader(tmp);
				BufferedReader tmpReader = new BufferedReader(tmpInp);) {
			for (String tree_line : penn_sentences(path)) {
				String tmp_line = tmpReader.readLine();
				Iterator<String> word_pos_iter = Arrays.asList(
						tmp_line.split(" ")).iterator();
				Matcher m = Pattern.compile("\\((\\S+) (\\S+)\\)").matcher(
						tree_line);
				StringBuffer sb = new StringBuffer();
				while (m.find()) {
					String tree_pos = m.group(1), tree_word = m.group(2);
					String repl;
					if ("-NONE-".equals(tree_pos)) {
						// keep original POS tag
						repl = String.format("(%s %s)", tree_pos, tree_word);
					} else {
						// replace by predicted POS tag
						String[] parts = word_pos_iter.next().split("_");
						assert_(parts.length == 2);
						String tmp_word = parts[0], tmp_pos = parts[1];
						if (!tree_word.replaceAll("\\\\/", "/").replaceAll(
								"\\\\\\*", "*").equals(tmp_word)
								&& !tree_word.equals("theatre")
								&& !tree_word.equals("august")
								&& !tree_word.equals("neighbours")) {
							System.out.format("WARN: Differing original && "
									+ "POS tagged words: %s vs. %s\n", tree_word,
									tmp_word);
						}
						if (tree_pos != tmp_pos) {
							changed = changed + 1;
						}
						total = total + 1;
						repl = String.format("(%s %s)", tmp_pos, tree_word);
					}
					repl = repl.replaceAll("([\\$\\\\])", "\\\\$1");
					m.appendReplacement(sb, repl);
				}
				m.appendTail(sb);
				assert_(!word_pos_iter.hasNext());
				f.println(sb.toString());
			}
		}
		System.out.format("Changed: %d, total: %d, wrote to: %s.\n", changed, total, jkf_path);
		exec("grep \"Total tags right:\" " + err_path);
	}

}
