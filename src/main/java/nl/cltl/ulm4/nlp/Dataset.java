package nl.cltl.ulm4.nlp;


public class Dataset {

	// columns of sentence matrix
	public static final int COL_ID = 0;
	public static final int COL_START = 1;
	public static final int COL_LENGTH = 2;
	
	// columns of token matrix
	public static final int COL_WORD = 0;
	public static final int COL_POS = 1;
	public static final int COL_HEAD = 2;
	public static final int COL_LABEL = 3;
	
	//TODO: make private
	public int[][] sents;
	public int[][] tokens;
	
	public Dataset(int[][] sents, int[][] tokens) {
		super();
		this.sents = sents;
		this.tokens = tokens;
	}
	
}
