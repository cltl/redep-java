package nl.cltl.ulm4.nlp;

import nl.cltl.ulm4.dep.TransitionSystem;

public interface FeatureExtractor<S extends TransitionSystem> {

	int num();

	void extract(int[] x, S sys, Vocabulary[] vocabs);
	
}
