package nl.cltl.ulm4.nlp;

import java.io.Serializable;

public class Indexer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int index = -1;

	public int max() {
		return index;
	}

	public int next() {
		index = index + 1;
		return index;
	}

}
