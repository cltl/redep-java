package nl.cltl.ulm4.nlp;

import static nl.cltl.ulm4.dep.DependencyUtils.newVocabularies;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_POS;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_WORD;
import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_ID;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.nlp.Dataset.COL_LENGTH;
import static nl.cltl.ulm4.nlp.Dataset.COL_POS;
import static nl.cltl.ulm4.nlp.Dataset.COL_START;
import static nl.cltl.ulm4.nlp.Dataset.COL_WORD;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.error;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nl.cltl.ulm4.util.Tensors;

public class CoNLL {

	public Vocabulary[] vocabs;

	public CoNLL() {
		vocabs = newVocabularies();
		vocabs[VOCAB_WORD].getIndex("__MISSING__");
		vocabs[VOCAB_WORD].getIndex("__NONE__");
		vocabs[VOCAB_POS].getIndex("__MISSING__");
		vocabs[VOCAB_POS].getIndex("__NONE__");
	}
	
	public CoNLL(Vocabulary[] vocabs) {
		this.vocabs = vocabs;
	}

	public void prepare(String path, int cutoffThreshold, String name, int max_rows) throws IOException {
		prepare(Collections.singletonList(new File(path)), cutoffThreshold, name, max_rows);
	}

	public void prepare(List<File> path_iter, int cutoffThreshold, String name, int max_rows) throws IOException {
		System.out.println("Preparing dataset builder... ");
		build_dataset(path_iter, name, max_rows);
		if (cutoffThreshold > 1) {
			Indexer indexer = new Indexer();
			for (Vocabulary vocab : vocabs) {
				vocab.prune(cutoffThreshold, indexer);
			}
		}
		for (Vocabulary vocab : vocabs) {
			vocab.seal();
		}
		System.out.println("Preparing dataset builder... Done.");
	}

	public Dataset build_dataset(String path, String name, int max_rows) throws IOException {
		return build_dataset(Collections.singletonList(new File(path)), name, max_rows);
	}

	public Dataset build_dataset(List<File> files, String name, int max_rows) throws IOException {
		if (name == null)
			name = "[noname]";
		System.out.format("Building dataset \"%s\"...\n", name);
		long start = System.currentTimeMillis();
		int[][] sents = new int[max_rows/2][3];
		int[][] tokens = Tensors.fill(new int[max_rows][4], 0);
		int sent_count = 0;
		int token_count = 0;
		for (File file : files) {
			System.out.println(file.getAbsolutePath());
			for (List<String> lines : iter_sentences(file)) {
				sents[sent_count][COL_ID] = sent_count;
				sents[sent_count][COL_START] = token_count;
				token_count = parse_sentence(lines, tokens, token_count);
				sents[sent_count][COL_LENGTH] = token_count - sents[sent_count][1];
				sent_count = sent_count + 1;
			}
		}
		sents = Tensors.narrow(sents, 0, sent_count);
		tokens = Tensors.narrow(tokens, 0, token_count);
		long stop = System.currentTimeMillis();
		System.out.format("Building dataset \"%s\"... Done (%d tokens, %d sentences, %d s).\n",
						name, token_count, sent_count, (stop - start) / 1000);
		return new Dataset(sents, tokens);
	}

	public List<List<String>> iter_sentences(File path) throws IOException {
		List<List<String>> sentences = new ArrayList<>();
		try (FileReader r = new FileReader(path);
				BufferedReader f = new BufferedReader(r);) {
			String line = f.readLine();
			while (line != null) {
				List<String> lines = new ArrayList<>();
				while (line != null) {
					line = line.trim();
					if (line.length() == 0)
						break;
					if (!line.startsWith("#"))
						lines.add(line);
					line = f.readLine();
				}
				if (lines.size() > 0) {
					sentences.add(lines);
				}
				line = f.readLine();
			}
		}
		return sentences;
	}

	public int parse_sentence(List<String> lines, int[][] tokens,
			int token_count) {
		tokens[token_count][COL_WORD] = vocabs[VOCAB_WORD].getIndex("__ROOT__");
		tokens[token_count][COL_POS] = vocabs[VOCAB_POS].getIndex("__ROOT__");
		tokens[token_count][COL_HEAD] = -1;
		tokens[token_count][COL_LABEL] = vocabs[VOCAB_LABEL].getIndex("__NONE__");
		token_count++;
		for (String line : lines) {
			String[] fields = line.split("\t");
			tokens[token_count][COL_WORD] = vocabs[VOCAB_WORD].getIndex(fields[1]);
			tokens[token_count][COL_POS] = vocabs[VOCAB_POS].getIndex(fields[3]);
			if (fields[7] != "_") {
				int head_id = Integer.parseInt(fields[6]);
				tokens[token_count][COL_HEAD] = head_id;
				Integer f = vocabs[VOCAB_LABEL].getIndex(fields[7]);
				assert_(f != null, "Strange label: " + fields[7]);
				tokens[token_count][COL_LABEL] = f;
			}
			token_count++;
		}
		return token_count;
	}

	public void write_sentence(PrintWriter f, int[][] tokens) {
		for (int i = 1; i < tokens.length; i++) {
			String word = vocabs[VOCAB_WORD].getWord(tokens[i][0]);
			String cpos = vocabs[VOCAB_POS].getWord(tokens[i][1]);
			String lemma = "_";
			int head = 1000; // dummy value;
			String head_label = "_";
			if (tokens[i][3] > 0) {
				head = tokens[i][2];
				head_label = vocabs[VOCAB_LABEL].getWord(tokens[i][3]);
			}
			f.format("%d\t%s\t%s\t%s\t_\t_\t%d\t%s\t_\t_", i, word, lemma,
					cpos, head, head_label);
			f.write("\n");
		}
	}

	public void write_sentence(String path, int[][] tokens) throws IOException {
		write_sentence(new File(path), tokens);
	}

	public void write_sentence(File file, int[][] tokens) throws IOException {
		try (PrintWriter f = new PrintWriter(file);) {
			write_sentence(f, tokens);
		}
	}

	public void write_all_sentences(String path, Dataset ds) throws IOException {
		write_all_sentences(new File(path), ds);
	}

	public void write_all_sentences(File file, Dataset ds) throws IOException {
		try (PrintWriter f = new PrintWriter(file, "UTF-8");) {
			for (int s = 0; s < ds.sents.length; s++) {
				int[][] tokens = Tensors.narrow(ds.tokens, ds.sents[s][1], ds.sents[s][2]);
				write_sentence(f, tokens);
				f.write("\n");
			}
		}
	}

	public void substitue_dependency(String input, String output, Dataset ds, int[][] links) throws IOException {
		/*
		 * Read a CoNLL file in `input`, replace all dependency links with
		 * those; specified by `ds` && write to `output`.
		 */
		try (PrintWriter f = new PrintWriter(output);) {
			int s = 0;
			for (List<String> lines : iter_sentences(new File(input))) {
				int t = ds.sents[s][1]+1; // skip root node
				for (String line : lines) {
					String[] fields = line.split("\t");
					if (ds.tokens[t][2] >= 0) {
						assert_(Integer.parseInt(fields[6]) == ds.tokens[t][COL_HEAD]);
						assert_(vocabs[VOCAB_LABEL].getIndex(fields[7]) == ds.tokens[t][COL_LABEL]);
						assert_(links[t][0] >= 0, "Some token doesn't have a head");
						fields[6] = Integer.toString(links[t][0]);
						String rel_name = vocabs[VOCAB_LABEL].getWord(
								links[t][1]);
						if (rel_name == null) {
							error("Unknown relation index: " + links[t][1]);
						}
						fields[7] = rel_name;
					} else {
						fields[6] = "";
						fields[7] = "";
					}
					f.write(String.join("\t", fields));
					f.write("\n");
					t = t + 1;
				}
				assert_(t == ds.sents[s][1] + ds.sents[s][2]);
				f.write("\n");
				s = s + 1;
			}
			assert_(s == ds.sents.length);
			f.close();
		}
	}
}