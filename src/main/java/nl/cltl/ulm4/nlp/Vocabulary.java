package nl.cltl.ulm4.nlp;

import static nl.cltl.ulm4.util.Pythonic.assert_;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Vocabulary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Indexer indexer;
	public Map<String, Integer> word2index = new HashMap<>();
	private Map<String, Integer> word2count = new HashMap<>();
	public Map<Integer, String> index2word = new HashMap<>();
	private boolean sealed = false;

	public Vocabulary() {
		this(new Indexer());
	}

	public Vocabulary(Indexer indexer) {
		this.indexer = indexer;
	}

	public void add_all(Vocabulary v) {
		for (String word : v.word2index.keySet()) {
			getIndex(word);
		}
	}

	public boolean contains(String word) {
		return word2index.containsKey(word);
	}
	
	public int getIndex(String word) {
		if (word == null)
			word = "__NONE__";
		if (word2index.containsKey(word)) {
			word2count.put(word, word2count.get(word) + 1);
		} else {
			if (sealed) {
				word = "__MISSING__";
			} else {
				int index = indexer.next();
				word2index.put(word, index);
				index2word.put(index, word);
				word2count.put(word, 1);
			}
		}
		return word2index.get(word);
	}

	public String getWord(int index) {
		return index2word.get(index);
	}

	public void prune(int minFrequency) {
		prune(minFrequency, null);
	}

	public void prune(int min_count, Indexer new_indexer) {
		assert_(!sealed);
		if (new_indexer == null)
			new_indexer = new Indexer();
		indexer = new_indexer;
		Map<Integer, String> oldIndex2word = index2word;
		index2word = new HashMap<>();
		for (String word : oldIndex2word.values()) {
			if (word2count.get(word) >= min_count) {
				int index = indexer.next();
				word2index.put(word, index);
				index2word.put(index, word);
			} else {
				word2index.remove(word);
				word2count.remove(word);
			}
		}
	}

	public void seal() {
		sealed = true;
	}

	public int size() {
		return word2index.size();
	}

	public static int max_index(Vocabulary[] vocabs) {
		return max_index(Arrays.asList(vocabs));
	}

	public static int max_index(Collection<Vocabulary> vocabs) {
		assert_(!vocabs.isEmpty());
		Indexer indexer = null;
		for (Vocabulary v : vocabs) {
			if (indexer == null) {
				indexer = v.indexer;
			}
			assert_(indexer == v.indexer);
		}
		return indexer.max();
	}

	public static HashMap<String, double[]> read_word2vec_bin(String path,
			boolean normalized, boolean new_format) throws IOException {
		Word2Vec word2vec = new Word2Vec();
		word2vec.loadModel(path, normalized);
		return word2vec.getWordMap();
	}

}
