package nl.cltl.ulm4.redep_experiment;

import static nl.cltl.ulm4.util.Pythonic.concatPaths;

import java.io.File;
import java.util.Random;

public class Config {

	public static String data_dir = "penntree/wsj";
	public static String out_dir = "output/dep";
	public static boolean jackknifeEnabled = true; 
	public static boolean jackknife_cache_models = true;
	public static String jackknife_dir = concatPaths(out_dir, "penntree.jackknife");
	public static String embeddings_path = "embeddings/merged.txt";

	public static String sd_dir = concatPaths(out_dir, "penntree.sd");
	public static String sd_train_path = concatPaths(sd_dir, "train.mrg.dep");
	public static String sd_valid_path = concatPaths(sd_dir, "valid.mrg.dep");
	public static String sd_test_path = concatPaths(sd_dir, "test.mrg.dep");

	public static String lth_dir = concatPaths(out_dir, "penntree.lth");
	public static String lth_train_path = concatPaths(lth_dir, "train.mrg.dep");
	public static String lth_valid_path = concatPaths(lth_dir, "valid.mrg.dep");
	public static String lth_test_path = concatPaths(lth_dir, "test.mrg.dep");

	public static long rand_seed = 20151213;
	public static Random rand = new Random(rand_seed);

	static {
		new File(out_dir).mkdirs();
	}
	
}
