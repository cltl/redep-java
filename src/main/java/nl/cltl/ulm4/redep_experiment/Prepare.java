package nl.cltl.ulm4.redep_experiment;

import static nl.cltl.ulm4.util.CollectionUtils.kfold_contiguous;
import static nl.cltl.ulm4.util.Pythonic.concatPaths;
import static nl.cltl.ulm4.util.Pythonic.exec;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import nl.cltl.ulm4.nlp.PennTreebank;
import nl.cltl.ulm4.util.FileUtils;

public class Prepare {

	public static Stream<File> iter_paths_in_sections(int from, int to,
			String dir) {
		return IntStream
				.rangeClosed(from, to)
				.boxed()
				.flatMap(
						i -> FileUtils.find_files_recursively(null,
								new File(String.format("%s/%02d", dir, i)))
								.stream());
	}

	public static void erase_and_concat(Stream<File> inp_paths, String out_path) {
		exec("echo '' > " + out_path);
		inp_paths.forEach(path -> exec("cat " + path + " >> " + out_path));
	}

	public static void write_iter_to_file(Iterable<String> iter, String path)
			throws IOException {
		try (PrintWriter f = new PrintWriter(path, "UTF-8");) {
			for (String s : iter) {
				f.println(s);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		String penn_dir = concatPaths(Config.out_dir, "penntree");
		new File(penn_dir).mkdirs();
		String penn_train_path = concatPaths(penn_dir, "train.mrg");
		String penn_valid_path = concatPaths(penn_dir, "valid.mrg");
		String penn_test_path = concatPaths(penn_dir, "test.mrg");
		erase_and_concat(iter_paths_in_sections(2, 21, Config.data_dir),
				penn_train_path);
		erase_and_concat(iter_paths_in_sections(22, 22, Config.data_dir),
				penn_valid_path);
		erase_and_concat(iter_paths_in_sections(23, 23, Config.data_dir),
				penn_test_path);

		if (Config.jackknifeEnabled) {
			// Measure POS tagging accuracy

			new File(Config.jackknife_dir).mkdirs();
			String jkf_train_path = concatPaths(Config.jackknife_dir,
					"train.mrg");
			String jkf_valid_path = concatPaths(Config.jackknife_dir,
					"valid.mrg");
			String jkf_test_path = concatPaths(Config.jackknife_dir, "test.mrg");

			String spt = "stanford-postagger-2015-12-09";
			String class_path = spt + "/stanford-postagger-3.6.0.jar:" + spt
					+ "/lib/slf4j-simple.jar:" + spt + "/lib/slf4j-api.jar";
			List<String> data_gen = PennTreebank
					.penn_sentences(penn_train_path);
			exec("echo '' > " + jkf_train_path);
			int i = 0;
			for (Map<String, Iterable<String>> fold : kfold_contiguous(
					data_gen, 10)) {
				String jkf_model_path = concatPaths(Config.out_dir,
						String.format("jackknife-%02d.model", i));
				if (Config.jackknife_cache_models
						&& new File(jkf_model_path).exists()) {
					System.out.println("Model exists at " + jkf_model_path
							+ ". Aborted training.");
				} else {
					String model_train_path = concatPaths(Config.out_dir,
							String.format("train_jackknife_%02d.mrg", i));
					write_iter_to_file(fold.get("train"), model_train_path);
					exec("java -mx2g -classpath "
							+ class_path
							+ " edu.stanford.nlp.tagger.maxent.MaxentTagger "
							+ "-props stanford-postagger-2015-12-09/penn-treebank.props "
							+ "-model " + jkf_model_path
							+ " -trainFile format=TREES," + model_train_path);
				}

				String model_test_path = concatPaths(Config.out_dir,
						String.format("test_jackknife_%02d.mrg", i));
				write_iter_to_file(fold.get("test"), model_test_path);
				String model_err_path = concatPaths(Config.out_dir,
						String.format("jackknife-%02d.out", i));
				PennTreebank.jackknife(model_test_path, jkf_train_path,
						model_err_path, jkf_model_path, true);
				System.out.format(
						"Tagging and measuring accuracy fold %02d... Done.\n", i);
				i++;
			}

			// train POS tagger on train dataset && apply on development && test
			// set
			String jkf_model_path = concatPaths(Config.out_dir,
					"jackknife-all.model");
			if (Config.jackknife_cache_models
					&& new File(jkf_model_path).exists()) {
				System.out.println("Model exists at " + jkf_model_path
						+ ". Aborted training.");
			} else {
				exec("java -mx2g -classpath "
						+ class_path
						+ " edu.stanford.nlp.tagger.maxent.MaxentTagger "
						+ "-props stanford-postagger-2015-12-09/penn-treebank.props "
						+ "-model " + jkf_model_path
						+ " -trainFile format=TREES," + penn_train_path);
			}
			String err_valid_path = concatPaths(Config.out_dir,
					String.format("jackknife-valid-%02d.err", i));
			String err_test_path = concatPaths(Config.out_dir,
					String.format("jackknife-test-%02d.err", i));
			PennTreebank.jackknife(penn_valid_path, jkf_valid_path,
					err_valid_path, jkf_model_path, false);
			PennTreebank.jackknife(penn_test_path, jkf_test_path,
					err_test_path, jkf_model_path, false);

			System.out.println("Jackknifing POS tags... Done");

			// replacing Penn sentences with jackknifed ones
			penn_dir = Config.jackknife_dir;
		}

		// Convert to dependency using LTH converter
		penn2lth(new File(penn_dir), new File(Config.lth_dir));

		// Convert to dependency using Stanford converter
		penn2sd(new File(penn_dir), new File(Config.sd_dir));
	}

	public static void penn2lth(File inp_dir_or_file, File out_dir_or_file) {
		if (inp_dir_or_file.isFile()) {
			out_dir_or_file.getParentFile().mkdirs();
			exec("java -jar pennconverter/pennconverter.jar "
					+ "-stopOnError=False -raw < %s > %s.dep", inp_dir_or_file,
					out_dir_or_file);
		} else {
			for (File inp_path : FileUtils.find_files_recursively(null, inp_dir_or_file)) {
				File out_path = new File(out_dir_or_file, inp_path.getName());
				penn2lth(inp_path, out_path);
			}
		}
	}

	public static void penn2sd(File inp, File out) {
		if (inp.isFile()) {
			out.getParentFile().mkdirs();
			exec("java -cp stanford-parser-full-2013-11-12/stanford-parser.jar "
					+ "edu.stanford.nlp.trees.EnglishGrammaticalStructure -basic -conllx "
					+ "-treeFile %s > %s.dep", inp.toString(), out.toString());
		} else {
			for (File inp_path : FileUtils.find_files_recursively(null, inp)) {
				File out_path = new File(inp_path.toString().replaceFirst(
						Pattern.quote(inp.toString()), out.toString()));
				penn2sd(inp_path, out_path);
			}
		}
	}

}
