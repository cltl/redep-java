package nl.cltl.ulm4.redep_experiment;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_POS;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_WORD;
import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.nlp.Dataset.COL_LENGTH;
import static nl.cltl.ulm4.nlp.Dataset.COL_POS;
import static nl.cltl.ulm4.nlp.Dataset.COL_START;
import static nl.cltl.ulm4.nlp.Dataset.COL_WORD;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.Parser;
import nl.cltl.ulm4.dep.TransitionSystem;
import nl.cltl.ulm4.dep.oracle.DynamicArcStandardOracle;
import nl.cltl.ulm4.dep.oracle.DynamicOracle;
import nl.cltl.ulm4.nlp.CoNLL;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nlp.Vocabulary;
import nl.cltl.ulm4.util.Tensors;

public class CompareParsers {
	
	private DynamicOracle myOracle = new DynamicArcStandardOracle();
	
	@Parameter(names={"-model1"}, required=true,
			description="path to the first parser (Stanford or Java format)")
	private String modelPath1;

	@Parameter(names={"-model2"}, required=true,
			description="path to the first parser (Stanford or Java format)")
	private String modelPath2;

	@Parameter(names={"-input"}, required=true, description="path to input file")
	private String inputPath;

	@Parameter(names={"-rootLabel"}, description="a string for root label, different when using LTH || Stanford dependencies")
	private String rootLabel = "root";

	@SuppressWarnings("unchecked")
	private Parser<? extends TransitionSystem> load(String path) throws IOException, ClassNotFoundException {
		Parser<? extends TransitionSystem> parser;
		if (path.endsWith(".txt") || path.endsWith(".txt.gz")) {
			parser = Parser.load_stanford_model(path, rootLabel);
		} else {
			try (FileInputStream fis = new FileInputStream(path);
					ObjectInputStream ois = new ObjectInputStream(fis);) {
				parser = (Parser<TransitionSystem>) ois.readObject();
			}
		}
		return parser;
	}
	
	private String action2str(int action, ActionsInfo info, Vocabulary[] vocabs) {
		String complex = info.vocab.getWord(action);
		String type = complex.replaceAll("\\(\\w+\\)", "");
		Matcher m = Pattern.compile("\\((\\w+)\\)").matcher(complex);
		String label = m.find() ? m.group(1) : "";
		return String.format("['%s', '%s']", type, label);
	}
	
	private void run() throws ClassNotFoundException, IOException {
		Parser<? extends TransitionSystem> parser1 = load(modelPath1);
		Parser<? extends TransitionSystem> parser2 = load(modelPath2);
		Parser<? extends TransitionSystem>.Worker worker1 = parser1.newWorker();
		Parser<? extends TransitionSystem>.Worker worker2 = parser2.newWorker();
		
		CoNLL conll = new CoNLL();
		conll.vocabs = parser1.vocabs;
		parser2.vocabs = parser1.vocabs;
		int max_rows = (int) (Files.lines(Paths.get(inputPath)).count() + 1);
		Dataset ds = conll.build_dataset(inputPath, "", max_rows);
		// for debugging
		//	ds.sents = Tensors.narrow(ds.sents, 0, 1);
	
		System.out.println("Parsing...");
		parser1.getNeuralNetwork().updatePrecomputedVectors();
		parser2.getNeuralNetwork().updatePrecomputedVectors();
		
		int total1 = 0, total2 = 0;
		int oneBetter = 0, twoBetter = 0;
		for (int s = 0; s < ds.sents.length; s++) {
			int start = ds.sents[s][COL_START];
			int num = ds.sents[s][COL_LENGTH];
			int[][] tokens = Tensors.narrow(ds.tokens, start, num);
			TransitionSystem sys1 = worker1.getTransitionSystem();
			TransitionSystem sys2 = worker2.getTransitionSystem();
			sys1.reset(tokens);
			sys2.reset(tokens);
			List<Integer> actions1 = new ArrayList<>();
			List<Integer> actions2 = new ArrayList<>();
			// run both through the same actions until the first mistake
			while (!sys1.isTerminated() && myOracle.loss(sys1) == 0) {
				int action = worker1.step();
				sys2.execute(action);
				actions1.add(action);
				actions2.add(action);
			}
			// run them differently to see which one is better
			if (!sys1.isTerminated()) {
				int firstError = actions1.size();
				while (!sys1.isTerminated()) {
					actions1.add(worker1.step());
				}
				while (!sys2.isTerminated()) {
					actions2.add(worker2.step());
				}
				int loss1 = myOracle.loss(sys1);
				int loss2 = myOracle.loss(sys2);
				total1 += loss1;
				total2 += loss2;
				if (loss1 < loss2) {
					oneBetter ++;
				} else if (loss2 < loss1) {
					twoBetter ++;
				}
				if (loss2 < loss1) {
					// print sentence
					System.out.println("tokens = [");
					for (int i = 1; i < tokens.length; i++) {
						System.out.format("    ['%s', '%s', %d, '%s'],\n",
								conll.vocabs[VOCAB_WORD].getWord(tokens[i][COL_WORD]).replaceAll("'", "\\\\'"),
								conll.vocabs[VOCAB_POS].getWord(tokens[i][COL_POS]).replaceAll("'", "\\\\'"),
								tokens[i][COL_HEAD]-1,
								conll.vocabs[VOCAB_LABEL].getWord(tokens[i][COL_LABEL]));
					}
					System.out.println("]");
					// print system 1's transitions
					System.out.format("sys = ArcStandard([t[0] for t in tokens])\n");
					System.out.format("print 'First error at: %d'\n", firstError);
					System.out.format("print 'loss = %d'\n", loss1);
					System.out.format("demo2(sys, [");
					for (Integer action : actions1) {
						System.out.format("%s, ", action2str(action, sys1.getInfo(), conll.vocabs));
					}
					System.out.format("])\n");
					// print system 2's transitions
					System.out.format("print 'loss = %d'\n", loss2);
					System.out.format("demo2(sys, [");
					for (Integer action : actions2) {
						System.out.format("%s, ", action2str(action, sys2.getInfo(), conll.vocabs));
					}
					System.out.format("])\n");
					System.out.println();
				}
			}
		}
		System.out.format("%d cases where parser 1 is better than parser 2\n", oneBetter);
		System.out.format("%d cases where parser 2 is better than parser 1\n", twoBetter);
		System.out.format("Total loss of system 1: %d\n", total1);
		System.out.format("Total loss of system 2: %d\n", total2);
	}
	
	public static void main(String[] args) throws Exception {
		System.out.format("Command line parameters: %s\n", Arrays.toString(args));
		CompareParsers cp = new CompareParsers();
		new JCommander(cp, args);
		cp.run();
	}
	
}
