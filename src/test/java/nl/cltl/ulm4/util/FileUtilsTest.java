package nl.cltl.ulm4.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.Test;

public class FileUtilsTest {

	@Test
	public void many() {
		String path = this.getClass().getResource("/").getPath();
		List<File> files = FileUtils.find_files_recursively(null, new File(path));
		for (File file : files) {
			assertTrue(file.isFile());
		}
		assertTrue(files.size() > 10);
	}

	@Test
	public void one() {
		String path = this.getClass().getResource("/1-sent.conll").getPath();
		List<File> files = FileUtils.find_files_recursively(null, new File(path));
		assertEquals(1, files.size());
	}

	@Test
	public void none() {
		String path = "/no/such/file";
		List<File> files = FileUtils.find_files_recursively(null, new File(path));
		assertEquals(0, files.size());
	}
	
}
