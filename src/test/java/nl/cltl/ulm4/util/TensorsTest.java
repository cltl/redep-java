package nl.cltl.ulm4.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TensorsTest {

	@Test
	public void expCloseToMath_exp() {
		for (double a = -0.1; a < 0.1; a += 0.001) {
			assertEquals(Math.exp(a), Tensors.exp(a), 0.1);
		}
	}

	@Test
	public void expNonNegative() {
		assertTrue(Tensors.exp(-10000) >= 0);
	}

	@Test
	public void expFinite() {
		assertTrue(Double.isFinite(Tensors.exp(100)));
		System.out.println(Math.log(Tensors.exp(107)));
	}
	
}
