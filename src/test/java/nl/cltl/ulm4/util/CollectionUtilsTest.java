package nl.cltl.ulm4.util;

import static nl.cltl.ulm4.util.CollectionUtils.sample;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.len;
import static nl.cltl.ulm4.util.Pythonic.range;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Test;

public class CollectionUtilsTest {

	private Random random = new Random(73874231);

	@Test public void kfold_contiguous() {
	    List<Integer> iter_gen = range(20);
	    List<Map<String, Iterable<Integer>>> folds = CollectionUtils.kfold_contiguous(iter_gen, 10);

	    Iterator<Integer> it = folds.get(0).get("test").iterator();
	    assert_(it.next() == 0);
	    assert_(it.next() == 1);
	    assert_(len(folds.get(0).get("test")) == 2);
	    assert_(folds.get(0).get("train").iterator().next() == 2);
	    assert_(len(folds.get(0).get("train")) == 18);

	    assert_(folds.get(1).get("test").iterator().next() == 2);
	    assert_(len(folds.get(1).get("test")) == 2);
	    assert_(folds.get(1).get("train").iterator().next() == 0);
	    assert_(len(folds.get(1).get("train")) == 18);

	    assert_(folds.get(9).get("test").iterator().next() == 18);
	    assert_(len(folds.get(9).get("test")) == 2);
	    assert_(folds.get(9).get("train").iterator().next() == 0);
	    assert_(len(folds.get(9).get("train")) == 18);
	}

	@Test
	public void sampleZero() {
		int[] items = sample(new ArrayList<>(), 1, random);
		assertEquals(0, items.length);
		items = sample(Arrays.asList(0.25, 0.25, 0.25, 0.25), 0, random);
		assertEquals(0, items.length);
	}

	@Test
	public void sampleOne() {
		ArrayList<Double> probs = new ArrayList<>(Arrays.asList(20.0));
		int[] items = sample(probs, 1, random);
		assertArrayEquals(new int[] {0}, items);
		items = sample(probs, 2, random);
		assertArrayEquals(new int[] {0}, items);
	}

	@Test
	public void sampleSmallList() {
		int[] counters0 = new int[3];
		int[] counters1 = new int[3];
		int n = 10000;
		for (int i = 0; i < n; i++) {
			List<Double> probs = Arrays.asList(0.7, 0.25, 0.05);
			int[] items = sample(probs, 3, random);
			boolean[] seen = new boolean[3];
			for (int item : items) {
				seen[item] = true;
			}
			counters0[items[0]]++;
			counters1[items[1]]++;
			for (boolean b : seen) {
				assertTrue(b);
			}
		}
		assertEquals(0.7, counters0[0]/(double)n, 0.01);
		assertEquals(0.25, counters0[1]/(double)n, 0.01);
		assertEquals(0.05, counters0[2]/(double)n, 0.01);
		assertEquals(0.25*0.7/(1-0.25) + 0.05*0.7/(1-0.05), counters1[0]/(double)n, 0.1);
		assertEquals(0.7*0.25/(1-0.7) + 0.05*0.25/(1-0.05), counters1[1]/(double)n, 0.1);
		assertEquals(0.25*0.05*(1-0.25) + 0.7*0.05/(1-0.7), counters1[2]/(double)n, 0.1);
	}

	@Test
	public void sampleUnnormalized() {
		int[] counters = new int[3];
		int n = 10000;
		for (int i = 0; i < n; i++) {
			List<Double> probs = Arrays.asList(7.0, 2.0, 1.0);
			int[] items = sample(probs, 3, random);
			counters[items[0]]++;
		}
		assertEquals(0.7, counters[0]/(double)n, 0.01);
		assertEquals(0.2, counters[1]/(double)n, 0.01);
		assertEquals(0.1, counters[2]/(double)n, 0.01);
	}

	@Test
	public void sampleSmallProbability() {
		int[] counters = new int[3];
		int n = 100000;
		for (int i = 0; i < n; i++) {
			List<Double> probs = Arrays.asList(0.5, 0.5, 1e-4);
			int[] items = sample(probs, 3, random);
			counters[items[0]]++;
		}
		assertTrue(counters[2] > 0); // with prob. 0.99995462276604
	}

}
