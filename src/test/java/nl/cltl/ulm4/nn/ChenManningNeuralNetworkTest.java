package nl.cltl.ulm4.nn;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import nl.cltl.ulm4.dep.SupervisedDataset;
import nl.cltl.ulm4.nn.ChenManningNeuralNetwork.Worker;
import nl.cltl.ulm4.util.IntPair;
import nl.cltl.ulm4.util.Tensors;

import org.junit.Test;

public class ChenManningNeuralNetworkTest {

	private ChenManningNeuralNetwork mlp;
	private Random r = new Random(103824323);

	public ChenManningNeuralNetworkTest() {
		mlp = new ChenManningNeuralNetwork(1000, 50, 48, 200, 5);
		int[][] masks = { {0}, {1,2,3,4}, {0,1,2,3,4} };
		mlp.setEligibleActionsMap(masks);
		mlp.initParameters(r, 0.01);
	}
	
	@Test
	public void forwardSimple() {
		int[] x = Tensors.random(r, 0, 1000, 48);
		Worker w = mlp.new Worker();
		
		assertEquals(0, w.forward(x, 0));
		assertEquals(1, w.getProbs()[0], 1e-8);
		
		int a = w.forward(x, 1);
		assertNotEquals(0, a);
		assertNotEquals(1, w.getProbs()[a], 1e-8);
	}

	@Test
	public void forwardPrecomputed() {
		List<IntPair> toPrecompute = Arrays.asList(new IntPair[] {
				new IntPair(0, 1), new IntPair(1, 23), new IntPair(2, 11),
				new IntPair(2, 12), new IntPair(0, 13), new IntPair(0, 14)
		});
		mlp.precompute(toPrecompute);
		int[] x = Tensors.random(r, 0, 1000, 48);
		Worker w = mlp.new Worker();
		
		assertEquals(0, w.forward(x, 0));
		assertEquals(1, w.getProbs()[0], 1e-8);
		
		int a = w.forward(x, 1);
		assertNotEquals(0, a);
		assertNotEquals(1, w.getProbs()[a], 1e-8);
	}

	@Test
	public void forwardBackwardSingleExample() {
		ChenManningNeuralNetwork mlp = new ChenManningNeuralNetwork(1000, 50, 4, 200, 5);
		  Random r = new Random(1034239834);
		  for (int i = 0; i < mlp.E.length; i++) {
			for (int j = 0; j < mlp.E[0].length; j++) {
				mlp.E[i][j] = r.nextDouble() * 0.02 - 0.01;
			}
		  }
		  for (int i = 0; i < mlp.W1.length; i++) {
			for (int j = 0; j < mlp.W1[0].length; j++) {
				mlp.W1[i][j] = r.nextDouble() * 0.02 - 0.01;
			}
		  }
		  for (int i = 0; i < mlp.b1.length; i++) {
			  mlp.b1[i] = r.nextDouble() * 0.02 - 0.01;
		  }
		  for (int i = 0; i < mlp.W2.length; i++) {
			for (int j = 0; j < mlp.W2[0].length; j++) {
				mlp.W2[i][j] = r.nextDouble() * 0.02 - 0.01;
			}
		  }
		mlp.setEligibleActionsMap(new int[][] { {0,2,3} });
		SupervisedDataset ds = new SupervisedDataset();
		ds.x = new int[][] { {0, 1, 3, 5} };
		ds.states = new int[] { 0 };
		ds.y = new int[] { 3 };
		mlp.initTraining(r, 0.5, 1e-8, mlp.new AdaGrad(0.1, 1e-6), 1);
		Worker w = mlp.new Worker();
		w.initTraining(new Random(34234025));
		w.forwardBackward(ds, 0);
        w.addL2Regularization();
        mlp.optim.step(w);

        w.forwardBackward(ds, 0);
        w.addL2Regularization();
        mlp.optim.step(w);
	}
	
	public void forwardBackwardSimple() {
		SupervisedDataset ds = new SupervisedDataset();
		ds.x = Tensors.random(r, 0, 1000, 10, 48);
		ds.states = Tensors.fill(new int[10], 2);
		ds.y = Tensors.random(r, 0, 5, 10);
		mlp.initParameters(r, 0.01);
		mlp.initTraining(r, 0, 1e-8, mlp.new AdaGrad(0.1, 1e-6), 1);
		Worker w = mlp.new Worker();
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		double lastCost = w.cost;
		for (int i = 0; i < 10; i++) {
			mlp.optim.step(w);
			IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
			double cost = w.cost;
			assertTrue(cost < lastCost);
			lastCost = cost;
		}
	}

	@Test
	public void forwardBackwardWithDropout() {
		SupervisedDataset ds = new SupervisedDataset();
		ds.x = Tensors.random(r, 0, 1000, 10, 48);
		ds.states = Tensors.fill(new int[10], 2);
		ds.y = Tensors.random(r, 0, 5, 10);
		mlp.initParameters(r, 0.01);
		mlp.initTraining(r, 0.5, 1e-8, mlp.new AdaGrad(0.1, 1e-6), 1);
		Worker w = mlp.new Worker();
		w.initTraining(new Random());
		
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		mlp.optim.step(w);
		
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		mlp.optim.step(w);
		
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		mlp.optim.step(w);
		
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		mlp.optim.step(w);
	}
	
	@Test
	public void forwardBackwardWithL2() {
		SupervisedDataset ds = new SupervisedDataset();
		ds.x = Tensors.random(r, 0, 1000, 10, 48);
		ds.states = Tensors.fill(new int[10], 2);
		ds.y = Tensors.random(r, 0, 5, 10);
		mlp.initParameters(r, 0.01);
		mlp.initTraining(r, 0, 1e-8, mlp.new AdaGrad(0.1, 1e-6), 1);
		Worker w = mlp.new Worker();
		
		w.initTraining(new Random());
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		w.addL2Regularization();
		double cost0 = w.cost;
		mlp.optim.step(w);
		
		w.initTraining(new Random());
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		w.addL2Regularization();
		double cost1 = w.cost;
		assertTrue(String.format("cost1 (%s) should have been smaller than "
				+ "cost0 (%s)", cost1, cost0), cost1 < cost0);
		mlp.optim.step(w);
		
		w.initTraining(new Random());
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		w.addL2Regularization();
		double cost2 = w.cost;
		assertTrue(cost2 < cost1);
		mlp.optim.step(w);
		
		w.initTraining(new Random());
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		w.addL2Regularization();
		double cost3 = w.cost;
		assertTrue(cost3 < cost2);
	}

	@Test
	public void numericalGradientChecking() {
		SupervisedDataset ds = new SupervisedDataset();
		ds.x = Tensors.random(r, 0, 1000, 10, 48);
		ds.states = Tensors.fill(new int[10], 2);
		ds.y = Tensors.random(r, 0, 5, 10);
		mlp.initParameters(r, 0.01);
		
		mlp.initTraining(r, 0, 1e-8, mlp.new AdaGrad(0.1, 1e-6), 1);
		Worker w = mlp.new Worker();
		w.initTraining(new Random(234234));
		
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		double cost1 = w.cost;
		double dw1_23_computed = w.gradW1[2][3];
		double db1_5_computed = w.gradb1[5];
		double dbe_92_computed = w.gradE[9][2];
		double dw2_07_computed = w.gradW2[0][7];

		w.initTraining(new Random(234234));
		double old_w2_07 = mlp.W2[0][7];
		mlp.W2[0][7] += 1e-6;
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		double cost2 = w.cost;
		double dw2_07_estimated = (cost2 - cost1) / 1e-6;
		assertEquals(dw2_07_estimated, dw2_07_computed, 1e-6);
		mlp.W2[0][7] = old_w2_07;
		
		w.initTraining(new Random(234234));
		double old_w1_23 = mlp.W1[2][3];
		mlp.W1[2][3] += 1e-6;
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		cost2 = w.cost;
		double dw1_23_estimated = (cost2 - cost1) / 1e-6;
		assertEquals(dw1_23_estimated, dw1_23_computed, 1e-6);
		mlp.W1[2][3] = old_w1_23;
		
		w.initTraining(new Random(234234));
		double old_b1_5 = mlp.b1[5];
		mlp.b1[5] += 1e-6;
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		cost2 = w.cost;
		double db1_5_estimated = (cost2 - cost1) / 1e-6;
		assertEquals(db1_5_estimated, db1_5_computed, 1e-6);
		mlp.b1[5] = old_b1_5;
		
		w.initTraining(new Random(234234));
		double old_e_92 = mlp.E[9][2];
		mlp.E[9][2] += 1e-6;
		IntStream.range(0, 10).forEach(e -> w.forwardBackward(ds, e));
		cost2 = w.cost;
		double de_92_estimated = (cost2 - cost1) / 1e-6;
		assertEquals(de_92_estimated, dbe_92_computed, 1e-6);
		mlp.E[9][2] = old_e_92;
	}
	
	@Test
	public void serializable() throws IOException, ClassNotFoundException {
		ChenManningNeuralNetwork original = new ChenManningNeuralNetwork(100, 3, 5, 7, 11);
		original.initParameters(new Random(), 0.1);
		original.setEligibleActionsMap(Tensors.random(new Random(), 0, 5, 10, 3));
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(bos);) {
			oos.writeObject(original);
			byte[] binaryData = bos.toByteArray();
			try (ByteArrayInputStream bis = new ByteArrayInputStream(binaryData);
					ObjectInputStream ois = new ObjectInputStream(bis);) {
				ChenManningNeuralNetwork copy = (ChenManningNeuralNetwork) ois.readObject();
				
				double[][] origE = original.getEmbeddingWeights();
				double[][] copyE = copy.getEmbeddingWeights();
				for (int i = 0; i < origE.length; i++) {
					assertArrayEquals(origE[i], copyE[i], 0);
				}
				
				int[][] origEligibleOutputs = original.getMasks();
				int[][] copyEligibleOutputs = copy.getMasks();
				for (int i = 0; i < origEligibleOutputs.length; i++) {
					assertArrayEquals(origEligibleOutputs[i], copyEligibleOutputs[i]);
				}
			}
		}
	}
	
}
