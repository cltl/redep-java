package nl.cltl.ulm4.nlp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Test;

public class VocabularyTest {

	@Test
	public void word2vec_bin() throws IOException {
		String path = this.getClass().getResource("/vectors-small.bin").getPath();
		HashMap<String, double[]> v = Vocabulary.read_word2vec_bin(path, false, true);
		assertEquals(42, v.size());
		assertEquals(200, v.get("</s>").length);
		assertTrue(v.containsKey("</s>"));
		assertTrue(v.containsKey("of"));
		assertTrue(v.containsKey("Rockwell"));
		assertTrue(v.containsKey("CorpPUNT"));
		assertTrue(v.containsKey("'s"));
		assertFalse(v.containsKey("!!"));
	}

}
