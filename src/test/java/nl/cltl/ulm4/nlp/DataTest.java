package nl.cltl.ulm4.nlp;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_POS;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_WORD;
import static nl.cltl.ulm4.util.Pythonic.assert_;
import static nl.cltl.ulm4.util.Pythonic.concatPaths;
import static nl.cltl.ulm4.util.Pythonic.newHashMap;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import nl.cltl.ulm4.util.Pythonic;

import org.junit.BeforeClass;
import org.junit.Test;

public class DataTest {
	private static CoNLL conll = new CoNLL();
//	private static String dir = System.getProperty("user.dir");
	private static String dir = DataTest.class.getResource("/").getPath();
	private static String sent_path = concatPaths(dir, "1-sent.conll");
	private static String sents_path = concatPaths(dir, "10-sents.conll");
	private static Dataset ds;

	@BeforeClass
	public static void setupClass() throws IOException {
		ds = conll.build_dataset(sent_path, "test", 100);
	}

	@Test public void parse_sentence() {
	    assert_(ds.sents.length == 1);
	    assert_(ds.tokens.length == 19);
	    assert_(ds.tokens[0][0] == conll.vocabs[VOCAB_WORD].getIndex("__ROOT__"));
	    assert_(ds.tokens[0][1] == conll.vocabs[VOCAB_POS].getIndex("__ROOT__"));
	    assert_(ds.tokens[0][2] == -1);
	    assert_(ds.tokens[1][0] == conll.vocabs[VOCAB_WORD].getIndex("Pierre"));
	    assert_(ds.tokens[1][1] == conll.vocabs[VOCAB_POS].getIndex("NNP"));
	    assert_(ds.tokens[1][2] == 2);
	    assert_(ds.tokens[18][0] == conll.vocabs[VOCAB_WORD].getIndex("."));
	    assert_(ds.tokens[18][1] == conll.vocabs[VOCAB_POS].getIndex("."));
	    assert_(ds.tokens[18][2] == 8);
	}

	@Test public void parse_all_in_file() throws IOException {
	    Dataset ds2 = conll.build_dataset(sents_path, "test", 500);
	    assert_(ds2.sents.length == 10);
	}

	@Test public void vocab() {
	    Vocabulary vocab = new Vocabulary();
	    assert_(vocab.getIndex("__MISSING__") == 0);
	    assert_(vocab.getIndex("a") == 1);
	    assert_(vocab.getIndex("a") == 1);
	    assert_(vocab.getIndex("b") == 2);
	    assert_(vocab.getWord(0) == "__MISSING__");
	    assert_(vocab.getWord(1) == "a");
	    assert_(vocab.getWord(2) == "b");
	}

	@Test public void max_index() {
	    Indexer indexer = new Indexer();
	    Map<String, Vocabulary> vocabs = newHashMap(
	    		"a", new Vocabulary(indexer), "b", new Vocabulary(indexer));
	    vocabs.get("a").getIndex("x");
	    vocabs.get("a").getIndex("y");
	    vocabs.get("b").getIndex("x");
	    vocabs.get("b").getIndex("y");
	    vocabs.get("b").getIndex("z");
	    assert_(Vocabulary.max_index(vocabs.values()) == 4);
	}

	@Test public void prune() {
	    Vocabulary vocab = new Vocabulary();
	    assert_(vocab.getIndex("a") == 0);
	    assert_(vocab.getIndex("a") == 0);
	    assert_(vocab.getIndex("b") == 1);
	    vocab.prune(1);
	    assert_(vocab.size() == 2);
	    vocab.prune(2);
	    assert_(vocab.size() == 1);
	}

	@Test public void write() throws IOException {
	    File tmpname = Pythonic.tmpname();
	    conll.write_sentence(tmpname, ds.tokens);
	    System.out.println("Dataset written to " + tmpname);
	    List<String> content = Files.lines(tmpname.toPath()).collect(Collectors.toList());
	    assert_(content.size() == 18);

	    Dataset ds2 = conll.build_dataset(sents_path, "test", 500);
	    List<String> orig_content = Files.lines(Paths.get(sents_path)).collect(Collectors.toList());
	    tmpname = Pythonic.tmpname();
	    conll.write_all_sentences(tmpname, ds2);
	    System.out.println("Dataset written to " + tmpname);
	    content = Files.lines(tmpname.toPath()).collect(Collectors.toList());
	    assert_(orig_content.size() == content.size() || orig_content.size() == content.size()-1);
	    for (int i = 0; i < orig_content.size(); i++) {
	        assert_(orig_content.get(i).equals(content.get(i)));
	    }
	}

}
