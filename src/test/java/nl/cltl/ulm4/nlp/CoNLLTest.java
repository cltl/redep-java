package nl.cltl.ulm4.nlp;

import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.util.Pythonic.concatPaths;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import nl.cltl.ulm4.util.Pythonic;
import nl.cltl.ulm4.util.Tensors;

import org.junit.Test;

public class CoNLLTest {

	private static String dir = CoNLLTest.class.getResource("/").getPath();
	private static String sentsPath = concatPaths(dir, "10-sents.conll");

	@Test
	public void substitueSame() throws IOException {
		CoNLL conll = new CoNLL();
		Dataset ds = conll.build_dataset(sentsPath, "10-sents", 500);
		int[][] links = Tensors.narrowColumns(ds.tokens, COL_HEAD, 2);
		String tmpPath = Pythonic.tmpname().toString();
		conll.substitue_dependency(sentsPath, tmpPath, ds, links);
//		System.out.println(String.join("\n", Files.readAllLines(Paths.get(tmpPath))));
		List<String> writenLines = Files.readAllLines(Paths.get(tmpPath));
		if (writenLines.get(writenLines.size()-1).trim().isEmpty())
			writenLines = writenLines.subList(0, writenLines.size()-1);
		assertEquals(Files.readAllLines(Paths.get(sentsPath)), writenLines);
	}
	
}
