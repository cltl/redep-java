package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.dep.DependencyUtils.newVocabularies;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static nl.cltl.ulm4.util.Pythonic.concatPaths;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.IntStream;

import nl.cltl.ulm4.dep.oracle.DeterministicArcStandardShortestStackOracle;
import nl.cltl.ulm4.nlp.CoNLL;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nlp.Vocabulary;
import nl.cltl.ulm4.util.Tensors;

import org.junit.Test;

public class SupervisedDatasetTest {

	private static String dir = SupervisedDatasetTest.class.getResource("/").getPath();
	private static String sentsPath = concatPaths(dir, "10-sents.conll");

	@Test
	public void build() throws IOException {
		CoNLL conll = new CoNLL();
		Dataset ds = conll.build_dataset(sentsPath, "10-sents", 500);
		ActionsInfo info = ArcStandard.buildActionsInfo(conll.vocabs[VOCAB_LABEL], "ROOT");
		SupervisedDataset sds = SupervisedDataset.build_dataset(ds,
				new DeterministicArcStandardShortestStackOracle(), new ArcStandard(info),
				ArcStandard.eligibleMap(info),
				new ChenManningFeatureExtractor(), "", 500, conll.vocabs);		
		int j = 0;
		int[][] masks = ArcStandard.eligibleMap(info);
		for (int i = 0; i < ds.sents.length; i++) {
			int[][] tokens = Tensors.narrow(ds.tokens, ds.sents[i][1], ds.sents[i][2]);
			ArcStandard sys = new ArcStandard(info, tokens);
			while (!sys.isTerminated()) {
				int action = sds.y[j];
				sys.execute(action);
				assertTrue(IntStream.of(masks[sds.states[j]]).anyMatch(k -> k == action));
				j++;
			}
			for (int k = 1; k < tokens.length; k++) {
				assertEquals(tokens[k][COL_HEAD], sys.links[k][0]);
				assertEquals(tokens[k][COL_LABEL], sys.links[k][1]);
			}
		}
//		for (int i = 0; i < sds.x.length; i++) {
//			System.out.println(Arrays.toString(sds.x[i]));
//		}
		assertEquals(sds.y.length, j);
	}

	private int[][] tokens = {
			{ 31, 9, -1, 0 }, // root
			{ 29, 2, 2, 2 }, // 1st word
			{ 7, 3, 0, 3 }, // 2nd word
			{ 11, 2, 2, 2 }, // 3rd word
			{ 13, 2, 3, 2 }, // 4th word
	};

	private int[][] sents = { { 0, 0, 5 }, };

	private Dataset ds = new Dataset(sents, tokens);

	private Vocabulary[] input_vocabs = newVocabularies();

	private ActionsInfo actions = ArcStandard.buildActionsInfo(
			Arrays.asList(new String[] { "dep", "prop", "v", "root" }), "root",
			input_vocabs[VOCAB_LABEL]);

	@Test
	public void checkLengths() {
		DeterministicArcStandardShortestStackOracle oracle = new DeterministicArcStandardShortestStackOracle();
		SupervisedDataset sds = SupervisedDataset.build_dataset(ds, oracle,
				new ArcStandard(actions), ArcStandard.eligibleMap(actions),
				new ChenManningFeatureExtractor(), "", 100, input_vocabs);
		assertEquals(8, sds.x.length);
		assertTrue(sds.x.length == sds.y.length);
		assertTrue(sds.states.length == sds.y.length);
	}

}
