package nl.cltl.ulm4.dep;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Random;

import nl.cltl.ulm4.dep.oracle.DeterministicArcEagerOracle;
import nl.cltl.ulm4.nlp.Vocabulary;

import org.junit.Test;

public class ArcEagerWithTreeConstraintTest {

	private ActionsInfo info = ArcEagerWithTreeConstraint.buildActionsInfo(
			Arrays.asList("root", "dep"), "root", new Vocabulary());
	ArcEagerWithTreeConstraint sys = new ArcEagerWithTreeConstraint(info);
	int[][] tokens = { 
			{0, 0, -1, -1}, 
			{0, 0, 2, 1}, 
			{0, 0, 3, 1}, 
			{0, 0, 0, 0}, 
			{0, 0, 3, 1}, 
			{0, 0, 4, 1}, 
			{0, 0, 7, 1}, 
			{0, 0, 5, 1}, 
			{0, 0, 7, 1}, 
			{0, 0, 7, 1}
	};

	@Test
	public void eligibleOutput() {
		int[][] map = ArcEagerWithTreeConstraint.eligibleMap(info);
		for (int i = 0; i < map.length; i++) {
			Arrays.sort(map[i]);
		}
		for (int i = 0; i < 3; i++) {
			DeterministicArcEagerOracle oracle = new DeterministicArcEagerOracle();
			sys.reset(tokens);
			oracle.reset(tokens);
			while (!sys.isTerminated()) {
				int action = oracle.next(sys);
				assertTrue(Arrays.binarySearch(map[sys.state()], action) >= 0);
				sys.execute(action);
			}
		}
	}

	@Test
	public void copy() {
		ArcEagerWithTreeConstraint copy = (ArcEagerWithTreeConstraint) sys.copy();
		assertTrue(copy.getStack() != sys.getStack());
	}

	@Test
	public void welformness() {
		int[][] map = ArcEagerWithTreeConstraint.eligibleMap(info);
		Random r = new Random();
		for (int n = 0; n < 1000; n++) {
			sys.reset(tokens);
			while (!sys.isTerminated()) {
//				System.out.println(sys.state());
				int[] eligibleActions = map[sys.state()];
				int action = eligibleActions[r.nextInt(eligibleActions.length)];
				sys.execute(action);
//				System.out.format("%s %s %s\n", info.getVocabulary().getWord(action),
//						reversed(sys.stack), sys.buffer);
			}
			for (int i = 1; i < sys.tokens.length; i++) {
				assertTrue(sys.tokens[i][0] >= 0);
			}
		}
	}
	
}
