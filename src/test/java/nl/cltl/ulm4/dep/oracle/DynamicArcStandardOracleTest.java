package nl.cltl.ulm4.dep.oracle;

import static nl.cltl.ulm4.dep.DependencyUtils.newVocabularies;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.dep.oracle.DynamicArcStandardOracle.edge_delta;
import static nl.cltl.ulm4.dep.oracle.DynamicArcStandardOracle.make_right_stack;
import static nl.cltl.ulm4.dep.oracle.DynamicArcStandardOracle.tree_frag_unlabeled_loss;
import static nl.cltl.ulm4.util.CollectionUtils.toArray;
import static nl.cltl.ulm4.util.CollectionUtils.unique;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.ArcStandard;
import nl.cltl.ulm4.nlp.Vocabulary;

import org.junit.Test;

public class DynamicArcStandardOracleTest {

	private int[][] tokens = { 
			{ 31, 9, -1, 0, 0 }, // root
			{ 29, 2, 2, 3, 3 }, // 1st word
			{ 7, 3, 0, 3, 3 }, // 2nd word
			{ 11, 2, 2, 3, 3 }, // 3rd word
			{ 13, 2, 3, 3, 3 }, // 4th word
	};
	
	private Vocabulary[] input_vocabs = newVocabularies();
	
	ActionsInfo info = ArcStandard.buildActionsInfo(
			Arrays.asList(new String[] {"dep", "prop", "v", "root"}), 
			"root", input_vocabs[VOCAB_LABEL]);
	int shift_index = unique(info.type2actions(ArcStandard.ATYPE_SHIFT));

	@Test
	public void right_stack() {
		ArcStandard sys = new ArcStandard(info, tokens);
		sys.execute(shift_index);
	    List<Integer> rs = make_right_stack(sys, tokens);
	    assertArrayEquals(new int[] {2}, toArray(rs));
	    
	    int[][] gold_links = new int[][] {{0, 0, -1}, {0, 0, 2}, {0, 0, 3}, {0, 0, 4}, {0, 0, 0}};
	    rs = make_right_stack(sys, gold_links);
	    assertArrayEquals(new int[] {2, 3, 4}, toArray(rs));
	}

	@Test
	public void treeFragUnlabeledLossSimple() {
	    int[][] links = new int[][] {{-1}, {0}, {1}, {2}, {3}};
	    assertEquals(2, tree_frag_unlabeled_loss(0, links, tokens));
	    assertEquals(1, tree_frag_unlabeled_loss(1, links, tokens));
	    assertEquals(0, tree_frag_unlabeled_loss(2, links, tokens));
	    assertEquals(0, tree_frag_unlabeled_loss(3, links, tokens));
	    assertEquals(0, tree_frag_unlabeled_loss(4, links, tokens));
	    links = new int[][] {{-1}, {2}, {0}, {2}, {3}};
	    assertEquals(0, tree_frag_unlabeled_loss(0, links, tokens));
	    assertEquals(0, tree_frag_unlabeled_loss(1, links, tokens));
	    assertEquals(0, tree_frag_unlabeled_loss(2, links, tokens));
	    assertEquals(0, tree_frag_unlabeled_loss(3, links, tokens));
	    assertEquals(0, tree_frag_unlabeled_loss(4, links, tokens));
	}

	@Test
	public void edgeDeltaSimple() {
	    assertEquals(0, edge_delta(-1, 0, tokens));
	    assertEquals(1, edge_delta(-1, 1, tokens));
	    assertEquals(1, edge_delta(-1, 2, tokens));
	    assertEquals(0, edge_delta(2, 1, tokens));
	    assertEquals(1, edge_delta(1, 2, tokens));
	}

	@Test
	public void config_loss() {
		DynamicArcStandardOracle oracle = new DynamicArcStandardOracle();
	    ArcStandard sys = new ArcStandard(info, tokens);
	    sys.execute(shift_index);
//	    System.out.println(Tensors.toString(sys.links));
	    sys.execute(info.vocab.getIndex("R(dep)"));
//	    System.out.println(Tensors.toString(sys.links));
	    assertEquals(1, oracle.unlabeledLoss(sys));
	    
	    sys.reset(tokens);
	    sys.execute(shift_index);
	    sys.execute(shift_index);
	    sys.execute(shift_index);
	    sys.execute(info.vocab.getIndex("L(dep)"));
	    assertEquals(3, oracle.unlabeledLoss(sys));
	    
	    sys.reset(tokens);
	    sys.execute(shift_index);
	    sys.execute(shift_index);
	    sys.execute(shift_index);
	    sys.execute(shift_index);
	    assertEquals(0, oracle.unlabeledLoss(sys));
	}
	
	@Test
	public void zeroLossAction() {
		DynamicArcStandardOracle oracle = new DynamicArcStandardOracle();
	    ArcStandard sys = new ArcStandard(info);
	    Random r = new Random();
	    int[][] eligibleMap = ArcStandard.eligibleMap(info);
	    for (int i = 0; i < 10; i++) {
	    	sys.reset(tokens);
			for (int j = 0; j < r.nextInt(3); j++) {
				int[] eligibleActions = eligibleMap[sys.state()];
				sys.execute(eligibleActions[r.nextInt(eligibleActions.length)]);
			}
			int loss = oracle.loss(sys);
			for (int j = 0; j < 5; j++) {
				sys.execute(oracle.getZeroLossAction(sys, tokens, info));
				assertEquals(loss, oracle.loss(sys));
			}
		}
	}
}