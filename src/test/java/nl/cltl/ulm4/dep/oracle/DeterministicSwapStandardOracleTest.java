package nl.cltl.ulm4.dep.oracle;

import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.SwapStandard;
import nl.cltl.ulm4.nlp.CoNLL;
import nl.cltl.ulm4.nlp.Dataset;
import nl.cltl.ulm4.nlp.Vocabulary;

import org.junit.Test;

public class DeterministicSwapStandardOracleTest {

	private ActionsInfo info = SwapStandard.buildActionsInfo(
			Arrays.asList("root", "dep"), "root", new Vocabulary());
	SwapStandard sys = new SwapStandard(info);
	int[][] tokens = { 
			{0, 0, -1, -1}, 
			{0, 0, 2, 1}, 
			{0, 0, 3, 1}, 
			{0, 0, 0, 0}, 
			{0, 0, 3, 1}, 
			{0, 0, 2, 1}, 
			{0, 0, 7, 1}, 
			{0, 0, 5, 1}, 
			{0, 0, 4, 1}, 
			{0, 0, 3, 1}
	};
	
	@Test
	public void eagerWithPaperSample() {
		DeterministicSwapStandardEagerOracle oracle = new DeterministicSwapStandardEagerOracle();
		oracle.run(sys, tokens);
		int[][] links = sys.getLinks();
		for (int i = 1; i < tokens.length; i++) {
			assertEquals(tokens[i][COL_HEAD], links[i][0]);
			assertEquals(tokens[i][COL_LABEL], links[i][1]);
		}
	}
	
	@Test
	public void sentence25() throws IOException {
		String path = this.getClass().getResource("/spmrl2014/sentence25.conll").getPath();
		CoNLL conll = new CoNLL();
		Dataset ds = conll.build_dataset(path, "s25", 100);

		ActionsInfo info = SwapStandard.buildActionsInfo(conll.vocabs[VOCAB_LABEL], "root");
		StaticOracle oracle = new DeterministicSwapStandardEagerOracle();
		int[][] eligibleActionsMap = SwapStandard.eligibleMap(info);
		SwapStandard sys = new SwapStandard(info);

        sys.reset(ds.tokens);
        oracle.reset(ds.tokens);
        int action = oracle.next(sys);
        while (action >= 0) {
        	boolean found = false;
        	for (int v : eligibleActionsMap[sys.state()]) {
        		if (v == action) found = true;
        	}
        	assertTrue(found);
            sys.execute(action);
            action = oracle.next(sys);
        }
        for (int i = 1; i < sys.getLinks().length; i++) {
        	int[] link = sys.getLinks()[i];
        	assertEquals(ds.tokens[i][COL_HEAD], link[0]);
        	assertEquals(ds.tokens[i][COL_LABEL], link[1]);
        }
	}
	
}
