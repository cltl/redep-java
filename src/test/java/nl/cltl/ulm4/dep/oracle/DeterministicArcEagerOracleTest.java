package nl.cltl.ulm4.dep.oracle;

import static nl.cltl.ulm4.nlp.Dataset.COL_HEAD;
import static nl.cltl.ulm4.nlp.Dataset.COL_LABEL;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import nl.cltl.ulm4.dep.ActionsInfo;
import nl.cltl.ulm4.dep.ArcEager;
import nl.cltl.ulm4.nlp.Vocabulary;

import org.junit.Test;

public class DeterministicArcEagerOracleTest {

	private ActionsInfo info = ArcEager.buildActionsInfo(
			Arrays.asList("root", "dep"), "root", new Vocabulary());
	ArcEager sys = new ArcEager(info);
	int[][] tokens = { 
			{0, 0, -1, -1}, 
			{0, 0, 2, 1}, 
			{0, 0, 3, 1}, 
			{0, 0, 0, 0}, 
			{0, 0, 3, 1}, 
			{0, 0, 4, 1}, 
			{0, 0, 7, 1}, 
			{0, 0, 5, 1}, 
			{0, 0, 7, 1}, 
			{0, 0, 7, 1}
	};

	@Test
	public void eagerWithPaperSample() {
		DeterministicArcEagerOracle oracle = new DeterministicArcEagerOracle();
		List<Integer> actions = oracle.run(sys, tokens);
		System.out.println(Arrays.toString(actions.stream()
				.map(a -> info.getVocabulary().getWord(a)).toArray()));
		int[][] links = sys.getLinks();
		for (int i = 1; i < tokens.length; i++) {
			assertEquals(tokens[i][COL_HEAD], links[i][0]);
			assertEquals(tokens[i][COL_LABEL], links[i][1]);
		}
	}
	
}
