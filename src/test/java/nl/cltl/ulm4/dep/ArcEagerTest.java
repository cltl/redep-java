package nl.cltl.ulm4.dep;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import nl.cltl.ulm4.dep.oracle.DeterministicArcEagerOracle;
import nl.cltl.ulm4.nlp.Vocabulary;

import org.junit.Test;

public class ArcEagerTest {

	private ActionsInfo info = ArcEager.buildActionsInfo(
			Arrays.asList("root", "dep"), "root", new Vocabulary());
	ArcEager sys = new ArcEager(info);
	int[][] tokens = { 
			{0, 0, -1, -1}, 
			{0, 0, 2, 1}, 
			{0, 0, 3, 1}, 
			{0, 0, 0, 0}, 
			{0, 0, 3, 1}, 
			{0, 0, 4, 1}, 
			{0, 0, 7, 1}, 
			{0, 0, 5, 1}, 
			{0, 0, 7, 1}, 
			{0, 0, 7, 1}
	};

	@Test
	public void eligibleOutput() {
		int[][] map = ArcEager.eligibleMap(info);
		for (int i = 0; i < map.length; i++) {
			Arrays.sort(map[i]);
		}
		DeterministicArcEagerOracle oracle = new DeterministicArcEagerOracle();
		sys.reset(tokens);
		oracle.reset(tokens);
		while (!sys.isTerminated()) {
			int action = oracle.next(sys);
			assertTrue(Arrays.binarySearch(map[sys.state()], action) >= 0);
			sys.execute(action);
		}
	}

	@Test
	public void copy() {
		ArcEager copy = (ArcEager) sys.copy();
		assertTrue(copy.getStack() != sys.getStack());
	}
	
}
