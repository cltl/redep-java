package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.dep.DependencyUtils.getLeftDependent;
import static nl.cltl.ulm4.dep.DependencyUtils.getTreeInOrder;
import static nl.cltl.ulm4.dep.DependencyUtils.is_projective;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DependencyUtilsTest {

	@Test public void projective() {
	    assertTrue(is_projective(new int[][] {{-1}, {0}, {1}}, 0)); // simple;
	    assertTrue(is_projective(new int[][] {{-1}, {0}, {1}, {1}, {6}, {6}, {3}}, 0)); // complex;
	    assertFalse(is_projective(new int[][] {{-1}, {0}, {0}, {1}}, 0)); // crossing edges;
	    assertFalse(is_projective(new int[][] {{-1}, {2}, {1}, {0}}, 0)); // cyclic;
	}

	@Test public void left_child() {
		int[][] links = {
	            {-1, 0, 0},
	            {0, 0, 0},
	            {1, 0, 0},
	            {0, 0, 0},
	    };
	    assertTrue(getLeftDependent(links, 0, 1, 0) < 0);
	    assertTrue(getLeftDependent(links, 1, 1, 0) < 0);
	    assertTrue(getLeftDependent(links, 1, 2, 0) < 0);

	    links = new int[][] {
	            {2, 0, 0},
	            {2, 0, 0},
	            {-1, 0, 0},
	            {2, 0, 0},
	    };
	    assertEquals(0, getLeftDependent(links, 2, 1, 0));
	    assertEquals(1, getLeftDependent(links, 2, 2, 0));
	    assertTrue(getLeftDependent(links, 2, 3, 0) < 0);
	}


	@Test public void right_child() {
	    int[][] links = { // having children, all to the right
	            {-1, 0, 0},
	            {0, 0, 0},
	            {1, 0, 0},
	            {0, 0, 0},
	    };
	    assertEquals(3, DependencyUtils.getRightDependent(links, 0, 1, 0));
	    assertEquals(1, DependencyUtils.getRightDependent(links, 0, 2, 0));
	    assertTrue(DependencyUtils.getRightDependent(links, 0, 3, 0) < 0);
	    assertEquals(2, DependencyUtils.getRightDependent(links, 1, 1, 0));
	    assertTrue(DependencyUtils.getRightDependent(links, 1, 2, 0) < 0);

	    links = new int[][] { // having children but not to the right
	            {3, 0, 0},
	            {2, 0, 0},
	            {3, 0, 0},
	            {-1, 0, 0},
	    };
	    assertTrue(DependencyUtils.getRightDependent(links, 0, 1, 0) < 0);
	    assertTrue(DependencyUtils.getRightDependent(links, 1, 1, 0) < 0);
	    assertTrue(DependencyUtils.getRightDependent(links, 2, 1, 0) < 0);
	    assertTrue(DependencyUtils.getRightDependent(links, 3, 1, 0) < 0);
	    assertTrue(DependencyUtils.getRightDependent(links, 3, 2, 0) < 0);

	    links = new int[][] { // having children, some left, some right
	            {2, 0, 0},
	            {2, 0, 0},
	            {3, 0, 0},
	            {2, 0, 0},
	    };
	    assertTrue(DependencyUtils.getRightDependent(links, 0, 1, 0) < 0);
	    assertTrue(DependencyUtils.getRightDependent(links, 1, 1, 0) < 0);
	    assertEquals(3, DependencyUtils.getRightDependent(links, 2, 1, 0));
	    assertTrue(DependencyUtils.getRightDependent(links, 2, 2, 0) < 0);
	    assertTrue(DependencyUtils.getRightDependent(links, 3, 1, 0) < 0);
	}
	
	@Test
	public void inOrder() {
		String[] words = "ROOT A hearing is scheduled on the issue today .".split(" ");
		int[][] links = { {-1}, {2}, {3}, {0}, {3}, {2}, {7}, {5}, {4}, {3}};
		int[] order = getTreeInOrder(links, 0);
		String[] sortedWords = new String[words.length];
		for (int i = 0; i < order.length; i++) {
			sortedWords[order[i]] = words[i];
		}
		assertEquals("ROOT A hearing on the issue is scheduled today .",  
				String.join(" ", sortedWords));
	}

}
