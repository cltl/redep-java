package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.dep.DependencyUtils.newVocabularies;
import static nl.cltl.ulm4.util.Pythonic.list;
import nl.cltl.ulm4.nlp.Vocabulary;

import org.junit.Test;

public class FeatureTest {

	Vocabulary[] vocabs = newVocabularies();

	ArcStandard sys = new ArcStandard(null);

	public FeatureTest() {
		sys.tokens = new int[][] { 
				{ 31, 9, 0, 2, 5 }, // root
				{ 29, 2, 3, 3, 2 }, // 1st word
				{ 7, 3, 1, 6, 4 }, // 2nd word
				{ 11, 2, 3, 5, 8 }, // 3rd word
				{ 13, 2, 4, 9, 7 }, // 4th word
		};
		sys.links = new int[][] { 
				{ -1, 0 }, 
				{  0, 7 }, 
				{ -1, 0 }, 
				{ -1, 0 },
				{ -1, 0 }, };
		sys.stack = list(2, 0);
		sys.buffer = list(3, 4);
	}

	@Test
	public void test_chen_manning() {
		ChenManningFeatureExtractor cm = new ChenManningFeatureExtractor();
		int[] x = new int[cm.num()];
		cm.extract(x, sys, vocabs);
	}

}
