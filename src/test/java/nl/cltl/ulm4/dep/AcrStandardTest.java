package nl.cltl.ulm4.dep;

import static nl.cltl.ulm4.dep.DependencyUtils.newVocabularies;
import static nl.cltl.ulm4.dep.DependencyUtils.VOCAB_LABEL;
import static nl.cltl.ulm4.util.CollectionUtils.unique;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;

import nl.cltl.ulm4.dep.oracle.DeterministicArcStandardShortestStackOracle;
import nl.cltl.ulm4.nlp.Vocabulary;

import org.junit.Test;

public class AcrStandardTest {

	private int[][] tokens = { 
			{ 31, 9, -1, 0 }, // root
			{ 29, 2, 2, 2 }, // 1st word
			{ 7, 3, 0, 2 }, // 2nd word
			{ 11, 2, 2, 2 }, // 3rd word
			{ 13, 2, 3, 2 }, // 4th word
	};
	
	private Vocabulary[] input_vocabs = newVocabularies();
	
	private ActionsInfo info = ArcStandard.buildActionsInfo(
			Arrays.asList("dep", "prop", "v", "root"), 
			"root", input_vocabs[VOCAB_LABEL]);
	private int shiftIndex = unique(info.type2actions(ArcStandard.ATYPE_SHIFT));

	@Test
	public void buildEligibleActionsMap() {
		int[][] map = ArcStandard.eligibleMap(info);
		boolean[] used = new boolean[info.vocab.size()];
		for (int i = 0; i < map.length; i++) {
			for (int j : map[i]) {
				used[j] = true;
			}
		}
		for (int j = 0; j < used.length; j++) {
			if (!info.vocab.getWord(j).equals("L(root)")) {
				assertTrue(used[j]);
			}
		}
	}
	
	@Test
	public void execute() {
	    ArcStandard sys = new ArcStandard(info, tokens);
		sys.execute(shiftIndex);
	    sys.execute(shiftIndex);
	    sys.execute(shiftIndex);
	    // print(sys.stack)
	    sys.execute(info.vocab.getIndex("L(dep)"));
	    // print(sys.stack)
	    sys.execute(info.vocab.getIndex("L(dep)"));
	    // print(sys.stack)
	    sys.execute(shiftIndex);
	    // print(sys.stack)
	    sys.execute(info.vocab.getIndex("R(v)"));
	    sys.execute(info.vocab.getIndex("R(v)"));
	    // print(sys.stack)
	    // print(sys.links)
	    assertEquals(-1, sys.links[0][0]);
	    assertEquals(3, sys.links[1][0]);
	    assertEquals(3, sys.links[2][0]);
	    assertEquals(0, sys.links[3][0]);
	    assertEquals(3, sys.links[4][0]);
	}

	@Test
	public void shortest_stack_oracle() {
		ArcStandard sys = new ArcStandard(info);
	    DeterministicArcStandardShortestStackOracle oracle = new DeterministicArcStandardShortestStackOracle();
	    int shift_index = unique(info.type2actions(ArcStandard.ATYPE_SHIFT));
	    Iterator<Integer> iter = oracle.run(sys, tokens).iterator();
	    assertEquals(shift_index, iter.next().intValue());
	    assertEquals(shift_index, iter.next().intValue());
	    assertEquals(info.vocab.getIndex("L(v)"), iter.next().intValue());
	    assertEquals(shift_index, iter.next().intValue());
	    assertEquals(shift_index, iter.next().intValue());
	    assertEquals(info.vocab.getIndex("R(v)"), iter.next().intValue());
	    assertEquals(info.vocab.getIndex("R(v)"), iter.next().intValue());
	    assertEquals(info.vocab.getIndex("R(v)"), iter.next().intValue());
	    assertFalse(iter.hasNext());
	}

}
