package nl.cltl.ulm4.dep;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

public class ParserTest {

	@Test
	public void load_stanford_model() throws IOException {
	    String path = "stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz";
	    Parser<ArcStandard> parser = Parser.load_stanford_model(path, "root");
//	    print(mlp);
	    assertNotNull(parser.mlp);
	    assertNotNull(parser.vocabs);
	    assertNotNull(parser.newTransitionSystem());
	}
	
}
