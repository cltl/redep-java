DEP_TYPE=$1
ROOT_LABEL=$2
MODEL_NAME=$3

# evaluate against valid set
java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.Parser -rootLabel $ROOT_LABEL \
        -model output/dep/$DEP_TYPE-$MODEL_NAME.bin \
        -input output/dep/penntree.$DEP_TYPE/valid.mrg.dep \
        -output output/dep/$DEP_TYPE-$MODEL_NAME-valid.conll && \
java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
        edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True \
        -g output/dep/penntree.$DEP_TYPE/valid.mrg.dep \
        -s output/dep/$DEP_TYPE-$MODEL_NAME-valid.conll && \
# evaluate against test set
java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.Parser -rootLabel $ROOT_LABEL \
        -model output/dep/$DEP_TYPE-$MODEL_NAME.bin \
        -input output/dep/penntree.$DEP_TYPE/test.mrg.dep \
        -output output/dep/$DEP_TYPE-$MODEL_NAME-test.conll && \
java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
        edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True \
        -g output/dep/penntree.$DEP_TYPE/test.mrg.dep \
        -s output/dep/$DEP_TYPE-$MODEL_NAME-test.conll
