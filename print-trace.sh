DEP_TYPE="sd"
ROOT_LABEL="root"
MODEL="stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz"

mvn package compile assembly:single && \
java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.Parser -rootLabel $ROOT_LABEL \
        -errorProp on -model $MODEL \
        -input output/dep/penntree.$DEP_TYPE/valid.mrg.dep \
        -output /dev/null
