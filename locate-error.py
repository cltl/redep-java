import sys
import re
from collections import Counter

assert len(sys.argv) == 3, 'Usage: error-only.py gold target'

with open(sys.argv[1]) as gf:
    error_count = Counter()
    s = 1 # match index of MaltEval
    with open(sys.argv[2]) as tf:
        gline = gf.readline()
        tline = tf.readline()
        while gline:
            while not re.match('\s*\n', gline):
                gfields = gline.strip().split('\t')
                tfields = tline.strip().split('\t')
                assert gfields[1] == tfields[1]
                if gfields[6] != tfields[6] or gfields[7] != tfields[7]:
                    error_count[s] += 1
                gline = gf.readline()
                tline = tf.readline()
            gline = gf.readline()
            tline = tf.readline()
            s += 1
for k in sorted(error_count, key=lambda x: error_count[x], reverse=True):
    print(k, error_count[k])