mvn clean package compile assembly:single && \
    # train sd parser
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.SupervisedLearning -rootLabel root \
            -train output/dep/penntree.sd/train.mrg.dep \
            -valid output/dep/penntree.sd/valid.mrg.dep \
            -parser output/dep/sd-supervised-arc-standard.bin && \
    ./evaluate.sh sd root supervised-arc-standard
        
# train lth parser