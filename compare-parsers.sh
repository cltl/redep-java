mvn clean package compile assembly:single && \
	java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        	nl.cltl.ulm4.redep_experiment.CompareParsers \
        	-model1 stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz \
        	-model2 output/dep/sd-reinforcement-arc-standard-memory.bin \
        	-input output/dep/penntree.sd/test.mrg.dep \
        	-rootLabel root