mvn clean package compile assembly:single && \
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.SupervisedLearning \
            -transition arc-standard -rootLabel root -maxIters 2000 \
            -train output/dep/penntree.sd/train.mrg.dep \
            -valid output/dep/penntree.sd/valid.mrg.dep \
            -parser output/dep/transition-supervised.bin && \
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        	nl.cltl.ulm4.dep.re.ReinforcementLearning \
        	-train output/dep/penntree.sd/train.mrg.dep \
        	-valid output/dep/penntree.sd/valid.mrg.dep \
        	-pretrained /output/dep/transition-supervised.bin \
        	-sampler memory -maxIters 1000 -measure uas \
            -parser output/dep/transition-reinforcement.bin
