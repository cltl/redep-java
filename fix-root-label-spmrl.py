import os
import re

for root, _, fnames in os.walk('spmrl2014'):
    for fname in fnames:
        if re.search(r'\.conll$', fname):
            path = os.path.join(root, fname)
            with open(path) as f:
                s = f.read()
            lines = s.split('\n')
            for i, line in enumerate(lines):
                if len(line.strip()) > 0:
                    fields = line.split('\t')
                    if int(fields[6]) == 0:
                        fields[7] = 'root'
                    line = '\t'.join(fields)
                    lines[i] =  line
            s = '\n'.join(lines)
            with open(path, 'wt') as f:
                f.write(s)