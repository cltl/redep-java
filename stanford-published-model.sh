java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
        edu.stanford.nlp.parser.nndep.DependencyParser \
        -model stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz \
        -testFile output/dep/penntree.sd/test.mrg.dep && \
java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
        edu.stanford.nlp.parser.nndep.DependencyParser \
        -model stanford-parser-full-2014-10-31/PTB_CoNLL_params.txt.gz \
        -testFile output/dep/penntree.lth/test.mrg.dep
