'''
Convert the output of exp-sample-size.sh into a Latex table
'''
import re
from collections import defaultdict
import sys
from pprint import pprint

table = defaultdict(lambda: defaultdict(dict))
best_rows = dict()
best_vals = defaultdict(float)

def read_file(fname):
    with open(fname) as f:
        line = f.readline()
        while line:
            m = re.search("Parsing results.+(?:supervised-([-\w]+)|reinforcement-([-\w]+)-(oracle|random|memory))-test.conll", line)
            if m:
                row = m.group(3) or 'supervised'  
                col1 = m.group(1) or m.group(2) 
                read_cols = set()
                while line and len(read_cols) < 2:
                    m2 = re.search("(\w+) Attachment Score.+(0\\.\d+)", line)
                    if m2:
                        col2 = m2.group(1)
                        val = float(m2.group(2))
                        table[row][col1][col2] = val
                        read_cols.add(col2)
                    line = f.readline()
            else:
                line = f.readline()

def write_cell(row, col1, col2, reported_val=None):
    sys.stdout.write(" & ")
    if best_rows[(col1, col2)] == row: sys.stdout.write(r"\textbf{")
    sys.stdout.write("%.1f" %(reported_val or table[row][col1][col2]*100))
    if reported_val: sys.stdout.write("*")
    if best_rows[(col1, col2)] == row: sys.stdout.write(r"}")

def write_row(name, row, reported_unlabeled=None, reported_labeled=None):
    sys.stdout.write(r"\textsc{%s}" %name)
    write_cell(row, 'arc-standard', 'Unlabeled', reported_unlabeled)
    write_cell(row, 'arc-standard', 'Labeled', reported_labeled)
    write_cell(row, 'arc-eager', 'Unlabeled')
    write_cell(row, 'arc-eager', 'Labeled')
    write_cell(row, 'swap-standard', 'Unlabeled')
    write_cell(row, 'swap-standard', 'Labeled')
    print(r" \\ \hline")

def find_best_val():
    for row, row_content in table.iteritems():
        for col1, col1_content in row_content.iteritems():
            for col2, val in col1_content.iteritems():
                if val > best_vals[(col1, col2)]:
                    best_rows[(col1, col2)] = row
                    best_vals[(col1, col2)] = val

read_file('exp-arc-standard.sh.out')
read_file('exp-arc-eager.sh.out')
read_file('exp-swap-standard.sh.out')
find_best_val()

print(r"""
\begin{tabular}{|l|c|c|c|c|c|c|}
\hline
\multirow{2}{*}{\textbf{Transition system}} & \multicolumn{2}{c|}{\textbf{Arc-standard}} & \multicolumn{2}{c|}{\textbf{Arc-eager}} & \multicolumn{2}{c|}{\textbf{Swap-standard}} \\ \cline{2-7}
& \textbf{UAS} & \textbf{LAS} & \textbf{UAS} & \textbf{LAS} & \textbf{UAS} & \textbf{LAS} \\ \hline""")
write_row("SL", 'supervised', 91.8, 89.6)
write_row("RL-Oracle", 'oracle')
write_row("RL-Random", 'random')
write_row("RL-Memory", 'memory')
print(r"\end{tabular}")