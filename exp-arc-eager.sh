REINFORCEMENT_CMD="java -cp 
        target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.re.ReinforcementLearning \
        -adaEps 0.0001 -sampleSize 8 \
        -train output/dep/penntree.sd/train.mrg.dep \
        -valid output/dep/penntree.sd/valid.mrg.dep \
        -pretrained output/dep/sd-supervised-arc-eager.bin"

repeat_reinforce() { # because it varies more
    for x in $(seq -f "%02g" 1 5); do
        echo "######### reinforcement learning: reinforce$x" && \
        $REINFORCEMENT_CMD -reinforce -maxIters 8000 \
                -parser output/dep/sd-reinforcement-arc-eager-reinforce$x.bin && \
        ./evaluate.sh sd root reinforcement-arc-eager-reinforce$x

        if [ $? -ne 0 ]
        then
            return $?
        fi
    done
}

echo -n 'Start: '; date 
mvn clean package compile assembly:single # && \
    echo "######### supervised learning" && \
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.SupervisedLearning \
            -transition arc-eager -rootLabel root \
            -train output/dep/penntree.sd/train.mrg.dep \
            -valid output/dep/penntree.sd/valid.mrg.dep \
            -parser output/dep/sd-supervised-arc-eager.bin && \
    ./evaluate.sh sd root supervised-arc-eager && \
    repeat_reinforce 5 && \
    echo "######### reinforcement learning: oracle" && \
    $REINFORCEMENT_CMD -sampler oracle -maxIters 1000 \
            -parser output/dep/sd-reinforcement-arc-eager-oracle.bin && \
    ./evaluate.sh sd root reinforcement-arc-eager-oracle && \
    echo "######### reinforcement learning: random" && \
    $REINFORCEMENT_CMD -sampler random -maxIters 1000 \
            -parser output/dep/sd-reinforcement-arc-eager-random.bin && \
    ./evaluate.sh sd root reinforcement-arc-eager-random && \
    echo "######### reinforcement learning: memory" && \
    $REINFORCEMENT_CMD -sampler memory -maxIters 1000 \
            -parser output/dep/sd-reinforcement-arc-eager-memory.bin && \
    ./evaluate.sh sd root reinforcement-arc-eager-memory
EXIT_CODE=$?
echo -n 'Stop: '; date
exit $EXIT_CODE