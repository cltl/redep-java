mvn clean package compile assembly:single && \
    java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
            nl.cltl.ulm4.dep.Parser -rootLabel root \
            -model arc-standard-models/sd-reinforcement-arc-standard-random.bin \
            -input output/dep/penntree.sd/test.mrg.dep \
            -output output/dep/sd_parse-published-model_test.conll \
            -errorProp on && \
    java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
            edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True \
            -g output/dep/penntree.sd/test.mrg.dep \
            -s output/dep/sd_parse-published-model_test.conll 
