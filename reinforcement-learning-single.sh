# train sd parser
java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.re.ReinforcementLearning -sampler $1 \
        -train output/dep/penntree.sd/train.mrg.dep \
        -valid output/dep/penntree.sd/valid.mrg.dep \
        -pretrained stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz \
        -parser output/dep/sd_reinforcement-learning_$1.bin && \
# evaluate against valid set
java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.Parser -rootLabel root \
        -model output/dep/sd_reinforcement-learning_$1.bin \
        -input output/dep/penntree.sd/valid.mrg.dep \
        -output output/dep/sd_reinforcement-learning_$1_valid.conll && \
java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
        edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True \
        -g output/dep/penntree.sd/valid.mrg.dep \
        -s output/dep/sd_reinforcement-learning_$1_valid.conll && \
# evaluate against test set
java -cp target/redep-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
        nl.cltl.ulm4.dep.Parser -rootLabel root \
        -model output/dep/sd_reinforcement-learning_$1.bin \
        -input output/dep/penntree.sd/test.mrg.dep \
        -output output/dep/sd_reinforcement-learning_$1_test.conll && \
java -cp stanford-parser-full-2014-10-31/stanford-parser.jar \
        edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True \
        -g output/dep/penntree.sd/test.mrg.dep \
        -s output/dep/sd_reinforcement-learning_$1_test.conll
